#include "GameMgr.h"

bool GameMgr::initGameMgr( )
{
    pDataMgr = new DataMgr( );
    pDataMgr->init( "data/GameConfig.xml" );
    version = pDataMgr->getSystemSetting( "version" );
    playerObject = new Player;
    // cout << "GameMgr initGameMgr() " << version << endl;
    if ( ! initIrrlicht() )
        return false;
    // init the sound library and set soundAvailable

    // can load resourses from archive
//    pDevice->getFileSystem()->addZipFileArchive("media/media.zip");
    cout << "Working Folder: " << pDevice->getFileSystem()->getWorkingDirectory().c_str() << endl;
    // init the other Irrlicht system pointers to pass on to levels
    pVideo = pDevice->getVideoDriver();
    pSceneMgr = pDevice->getSceneManager();
    pGUIEnv = pDevice->getGUIEnvironment();
    clearKeys();
    initSound();
    // set the caption of the window
    pDevice->setWindowCaption( Utils::wideString( version ).c_str() );
    running = true;
    cout << "GameMgr initGameMgr() success." << endl;
    return true;
}

void GameMgr::resetGameMgr()
{

    pDevice->closeDevice();

    cout << "pre initGameMgr  )" << endl;
    initGameMgr();

    changeState( StartMenu::getInstance() );
}

bool GameMgr::initIrrlicht()
{
    // set video::E_DRIVER_TYPE
    if( pDataMgr->getSystemSetting( "device" ) == "OpenGL" )
        videoDriverType = video::EDT_OPENGL;
    else if( pDataMgr->getSystemSetting( "device" ) == "DirectX 9" )
        videoDriverType = video::EDT_DIRECT3D9;
    else
        videoDriverType = video::EDT_SOFTWARE ;
    // set screen resolution rectangle
    screenResolution = core::dimension2d<irr::u32>(
                           Utils::toInt( pDataMgr->getSystemSetting( "width" ) ),
                           Utils::toInt( pDataMgr->getSystemSetting( "height" ) ) );
    // set full screen
    fullScreen = Utils::toBoolean( pDataMgr->getSystemSetting( "fullscreen" ) );
    // build the parameters struct for createDeviceEx() function
    irr::SIrrlichtCreationParameters params;
    params.AntiAlias = Utils::toBoolean( pDataMgr->getSystemSetting( "antialias" ) );
    params.Bits = Utils::toInt( pDataMgr->getSystemSetting( "bpp" ) );
    params.DriverType = videoDriverType;
    params.Doublebuffer = Utils::toBoolean( pDataMgr->getSystemSetting( "doublebuffer" ) );
    params.EventReceiver = this;
    params.Fullscreen = fullScreen;
    params.HighPrecisionFPU = Utils::toBoolean( pDataMgr->getSystemSetting( "highprecisionfpu" ) );
    params.IgnoreInput = false;
    params.Stencilbuffer = Utils::toBoolean( pDataMgr->getSystemSetting( "stencilbuffer" ) );
    params.Stereobuffer = Utils::toBoolean( pDataMgr->getSystemSetting( "stereobuffer" ) );
    params.Vsync = Utils::toBoolean( pDataMgr->getSystemSetting( "vsync" ) );
    params.WindowSize = screenResolution;
    params.WithAlphaChannel = Utils::toBoolean( pDataMgr->getSystemSetting( "withalphachannel" ) );
    params.ZBufferBits = Utils::toInt( pDataMgr->getSystemSetting( "zbufferbits" ) );
    // create the irrlicht device
    pDevice = irr::createDeviceEx( params );
    // initialise joystick support
    irr::core::array<irr::SJoystickInfo> gamePadArray;
    if ( ! pDevice->activateJoysticks( gamePadArray ) )
        std::cout << "GamePad initialisation failed" << std::endl;
    else
        std::cout << "Number of joysticks connected: " << gamePadArray.size() << std::endl;
    // set return status
    if( pDevice )
    {
        // uncomment to set a resizable window - does not apply to full screen size
        // pDevice->setResizable( true );
        std::cout << "initIrrlicht() success." << std::endl;
        return true;
    }
    std::cout << "initIrrlicht() Error: could not initialise system. Check configuration files." << std::endl;
    return false;
}

void GameMgr::changeState( IState* state )
{
    if ( state == NULL )
    {
        return;
    }
    // if there is an existing active state - free it.
    if ( ! stateStack.isEmpty() )
    {
        stateStack.top()->freeResources();
        stateStack.pop();

    }
    stateStack.push( state );
    pCurrentState = state;

    stateStack.top()->initState( *this );
    cout << "GameMgr changeState()." << endl;
}

void GameMgr::popState()
{
    // cleanup the current state
    if ( ! stateStack.isEmpty() )
    {
        stateStack.top()->freeResources();
        stateStack.pop();
    }
    // resume previous state
    if ( ! stateStack.isEmpty() )
    {
        stateStack.top()->resume();
        pCurrentState = stateStack.top();
        stateStack.top()->initState( *this );
    }
}

void GameMgr::pushState( IState* state )
{
    /// pause current state
    if ( ! stateStack.isEmpty() )
    {
        stateStack.top()->pause();
        stateStack.top()->freeResources();
    }
    /// store and init the new state
    stateStack.push( state );
    pCurrentState = state;
    stateStack.top()->initState( *this );

}

void GameMgr::run()
{
    // while the irrlicht device is active
    // note that the run function also provides event processing
    // running is set true in initGameMgr() when all is OK
    while ( running )
        // and it is the OS window that has focus and not std::cout << minimised
        if ( getDevice()->run() && pDevice->isWindowActive() )
            // and there is an active init'd level
            if ( pCurrentState )
                pCurrentState->update( );

}

void GameMgr::freeResources()
{
    // ensure freeResources() is called if a state is still open
    if ( pCurrentState )
        pCurrentState->freeResources( );
    // delete sound engine
    if ( pSound )
        pSound->drop();
    pSound = NULL;
    pDevice->closeDevice();
    pDevice->drop();
    delete pDataMgr;
    if( playerObject ) delete playerObject;
    cout << "GameMgr freeResources() success." << endl;
}

bool GameMgr::OnEvent( const SEvent& event )
{
    if ( pDevice )
    {
        if( event.EventType == EET_KEY_INPUT_EVENT )
        {
            pCurrentState->keyboardEvent( event );
        }
        // Pass input down to the specific game state mouse handler
        if ( event.EventType == EET_MOUSE_INPUT_EVENT )
        {
            pCurrentState->mouseEvent( event );
        }
        else if ( event.EventType == EET_GUI_EVENT )
        {
            pCurrentState->mouseGUIEvent( event );
        }
    }
    return false;   // interface compliance and/or pDevice NULL
}

void GameMgr::clearKeys()
{
    for ( int x = 0; x < KEY_KEY_CODES_COUNT; ++x )
        keys[ x ] = false;
}

bool GameMgr::getKey( unsigned char key )
{
    return keys[ key ];
}

std::string GameMgr::getDataStore( std::string name )
{
    // if can not find requested name - return empty string
    if ( dataStore.find( name ) == dataStore.end() )
    {
        std::cout << "getDataStore() failed: Could not find " << name << std::endl;
        return "";
    }
    return dataStore.find( name )->second;
}

bool GameMgr::isNameInDataStore( std::string name )
{
    if ( dataStore.find( name ) == dataStore.end() )
        return false;
    return true;
}

void GameMgr::addDataStore( std::string name, std::string data )
{
    dataStore[ name ] = data;
}

void GameMgr::clearDataStore()
{
    dataStore.clear();
}

// sound functions
bool GameMgr::initSound()
{
    if ( (  pSound = irrklang::createIrrKlangDevice() ) )
    {
        std::cout << "GameMgr::initSound() success" << std::endl;
        this->setVolume( 1.0f );
        soundAvailable = true;
        return true;
    }
    soundAvailable = false;
    std::cout << "*** GameMgr::initSound() failed" << std::endl;
    return false;
}

void GameMgr::updateSounds(
    core::vector3df camPos,
    core::vector3df camLookAt,
    core::vector3df velocity,
    core::vector3df upVector )
{
    pSound->setListenerPosition( camPos, camLookAt, velocity, upVector );
    // default velPerSecond = vec3df(0, 0, 0),
    // and upVector = vec3df(0, 1, 0)
}

irrklang::ISound* GameMgr::play3DSound( const char* fileName, irr::core::vector3df position, bool loop )
{
    irrklang::ISound* rtn = NULL;
    // true = start paused, true = tracking, true = sound effects
    rtn = pSound->play3D( fileName, ( irrklang::vec3df ) position, loop, true, false, irrklang::ESM_AUTO_DETECT, true );
    return rtn;
}

void GameMgr::stopSound( const char* fileName )
{
    pSound->removeSoundSource( fileName );
}

void GameMgr::stopAllSounds()
{
    pSound->removeAllSoundSources( );
}

void GameMgr::setVolume( float volume )
{
    pSound->setSoundVolume( volume );
}


