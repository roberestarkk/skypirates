#ifndef IRR_PAUSEGUI_H
#define IRR_PAUSEGUI_H

#include <string>
#include <map>
#include <vector>
#include <iostream>
using std::cout;
using std::endl;

#include "irrlicht.h"
// everything in the Irrlicht Engine is in the 'irr' namespace
using namespace irr;
// the 5 sub namespaces of the 'irr' namespace
//using namespace core;
//using namespace scene;
//using namespace video;
//using namespace io;
//using namespace gui;
// load the irrklang sound library

// triangle selectors for collisions
// #include "Irr_Utils.h"
// string handling functions and string <-> number conversions
// #include "Utils.h"
// stub class for game engine
#include "GameMgr.h"

/** this class uses the Singleton pattern to ensure there is only ever one instance.<br>
  Derives from IState to allow creation and control by the IrrGameMgr and is
  for testing a single level or game state. <br>
  Implements all IState's pure virtual functions.<br>
  initState() calls all the init...() functions. If all OK then update() and
  the event processing functions control the level.
    */
class PauseGUI : public IState
{
private:
    // declarations of objects/data only used for this demo class
//    //! custom mouse cursor - not used here with the FPS camera
//    video::ITexture* mouseCursor;
    //! need at least one camera except for 2D only scenes
    scene::ICameraSceneNode* pCamera;


    video::ITexture*bgImage;

    gui::IGUIInOutFader* fader;
    bool readXMLData( std::string fileName );



    // class ref. to the gui objects for interaction
    gui::IGUIButton* cmdResume;
    gui::IGUIButton* cmdOptions;
    gui::IGUIButton* cmdExitGame;
    gui::IGUIButton* cmdExitWindows;


    // active GUI elements need an ID
    enum
    {
        GUI_CMD_RESUME,
        GUI_CMD_OPTIONS,
        GUI_CMD_EXITGAME,
        GUI_CMD_EXITWINDOWS,

    };


    //storage for the resolution bpp and render engine data
    std::map<std::string, std::vector<std::string> > settingsMap;
    // read the xml for the gui componement data
    bool readXMLSettings( std::string fileName );

    void loadMouseCursor( );
    /** @brief displayMouse
      * show the mouse image at the x y user moves to.
      */



public:
    //! Initialise the game objects. Calls the private init...() functions.
    /** The game manager class does the device initialisation as there should
    only be a single reference to each of the Irrlicht devices
    \param game the local game manager reference
    \return false if there are any initialisation errors */
    bool initState( GameMgr& game );

    //! free any resources on the heap
    /** delete any objects created by new and drop any Irrlicht objects
    made by functions beginning with create, such as createDevice() */
    void freeResources( );

    //! The main game loop update, call any other update functions from here.
    /** Anything can be drawn between a beginScene() and endScene() call.
    The beginScene clears the screen with a colour and depth buffer if wanted.
    The Scene Manager and the GUI Environment then draw their content.
    The endScene() call presents everything on to the screen. */
    void update();

    //! timer pause
    void pause();
    //! timer resume
    void resume();

    //! singleton class creation
    static PauseGUI* getInstance()
    {
        return &instance;
    }

private:
    //! static instance reference used to enforce singleton pattern
    static PauseGUI instance;

    //! inline private ctor
    /** prevents "accidental" attempts to create an instance of the class */
    PauseGUI( ) {}

    //! inline private non virtual dtor
    /** Inheritance is not allowed. This class is controlled by the game manager */
    ~PauseGUI()
    {
        cout << "PauseGUI deleted" << endl;
    }

    //! inline private copy ctor
    /** prevent "accidental" copies */
    PauseGUI( const PauseGUI& );

    //! inline private assign operator
    /** prevent "accidental" assignments */
    PauseGUI& operator=( const PauseGUI& );

    //! init any general data, timers player/enemy/weapon states
    /** \return false if there any initialisation errors */
    bool initData();


    //! Init animated meshes and textures etc
    /** \return false if there any initialisation errors */
    bool initNodes();


    //! init any gui or other 2D elements
    /** \return false if there any initialisation errors */
    bool initGUIComponents();

    //! init any gui data - after the gui objects are created
    /** \return false if there any initialisation errors */
    bool initGUIData();


    //! init the camera/s required for this level
    /** \return false if there any initialisation errors */
    bool initCameras();

    //! init the sky, terrain, static meshes, lights etc
    /** \return false if there any initialisation errors */
    bool initWorld();

    //! init the sky, terrain, static meshes, lights etc
    /** \return false if there any initialisation errors */
    bool initGUISkin();


    //! key array to track which keys are down at any time
    /** \param KEY_KEY_CODES_COUNT: Irrlicht provided OS dependant constant
    for the number of keys available. */
    bool keys[ irr::KEY_KEY_CODES_COUNT ];

    //! key events passed on from OnEvent() in the game manager
    /** \param the event passed from Irrlicht's event handler */
    void keyboardEvent( SEvent event );

    //! mouse events passed on from OnEvent()
    /** \param the event passed from Irrlicht's event handler */
    void mouseEvent( SEvent event );

    //! a mouse event happened on a GUI object
    /** \param the event passed from Irrlicht's event handler */
    void mouseGUIEvent( SEvent event );

    // the following are only required for the debug display
    //! show FPS, poly count, active camera position and time
    void displayDebugData();
    //! if true show the debug displaythe
    bool showDebugData;
    //! the debug display on a label
    gui::IGUIStaticText* lblDebugData;

    // the following are only required for the level timer.
    // Physics engines require a (usually provided) fixed timer
    //! update local time keeper
    void updateTimer();
    //! Stores the complete time that has passed for this level
    unsigned int gameTime;
    //! the actual time (milliseconds) taken to render a scene
    unsigned int timeDelta;
    //! milliseconds since last frame - class member as need to adjust for pause / resume
    unsigned int timeLast;
    //! time now - class member as need to adjust for pause / resume
    unsigned int timeNow;
    //! desired render cycles per second
    unsigned int desiredFPS;

};

#endif // IRR_TEMPLATE_H


























