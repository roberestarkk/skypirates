#include "Player.h"

bool Player::isDead()
{
    if( dead )
        return true;
    else
        return false;
}

void Player::reload()
{
    // Test which weapon is selected in order to determine which maximum clip size to use and which ammo total to deduct from.
    switch ( weaponSelected )
    {
    case weaponRifle:
        // If the current clip size is less than the preset static value representing the maximum size of the clip,
        // then deduct the necessary ammunition from the total ammunition count and add it to the current clip so that it is full.
        if( Rifle.currentClip < MAX_CLIP_RIFLE && Rifle.totalAmmo > 0 )
        {
            int temp;
            temp = MAX_CLIP_RIFLE - Rifle.currentClip;
            Rifle.totalAmmo -= temp;
            Rifle.currentClip = MAX_CLIP_RIFLE;
        }
        break;
    case weaponPistol:
        if( Pistol.currentClip < MAX_CLIP_PISTOL && Pistol.totalAmmo > 0 )
        {
            int temp;
            temp = MAX_CLIP_PISTOL - Pistol.currentClip;
            Pistol.totalAmmo -= temp;
            Pistol.currentClip = MAX_CLIP_PISTOL;
        }
        break;
    case weaponGatling:
        if( Gatling.currentClip < MAX_CLIP_GATLING && Gatling.totalAmmo > 0 )
        {
            int temp;
            temp = MAX_CLIP_GATLING - Gatling.currentClip;
            Gatling.totalAmmo -= temp;
            Gatling.currentClip = MAX_CLIP_GATLING;
        }
        break;
    case weaponBlunderbuss:
        if( Blunderbuss.currentClip < MAX_CLIP_BLUNDERBUSS && Blunderbuss.totalAmmo > 0 )
        {
            int temp;
            temp = MAX_CLIP_BLUNDERBUSS - Blunderbuss.currentClip;
            Blunderbuss.totalAmmo -= temp;
            Blunderbuss.currentClip = MAX_CLIP_BLUNDERBUSS;
        }
        break;

    default:
        break;
    }

}

bool Player::isHit( core::line3d<f32> trajectory )
{
    //TODO - test trajectory for collision with player mesh
    // Already happening elsewhere in code.
    return true;
}

std::string Player::getName()
{
    return name;
}

int Player::getID()
{
    return playerID;
}

core::vector3df Player::getPosition()
{
    return position;
}

core::vector3df Player::getRotation()
{
    return rotation;
}

bool Player::getTeam()
{
    return team;
}

int Player::getHealth()
{
    return health;
}

int Player::getAmmo()
{
    switch ( weaponSelected )
    {
    case weaponRifle:
        return Rifle.totalAmmo;
        break;
    case weaponPistol:
        return Pistol.totalAmmo;
        break;
    case weaponGatling:
        return Gatling.totalAmmo;
        break;
    case weaponBlunderbuss:
        return Blunderbuss.totalAmmo;
        break;

    default:
        return Rifle.totalAmmo;
        break;
    }
}

int Player::getClip()
{
    switch ( weaponSelected )
    {
    case weaponRifle:
        return Rifle.currentClip;
        break;
    case weaponPistol:
        return Pistol.currentClip;
        break;
    case weaponGatling:
        return Gatling.currentClip;
        break;
    case weaponBlunderbuss:
        return Blunderbuss.currentClip;
        break;

    default:
        return Rifle.currentClip;
        break;
    }
}

bool Player::getWeaponAvailability( int weaponTest )
{
    switch ( weaponTest )
    {
    case weaponRifle:
        return Rifle.weaponAvailability;
        break;
    case weaponPistol:
        return Pistol.weaponAvailability;
        break;
    case weaponGatling:
        return Gatling.weaponAvailability;
        break;
    case weaponBlunderbuss:
        return Blunderbuss.weaponAvailability;
        break;

    default:
        return Rifle.weaponAvailability;
        break;
    }
}

int Player::getSelectedWeapon()
{
    return weaponSelected;
}

void Player::setWeaponAvailability( int weapon, bool availability )
{
    switch ( weapon )
    {
    case weaponRifle:
        Rifle.weaponAvailability = availability;
        break;
    case weaponPistol:
        Pistol.weaponAvailability = availability;
        break;
    case weaponGatling:
        Gatling.weaponAvailability = availability;
        break;
    case weaponBlunderbuss:
        Blunderbuss.weaponAvailability = availability;
        break;

    default:
        break;
    }

}

void Player::takeDamage( int damageTaken )
{
    health -= damageTaken;
}

void Player::pickupAmmo( int ammoDrop, int weaponType )
{
    switch ( weaponType )
    {
    case weaponRifle:
        Rifle.totalAmmo += ammoDrop;
        break;
    case weaponPistol:
        Pistol.totalAmmo += ammoDrop;
        break;
    case weaponGatling:
        Gatling.totalAmmo += ammoDrop;
        break;
    case weaponBlunderbuss:
        Blunderbuss.totalAmmo += ammoDrop;
        break;

    default:
        break;
    }

}

void Player::pickupHealth( int healthDrop )
{
    health += healthDrop;
    if(health>MAX_HEALTH)
    {
        health = MAX_HEALTH;
    }
}

void Player::pickupFlag( int newFlagVal )
{
    carryingFlag = true;
}

void Player::dropFlag()
{
    carryingFlag = false;
}


void Player::setName( std::string playerName )
{
    name = playerName;
}

void Player::setID( int id )
{
    playerID = id;
}

void Player::setDead( bool hasDied )
{
    dead = hasDied;
}

void Player::setTeam( bool onTeam )
{
    team = onTeam;
}

void Player::setAmmo( int currentAmmo )
{
    switch ( weaponSelected )
    {
    case weaponRifle:
        Rifle.totalAmmo = currentAmmo;
        break;
    case weaponPistol:
        Pistol.totalAmmo = currentAmmo;
        break;
    case weaponGatling:
        Gatling.totalAmmo = currentAmmo;
        break;
    case weaponBlunderbuss:
        Blunderbuss.totalAmmo = currentAmmo;
        break;
    default:
        break;
    }
}

void Player::setPosition( core::vector3df newPos )
{
    position = newPos;
}

void Player::setRotation( core::vector3df newRot )
{
    rotation = newRot;
}

void Player::setSelectedWeapon( int selection )
{
    weaponSelected = selection;
}

void Player::setClip( int newClip )
{
    switch ( weaponSelected )
    {
    case weaponRifle:
        Rifle.currentClip = newClip;
        break;
    case weaponPistol:
        Pistol.currentClip = newClip;
        break;
    case weaponGatling:
        Gatling.currentClip = newClip;
        break;
    case weaponBlunderbuss:
        Blunderbuss.currentClip = newClip;
        break;
    default:
        break;
    }
}



io::path Player::getWeaponMesh( int weaponSelected )
{
    switch ( weaponSelected )
    {
    case weaponRifle:
        return Rifle.weaponModelMesh;
        break;
    case weaponPistol:
        return Pistol.weaponModelMesh;
        break;
    case weaponGatling:
        return Gatling.weaponModelMesh;
        break;
    case weaponBlunderbuss:
        return Blunderbuss.weaponModelMesh;
        break;

    default:
        return "No Mesh Here, Sorry.";
        break;
    }

}

io::path Player::getWeaponTexture( int weaponSelected )
{
    switch ( weaponSelected )
    {
    case weaponRifle:
        return Rifle.weaponModelTexture;
        break;
    case weaponPistol:
        return Pistol.weaponModelTexture;
        break;
    case weaponGatling:
        return Gatling.weaponModelTexture;
        break;
    case weaponBlunderbuss:
        return Blunderbuss.weaponModelTexture;
        break;

    default:
        return "No Texture Here, Sorry.";
        break;
    }

}

std::string Player::getWeaponName( int weaponSelected )
{
    switch ( weaponSelected )
    {
    case weaponRifle:
        return Rifle.weaponName;
        break;
    case weaponPistol:
        return Pistol.weaponName;
        break;
    case weaponGatling:
        return Gatling.weaponName;
        break;
    case weaponBlunderbuss:
        return Blunderbuss.weaponName;
        break;

    default:
        return "No Name Here, Sorry.";
        break;
    }

}
