#include "Entity.h"

//{ Setters
void Entity::setPosition( core::vector3df newPos )
{
    position = newPos;
}

void Entity::setRotation( core::vector3df newRot )
{
    rotation = newRot;
}

void Entity::setType( char newType )
{
    type = newType;
}

void Entity::setSubtype( char newSubtype )
{
    subtype = newSubtype;
}

void Entity::setValue( int newVal )
{
    value = newVal;
}

void Entity::setEntityMesh( scene::IMeshSceneNode* newMesh )
{
    entityMesh = newMesh;
}

void Entity::setEntityTimer(unsigned int newTimer)
{
    respawnTime = newTimer;
}

//}


//{ Getters
core::vector3df Entity::getPosition()
{
    return position;
}

core::vector3df Entity::getRotation()
{
    return rotation;
}

char Entity::getType()
{
    return type;
}

char Entity::getSubtype()
{
    return subtype;
}

int Entity::getValue()
{
    return value;
}

scene::IMeshSceneNode* Entity::getEntityMesh()
{
    return entityMesh;
}

unsigned int Entity::getEntityTimer()
{
    return respawnTime;
}

//}

