#ifndef PLAYER_H
#define PLAYER_H

#include <string>
#include <iostream>
using std::cout;
using std::endl;

#include "irrlicht.h"
using namespace irr;

#include "IrrUtils.h"
#include <NetworkIDObject.h>


/** \brief A class used to store Player-related data. An object of this class type is created in GameMgr.h and is therefore accessible across all game states.
 */
class Player : public RakNet::NetworkIDObject
{
protected:
    //! The name of the Player.
    std::string name;
    //! The ID of the Player.
    int playerID;
    //! The location of the mesh file for the Player.
    io::path playerModelMesh;
    //! The location of the texture file for the Player.
    io::path playerModelTexture;
    //! The position of the Player.
    core::vector3df position;
    //! The rotation of the Player.
    core::vector3df rotation;
    //! Whether the Player is considered 'dead' or not(bool).
    bool dead;

    //! An enumeration of the various powerups available to be applied to the Player.
    enum Powerups
    {
        powerup1,
        powerup2,
        powerup3,
    };

    //! An enumeration of the various weapons the player could have selected.
    enum WeaponSelection
    {
        weaponRifle = 1,
        weaponPistol,
        weaponGatling,
        weaponBlunderbuss,
    };


public:
    /** Default inline constructor */

    //! Initialises 'constants' which aren't actual constants, but will not change past this point.

    Player()
    {
        Rifle.weaponID = 1;
        Rifle.weaponAvailability = true;
        Rifle.weaponName = "Rifle";
        Rifle.maximumClip = MAX_CLIP_RIFLE;
        Rifle.weaponModelMesh = "media/meshes/reconfiguredAnimations/rifle.b3d";
        Rifle.weaponModelTexture = "media/textures/rifleTexture.png";

        Pistol.weaponID = 2;
        Pistol.weaponAvailability = false;
        Pistol.weaponName = "Pistol";
        Pistol.maximumClip = MAX_CLIP_PISTOL;
        Pistol.weaponModelMesh = "media/meshes/reconfiguredAnimations/pistols.b3d";
        Pistol.weaponModelTexture = "media/textures/pistolTexture.png";

        Gatling.weaponID = 3;
        Gatling.weaponAvailability = false;
        Gatling.weaponName = "Gatling";
        Gatling.maximumClip = MAX_CLIP_GATLING;
        Gatling.weaponModelMesh = "media/meshes/reconfiguredAnimations/gattling.b3d";
        Gatling.weaponModelTexture = "media/textures/gattlingTexture.png";

        Blunderbuss.weaponID = 4;
        Blunderbuss.weaponAvailability = false;
        Blunderbuss.weaponName = "Blunderbuss";
        Blunderbuss.maximumClip = MAX_CLIP_BLUNDERBUSS;
        Blunderbuss.weaponModelMesh = "media/meshes/reconfiguredAnimations/blunderbuss.b3d";
        Blunderbuss.weaponModelTexture = "media/textures/blunderbussTexture.png";

        Imperial = true;
        Rebel = false;
    }
    /** Default inline destructor */
    virtual ~Player() {}

    //! A constant value representing the clip size of the Rifle.
    const static int MAX_CLIP_RIFLE = 8;
    //! A constant value representing the clip size of the Pistol.
    const static int MAX_CLIP_PISTOL = 16;
    //! A constant value representing the clip size of the Gattling Gun.
    const static int MAX_CLIP_GATLING = 100;
    //! A constant value representing the clip size of the Blunderbuss.
    const static int MAX_CLIP_BLUNDERBUSS = 6;
    //! A constant value representing the maximum amount of health the player can have.
    const static int MAX_HEALTH = 150;

    //{ Different Teams
    //! Whether the Player is on team 0 or 1 (false or true).
    bool team;

    //! A Boolean Value, set to true in the constructor and never changed again.
    bool Imperial;
    //! A Boolean Value, set to false in the constructor and never changed again.
    bool Rebel;
    //}

    //! A variable to hold the Player's total health.
    int health;
    //! A variable to hold the Player's total available ammunition.
    int ammo;
    //! A variable to hold the current amount of ammunition in the Player's clip.
    int clip;
    //! A variable to hold a representation of which weapon the Player currently has selected.
    int weaponSelected;

    //! A variable to hold a representation of whether the Player currently has a flag.
    bool carryingFlag;
    //! A variable to hold the value of the currently held flag.
    int flagValue;

    /** \brief A function to return whether or not the Player is 'in a state of death'.
     * \return True if player is dead, else false.
     */
    bool isDead();
    /** \brief A subroutine to be called when the player's clip is empty, or when a key is pressed.
     * Subtracts enough from the total ammunition to refill the current clip size to it's maximum.
     * \return NULL.
     */
    void reload();
    /** \brief A function to determine whether a certain projectile collides with the player.
     * \param trajectory of the projectile in the form of a core::line3d<f32>.
     * \return whether or not it hit in the form of a boolean.
     *
     */
    bool isHit( core::line3d<f32> trajectory );
    /** \brief A function to return the Player's name
     * \return The Player's name in a std::string.
     */
    std::string getName();
    /** \brief A function to return the Player's ID.
     * \return The Player's ID in int form.
     */
    int getID();
    /** \brief A function to return the Player's position.
     * \return The Player's position, in point form, as a core::vector3df.
     */
    core::vector3df getPosition();
    /** \brief A function to return the Player's rotation(Absolute).
     * \return The Player's Absolute rotation as a core::vector3df
     */
    core::vector3df getRotation();
    /** \brief A function to return the Player's team.
     * \return Whether the Player's team is team true or false as a bool.
     */
    bool getTeam();
    /** \brief A function to return the Player's health.
     * \return Player's current health as an int.
     */
    int getHealth();
    /** \brief A function to return the Player's current weapon's total Ammunition.
     * \return The Player's overall ammunition for the currently selected weapon as int.
     */
    int getAmmo();
    /** \brief A function to return the Player's current weapon's current clip.
     * \return The Player's current clip-size for the currently selected weapon as an int.
     */
    int getClip();
    /** \brief A function to return whether the Weapon being tested is valid for use by the Player.
     * \param weaponTest as an int, to be used to specify which Weapon's validity is being tested
     * \return the tested Weapon's validity for use as a bool.
     */
    bool getWeaponAvailability(int weaponTest);
    /** \brief A subroutine to apply availability(or lack thereof) to a specific weapon.
     * \param weapon as an int, the numeric identifier of a certain weapon.
     * \param availability as a bool, the new availability of the previously specified weapon.
     */
    void setWeaponAvailability(int weapon, bool availability);
    /** \brief A subroutine to apply damage to the Player.
     * \param damageTaken as an int, removed directly from the health variable.
     */
    void takeDamage( int damageTaken );
    /** \brief A subroutine to increase the Player's avaibale ammunition for the selected weapon.
     * \param ammoDrop as an int, to be added to the total ammunition of the [weaponType].
     * \param weaponType as an int, the number which denotes which type of weapon ammo to increase.
     */
    void pickupAmmo( int ammoDrop, int weaponType );
    /** \brief A subroutine to increase the Player's available Health.
     * \param healthDrop as an int, to be added to the total health of the Player.
     */
    void pickupHealth( int healthDrop );
    /** \brief A subroutine to specify that a flag has been acquired, and how much it is worth
     * \param newFlagVal as an int, to be used to increment the score when the player reaches their own flag.
     */
    void pickupFlag( int newFlagVal );
    /** \brief A subroutine to specify that the player no-longer has the enemy flag.
     * \param newFlagVal as an int, to increment the player's score if dropped within the confines of the target area.
     */
    void dropFlag();
    /** \brief A subroutine to set the Player's name.
     */
    void setName( std::string playerName );
    /** \brief A subroutine to set the Player's ID.
     * \param id as an int.
     */
    void setID( int id );
    /** \brief A subroutine to set whether the Player has died.
     * \param hasDied as a bool.
     */
    void setDead( bool hasDied );
    /** \brief A subroutine to set the Player's team.
     * \param onTeam as a bool.
     */
    void setTeam( bool onTeam );
    /** \brief A subroutine to set the Player's total available ammunition for the selected weapon.
     * \param currentAmmo of the current weapon as an int.
     */
    void setAmmo( int currentAmmo );
    /** \brief A subroutine to set the Player's position.
     * \param newPos as a core::vector3df.
     */
    void setPosition( core::vector3df newPos );
    /** \brief A subroutine to set the Player's rotation.
     * \param newRot as a core::vector3df.
     */
    void setRotation( core::vector3df newRot );
    /** \brief A function to return the Player's currently selected weapon.
     * \return int representation of the selected weapon (use the WeaponSelection enum to associate the number with an intrinsic weapon name).
     */
    int getSelectedWeapon();
    /** \brief A subroutine to set the Player's currently selected weapon.
     * \param selection as an int (use the WeaponSelection enum to associate the number with an intrinsic weapon name).
     */
    void setSelectedWeapon( int selection );
    /** \brief A subroutine to set the Player's currently selected weapon's clip size.
     * \param newClip int
     */
    void setClip( int newClip );

    /** \brief A structure containing information specific to each weapon. Each weapon will have an object of this class type created under it's name.
     */
    struct Weapon
    {
        //! A variable to hold the weapon's ID.
        int weaponID;
        //! A variable to hold the weapon's Name.
        std::string weaponName;
        //! A variable to hold whether they player has access to the weapon.
        bool weaponAvailability;
        //! A variable to hold the weapon's (total)Ammunition.
        int totalAmmo;
        //! A variable to hold the weapon's clip size (How much ammo is in the clip).
        int currentClip;
        //! A variable to delineate the maximum value that can be held within a clip (currentClip).
        int maximumClip;
        //! A variable to hold the system path to the weapon's Mesh.
        io::path weaponModelMesh;
        //! A variable to hold the system path to the weapon's Texture.
        io::path weaponModelTexture;
    };

    // Instantiation of the Weapon Struct's as discrete Objects.
    //! An instance of the Weapon struct for the Rifle weapon.
    Weapon Rifle;
    //! An instance of the Weapon struct for the Pistol weapon.
    Weapon Pistol;
    //! An instance of the Weapon struct for the Gattling weapon.
    Weapon Gatling;
    //! An instance of the Weapon struct for the Blunderbuss weapon.
    Weapon Blunderbuss;

    /** \brief A function to return the filepath of the selected weapon's mesh.
     * \param weaponSelected as an int.
     * \return filepath of the weapon's mesh as a std::string.
     */
    io::path getWeaponMesh( int weaponSelected );
    /** \brief A function to return the filepath of the selected weapon's texture.
     * \param weaponSelected as an int.
     * \return filepath of the weapon's mesh as a std::string.
     */
    io::path getWeaponTexture( int weaponSelected );
    /** \brief A function to return the name of the selected weapon.
     * \param weaponSelected as an int.
     * \return the weapon's name as a std::string.
     */
    std::string getWeaponName( int weaponSelected );

};

#endif // PLAYER_H
