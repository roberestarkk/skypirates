#ifndef GAME_HELP
#define GAME_HELP

#include <string>
#include <iostream>
using std::cout;
using std::endl;

#include "irrlicht.h"
#include "irrklang.h"
// everything in the Irrlicht Engine is in the 'irr' namespace
using namespace irr;
using namespace irrklang;

// string handling functions and string <-> number conversions
#include "Utils.h"
// game manager
#include "GameMgr.h"
#include "DataMgr.h"
#include "StartMenu.h"

/** this class uses images for "buttons"
Shows navigation to another state/level from the current one.
No state stack is used and only one state is active at any time.
Each state will need to know which state comes next or previously if it needs to return. */
class GameHelp : public IState
{
private:

    gui::IGUIImage * imgExitDown;
    gui::IGUIImage * imgExitUp;
    video::ITexture* imgBg;          // bg image
    gui::IGUIInOutFader* fader;         // start/finish fader after resource loading

    scene::ICameraSceneNode* pCamera;

    ISound *music;

    //! mouse cursor image
    video::ITexture* mouseCursor;
    //! mouse position - set in update
    core::position2d<s32> mousePos;
    /** @brief displayMouse
      * show the mouse image at the x y user moves to.
      */
    void displayMouse( );
    /** @brief mouseClicked
    * find out which button image was clicked on
    * and act upon it
    */
    void mouseClicked( );
    /** @brief mouseOver
      * set button images if mouse is over a button
      */
    bool mouseOver( );
    /** @brief resetButtons
      * show the image buttons in their starting state
      */
    void resetButtons( );
    /** @brief loadMouseCursor
      * the image to use for the mouse cursor
      */
    void loadMouseCursor( );

public:
    //! Initialise the game objects. Calls the private init...() functions.
    /** The State Manager class will take over most of the device initialisation
    as there should only be a single reference to each of the Irrlicht devices
    \param the title for the window
    \return false if there are any initialisation errors */
    bool initState( GameMgr& game );

    //! free any resources on the heap
    /** delete any objects created by new and drop any Irrlicht objects
    made by functions beginning with create, such as createDevice() */
    void freeResources( );

    //! The main game loop update, call any other update functions from here.
    /** Anything can be drawn between a beginScene() and endScene() call.
    The beginScene clears the screen with a color and depth buffer if wanted.
    The Scene Manager and the GUI Environment then draw their content.
    The endScene() call presents everything on the screen. */
    void update();

    //! timer pause
    void pause();
    //! timer resume
    void resume();

    //! singleton class creation
    static GameHelp* getInstance()
    {
        return &instance;
    }

private:
    //! static instance reference
    static GameHelp instance;

    //! inline private ctor
    /** prevent "accidental" attempts to create an instance of the class */
    GameHelp( )
    {
        cout << "GameHelp created" << endl;    // ctor
    }

    //! inline private non virtual dtor
    /** Inheritance is not allowed.
    This class kills itself in freeResources() */
    ~GameHelp()
    {
        cout << "GameHelp deleted" << endl;
    }

    //! inline private copy ctor
    /** prevent "accidental" copies */
    GameHelp( const GameHelp& );

    //! inline private assign operator
    /** prevent "accidental" assignments */
    GameHelp& operator=( const GameHelp& );

    //! init any general data, timers player/enemy/weapon states
    /** \return false if there any initialisation errors */
    bool initData();

    //! Init animated meshes and textures etc
    /** \return false if there any initialisation errors */
    bool initNodes();

    //! init any gui or other 2D elements
    /** \return false if there any initialisation errors */
    bool initGUISkin();

    //! init any gui or other 2D elements
    /** \return false if there any initialisation errors */
    bool initGUIComponents();

    //! init any gui data - after the gui objects are created
    /** \return false if there any initialisation errors */
    bool initGUIData();

    //! init the camera/s required for this level
    /** \return false if there any initialisation errors */
    bool initCameras();

    //! init the sky, terrain, static meshes, lights etc
    /** \return false if there any initialisation errors */
    bool initWorld();

    //! key events passed on from OnEvent()
    /** \param the event passed from Irrlicht's event handler */
    void keyboardEvent( SEvent event );

    //! mouse events passed on from OnEvent()
    /** \param the event passed from Irrlicht's event handler */
    void mouseEvent( SEvent event );

    //! a mouse event happened on a GUI object
    /** \param the event passed from Irrlicht's event handler */
    void mouseGUIEvent( SEvent event );

    //! show FPS, poly count, active camera position and time
    void debugData();

    //! the debug display
    void displayDebugData();
    //! if true show the debug display
    bool showDebugData;
    //! the debug display on a label
    gui::IGUIStaticText* lblDebugData;

    //! update local time keeper
    void updateTimer();
    //! Stores the complete time that has passed for this level
    unsigned int gameTime;
    //! the actual time taken to render a scene
    unsigned int timeDelta;
    //! msecs since last frame - will need to adjust for pause / resume
    unsigned int timeLast;
    //! time now - will need to adjust for pause / resume
    unsigned int timeNow;
    //! desired render cycles per second
    unsigned int desiredFPS;

};

#endif // IRR_TEMPLATE_H


























