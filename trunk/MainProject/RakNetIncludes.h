#ifndef RAKNETINCLUDES_H_INCLUDED
#define RAKNETINCLUDES_H_INCLUDED

#include "MessageIdentifiers.h"
#include "RakPeerInterface.h"
#include "RakNetTypes.h"
#include "GetTime.h"
#include "BitStream.h"
#include <assert.h>
#include <cstdio>
#include <cstring>
#include <stdlib.h>
#include <string.h>
#include "RakSleep.h"
#include "Gets.h"
#include <stdio.h>
#include "Kbhit.h"
#include "ReplicaManager3.h"
#include "NetworkIDManager.h"
#include "FormatString.h"
#include "RakString.h"
#include "SocketLayer.h"
#include "Getche.h"
#include "Rand.h"
#include "VariableDeltaSerializer.h"
#include "FullyConnectedMesh2.h"
#include "ConnectionGraph2.h"

#include "PacketLogger.h"

#define MAX_CLIENTS 2
#define SERVER_PORT 60000

enum GameMessages
{
    ID_GAME_MESSAGE_1 = ID_USER_PACKET_ENUM + 1,
    ID_GAME_MESSAGE_2 = ID_USER_PACKET_ENUM + 2,
    ID_GAME_MESSAGE_3 = ID_USER_PACKET_ENUM + 3,
    ID_GAME_MESSAGE_4 = ID_USER_PACKET_ENUM + 4,
    ID_GAME_MESSAGE_5 = ID_USER_PACKET_ENUM + 5,
    ID_GAME_MESSAGE_6 = ID_USER_PACKET_ENUM + 6,
    ID_GAME_MESSAGE_START_GAME = ID_USER_PACKET_ENUM + 7,
    ID_GAME_MESSAGE_CLIENT_COMMIT = ID_USER_PACKET_ENUM + 8,
};

#endif // RAKNETINCLUDES_H_INCLUDED
