#include "StartMenu.h"

//! define the static singleton pointer
GameHelp GameHelp::instance;

void GameHelp::update()
{
    this->updateTimer();
    pVideo->beginScene( true, true, video::SColor( 255, 100, 101, 140 ) );
    if ( imgBg )
        pVideo->draw2DImage(
            imgBg,
            core::rect< irr::s32 >( 0, 0, pGameMgr->getScreenResolution().Width, pGameMgr->getScreenResolution().Height ),
            core::rect< irr::s32 >( 0, 0, imgBg->getSize().Width, imgBg->getSize().Height ) );
    pSceneMgr->drawAll();
    pGUIEnv->drawAll();
    mouseOver( );
    displayMouse( );
    pVideo->endScene();
#ifdef _DEBUG
    if( showDebugData )
        this->displayDebugData();
#endif
}

bool GameHelp::initState( GameMgr& game )
{
    cout << "GameHelp initState() start" << endl;
    // init the local game engine reference
    pGameMgr = &game;
    pDataMgr = pGameMgr->getDataMgr();
    pDevice = pGameMgr->getDevice();
    // local references to the other Irrlicht system pointers
    pVideo = pDevice->getVideoDriver();
    pSceneMgr = pDevice->getSceneManager();
    pGUIEnv = pDevice->getGUIEnvironment();
    // set or disable system mouse cursor - false as drawing own cursor
    pDevice->getCursorControl()->setVisible( false );
    // load the mouse cursor image
    loadMouseCursor( );
    // init the keys array
    pGameMgr->clearKeys();
    // the order of the following function calls may be important
    // if there are dependencies so rearrange/remove as reqd.
    if ( not this->initData() )
        return false;
    if ( not this->initGUISkin() )
        return false;
    if ( not this->initGUIComponents() )
        return false;
    if ( not this->initGUIData() )
        return false;
    if ( not this->initWorld()    )
        return false;
    if ( not this->initNodes()    )
        return false;
    if ( not this->initCameras()  )
        return false;
    // msecs for the fade to complete
    fader->fadeIn( 3000 );
    cout << "GameHelp initState() success" << endl;
    return true;
}

bool GameHelp::initData()
{
    timeNow = timeLast = gameTime = timeDelta = 0;
    desiredFPS = 60;
    showDebugData = false;
    imgBg = NULL;
    cout << "GameHelp initData() success" << endl;
    return true;
}

bool GameHelp::initGUISkin()
{
    gui::IGUIFont* pFontGUI;
    //! load background image - it is drawn in the update function
    imgBg = pVideo->getTexture( "media/images/HelpImg.png" );
    //! set fader
    fader = pDevice->getGUIEnvironment()->addInOutFader();
    // the colour to start from
    fader->setColor( video::SColor( 0, 128, 0, 0 ) );
    //! font for text labels
    pFontGUI = pGUIEnv->getFont( "media/fonts/Perpetua24.bmp" );
    // skin renderers - EGST_WINDOWS_CLASSIC, EGST_WINDOWS_METALLIC or EGST_BURNING_SKIN
    gui::IGUISkin* pSkin = pGUIEnv->createSkin( gui::EGST_WINDOWS_METALLIC );
    // the default font - used unless the object overrides it
    pSkin->setFont( pFontGUI, gui::EGDF_DEFAULT );
    // not changing tooltip font so not keeping a reference to it
    pSkin->setFont( pGUIEnv->getFont( "media/fonts/ConsoleFont.bmp" ), gui::EGDF_TOOLTIP );
    // the GUI environment will manage the pointer
    pGUIEnv->setSkin( pSkin );
    // must drop() as used a create...() function, createSkin().
    pSkin->drop();
    cout << "GameHelp initGUISkin() success" << endl;
    return true;
}

bool GameHelp::initGUIComponents()
{
    //! set up some common variables for the gui components
    u32 scrnWidth = pGameMgr->getScreenResolution().Width;
    u32 scrnHeight = pGameMgr->getScreenResolution().Height;
    // starting values for the first component, the title
    // object start top, width and height
    signed topY = scrnHeight / 32, width = 480, height = 48;
    // centred on the screen
    signed leftX = ( scrnWidth - width ) / 2;
    // all components req. a dimension this is first used on the title
    core::rect<s32> sizer( leftX, topY, leftX + width, topY + height );
    //! set title text and position
    gui::IGUIStaticText* title;
    gui::IGUIStaticText* controls;
    // text, size, border, wordwrap, parent, id, background
    title = pGUIEnv->addStaticText ( L"SkyPirates Help Page.", sizer, false, false, 0, -1, false );
    // diferent colour and font for the title
    title->setOverrideColor( video::SColor( 255, 255, 255, 0 ) );
    title->setOverrideFont( pGUIEnv->getFont( "media/fonts/bigfont.png" ) );
    title->setTextAlignment( gui::EGUIA_CENTER, gui::EGUIA_CENTER );

    width = scrnWidth - (scrnWidth/5);
    height = scrnHeight - (scrnHeight/8);
    leftX = (scrnWidth/10);
    topY = (scrnHeight/242);
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    controls = pGUIEnv->addStaticText ( L"Controls: \n\t'W' or 'Up Arrow': Move Forward. \n\t'S' or 'Down Arrow': Move Backward. \n\t'A' or 'Left Arrow': Strafe Left. \n\t'D' or 'Right Arrow': Strafe Right.\n\t'Left Click' will fire your weapon.\n\t'R': Reloads your current weapon.\n\t'Space': Jump \n As always, moving the Mouse controls where you are looking.\n\nPickups:\n\tWeapon: If you pick up a floating weapon. You will unlock that weapon. \n\t\tIf you already have that weapon, you will gain ammunition for it.\n\tHealth: If you pick up a floating red cross, your health will increase by 50.\n\t\tThe highest your health can get is 150.", sizer, false, false, 0, -1, false );
    // diferent colour and font for the controls
    controls->setOverrideColor( video::SColor( 255, 0, 0, 255 ) );
    controls->setOverrideFont( pGUIEnv->getFont( "media/fonts/Times16.png" ) );
    controls->setTextAlignment( gui::EGUIA_CENTER, gui::EGUIA_CENTER );

    // set up images for "button" images 256x64
    width = 256;
    height = 64;
    leftX = ( scrnWidth - width ) / 2;
    topY = scrnHeight - height * 2;
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    imgExitUp = pGUIEnv->addImage( sizer );
    imgExitUp->setImage( pVideo->getTexture( "media/textures/InGameButtons/back.png" ) );
    imgExitUp->setUseAlphaChannel( true );
    imgExitDown = pGUIEnv->addImage( sizer );
    imgExitDown->setImage( pVideo->getTexture( "media/textures/InGameButtons/back.png" ) );
    imgExitDown->setUseAlphaChannel( true );
    //! debug data display
    lblDebugData = pGUIEnv->addStaticText(
                       L"",
                       core::rect<s32>( 0, scrnHeight - 64, 256, scrnHeight ),
                       true,
                       true,
                       0,
                       -1,
                       true );
    lblDebugData->setOverrideColor( video::SColor( 255, 255, 255, 0 )  );
    // starting off invisible for this - no poly count
    lblDebugData->setVisible( false );
    cout << "GameHelp initGUIComponents() success" << endl;
    return true;
}

bool GameHelp::initGUIData()
{
    std::cout << "GameHelp initGUIData() success" << std::endl;
    return true;
}

bool GameHelp::initNodes()
{
    return true;
}

bool GameHelp::initCameras()
{
    pCamera = pSceneMgr->addCameraSceneNode(
              );
    pCamera->setPosition( core::vector3df( 0, 0, 0 ) );
    pCamera->setTarget( core::vector3df( 0, 0, 1 ) );
    return true;
}

bool GameHelp::initWorld()
{
    return true;
}

void GameHelp::keyboardEvent( SEvent event )
{
    if ( event.KeyInput.PressedDown )
    {
        switch ( event.KeyInput.Key )
        {
        case irr::KEY_F2:
            break;
        case irr::KEY_F3:
            break;
        case irr::KEY_F4:
            break;
        default :
            break;
        }
    }
}

void GameHelp::mouseEvent( SEvent event )
{
    // mouse clicks
    if ( event.MouseInput.Event == EMIE_LMOUSE_LEFT_UP )
    {
        mouseClicked( );
    }
    if ( event.MouseInput.Event == EMIE_LMOUSE_PRESSED_DOWN )
    {
    }
}

void GameHelp::mouseGUIEvent( SEvent event )
{
//    s32 id = event.GUIEvent.Caller->getID();
}

void GameHelp::loadMouseCursor( )
{
    mouseCursor =  pVideo->getTexture( "media/cursors/pointercursor.png" );
    pVideo->makeColorKeyTexture( mouseCursor, core::position2d<s32>( 0, 0 ) );
    pDevice->getCursorControl()->setVisible( false );
}

void GameHelp::resetButtons( )
{
    // set highlighed button/s to off
    imgExitUp->setVisible( false );
    // set normal button/s on
    imgExitDown->setVisible( true );
}

bool GameHelp::mouseOver( )
{
    // resets if over another button
    resetButtons( );
    // get mouse position2d<s32>
    mousePos = pDevice->getCursorControl()->getPosition();
    // check if over Exit button
    if ( imgExitDown->getRelativePosition().isPointInside( mousePos ) )
    {
        imgExitUp->setVisible( true );
        imgExitDown->setVisible( false );
        return true;
    }
    return false;
}

void GameHelp::mouseClicked( )
{
    // get mouse position2d<s32>
    mousePos = pDevice->getCursorControl()->getPosition();
    // check if over Exit button
    if ( imgExitDown->getRelativePosition().isPointInside( mousePos ) )
        pGameMgr->changeState( StartMenu::getInstance() );
}

void GameHelp::displayMouse( )
{
    mousePos = pDevice->getCursorControl()->getPosition();
    pVideo->draw2DImage( mouseCursor, core::position2d<s32>( mousePos.X, mousePos.Y ),
                         core::rect<s32>( 0, 0, 16, 16 ), 0, video::SColor( 255, 255, 255, 255 ), true );
//    }
}

void GameHelp::freeResources( )
{
    // clear the gui environment unless there are elements to use in the next state
    pGUIEnv->clear();
    // same with the scene manager
    pSceneMgr->clear();
    cout << "GameHelp freeResources()" << endl;
}


void GameHelp::displayDebugData()
{
    char buffer[256]; // using sprintf for its easy formatting ability
    core::stringw wstr( "Debug - FPS: " );       // debug title
    wstr += core::stringw( pVideo->getFPS() );
    wstr += "\nPoly: ";
    wstr += core::stringw( pVideo->getPrimitiveCountDrawn() );
    // get camera position
//    if ( pCamera )
//    {
//        sprintf( buffer, "\nCam: X:%.0f Y:%.0f Z:%.0f",
//                 pCamera->getPosition().X,
//                 pCamera->getPosition().Y,
//                 pCamera->getPosition().Z );
//    }
//    else
    sprintf( buffer, "No Camera\n" );
    wstr += buffer;
    // get game time
    unsigned int seconds = gameTime / 1000;
    sprintf( buffer, "%02d:%02d:%02d", seconds / 60, seconds % 60, gameTime % 1000 );
    wstr += "\nTime: ";
    wstr += buffer;
    lblDebugData->setText( wstr.c_str() );
}

void GameHelp::updateTimer()
{
    timeNow = pDevice->getTimer()->getTime();
    timeDelta = timeNow - timeLast;
    timeLast = timeNow;
    gameTime += timeDelta;
}

void GameHelp::pause()
{
}

void GameHelp::resume()
{
}


























