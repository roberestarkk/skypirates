#include "GameSelection.h"

//! define the static singleton pointer
GameSelection GameSelection::instance;
using namespace irr;

void GameSelection::update()
{
    this->updateTimer();
    pVideo->beginScene( true, true, video::SColor( 255, 100, 101, 140 ) );
    if ( imgBg )
        pVideo->draw2DImage(
            imgBg,
            core::rect< irr::s32 >( 0, 0, pGameMgr->getScreenResolution().Width, pGameMgr->getScreenResolution().Height ),
            core::rect< irr::s32 >( 0, 0, imgBg->getSize().Width, imgBg->getSize().Height ) );
    pSceneMgr->drawAll();
    pGUIEnv->drawAll();

//cout<<"before packet"<<endl;
//    packet=peer->Receive();
//    cout<<"during packet"<<endl;
//        if (packet->data[0] == ID_ADVERTISE_SYSTEM)
//        {
//            printf("A system is Advertising to us!\n");
//            printf("The Advertising system is at: ");
//            printf(packet->systemAddress.ToString());
//            lstServer->addItem(Utils::wideString(packet->systemAddress.ToString()).c_str());
//            printf("\n");
//        }
//cout<<"after packet"<<endl;
    // peer->DeallocatePacket(packet);
//    cout<<"after after packet"<<endl;
//    mouseOver( );
    displayMouse( );
    pVideo->endScene();
#ifdef _DEBUG
    if( showDebugData )
        this->displayDebugData();
#endif
}

bool GameSelection::initState( GameMgr& game )
{
    cout << "GameSelection initState() start" << endl;
    // init the local game engine reference
    pGameMgr = &game;
    pDataMgr = pGameMgr->getDataMgr();
    pDevice = pGameMgr->getDevice();
    // local references to the other Irrlicht system pointers
    pVideo = pDevice->getVideoDriver();
    pSceneMgr = pDevice->getSceneManager();
    pGUIEnv = pDevice->getGUIEnvironment();
    // set or disable system mouse cursor - false as drawing own cursor
    pDevice->getCursorControl()->setVisible( false );
    // load the mouse cursor image
    loadMouseCursor( );
    // init the keys array
    pGameMgr->clearKeys();
    // the order of the following function calls may be important
    // if there are dependencies so rearrange/remove as reqd.
    if ( not this->initData() )
        return false;
    if ( not this->initGUISkin() )
        return false;
    if ( not this->initGUIComponents() )
        return false;
    if ( not this->initGUIData() )
        return false;
    if ( not this->initWorld()    )
        return false;
    if ( not this->initNodes()    )
        return false;
    if ( not this->initCameras()  )
        return false;
    // msecs for the fade to complete
    fader->fadeIn( 3000 );
    cout << "GameSelection initState() success" << endl;
    return true;
}

bool GameSelection::initData()
{
    timeNow = timeLast = gameTime = timeDelta = 0;
    desiredFPS = 60;
    showDebugData = false;
    imgBg = NULL;
    cout << "GameSelection initData() success" << endl;
    return true;
}

bool GameSelection::initGUISkin()
{
    gui::IGUIFont* pFontGUI;
    //! load background image - it is drawn in the update function
    imgBg = pVideo->getTexture( "media/images/theflyingship.jpg" );
    //! set fader
    fader = pDevice->getGUIEnvironment()->addInOutFader();
    // the colour to start from
    fader->setColor( video::SColor( 0, 0, 0, 0 ) );
    //! font for text labels
    pFontGUI = pGUIEnv->getFont( "media/fonts/Perpetua24.bmp" );
    // skin renderers - EGST_WINDOWS_CLASSIC, EGST_WINDOWS_METALLIC or EGST_BURNING_SKIN
    gui::IGUISkin* pSkin = pGUIEnv->createSkin( gui::EGST_WINDOWS_METALLIC );
    // the default font - used unless the object overrides it
    pSkin->setFont( pFontGUI, gui::EGDF_DEFAULT );
    // not changing tooltip font so not keeping a reference to it
    pSkin->setFont( pGUIEnv->getFont( "media/fonts/ConsoleFont.bmp" ), gui::EGDF_TOOLTIP );
    // the GUI environment will manage the pointer
    pGUIEnv->setSkin( pSkin );
    // must drop() as used a create...() function, createSkin().
    pSkin->drop();
    cout << "GameSelection initGUISkin() success" << endl;
    return true;
}

bool GameSelection::initGUIComponents()
{
    //! set up some common variables for the gui components
    u32 scrnWidth = pGameMgr->getScreenResolution().Width;
    u32 scrnHeight = pGameMgr->getScreenResolution().Height;
    // starting values for the first component, the title
    // object start top, width and height
    signed topY = scrnHeight / 32, width = 540, height = 46;
    // centred on the screen
    signed leftX = ( scrnWidth - width ) / 2;
    // all components req. a dimension this is first used on the title
    core::rect<s32> sizer( leftX, topY, leftX + width, topY + height );
    //! set title text and position
    // text, size, border, wordwrap, parent, id, background
    title = pGUIEnv->addStaticText ( L"Sky Pirates Game Selection", sizer, false, false, 0, -1, false );
    // diferent colour and font for the title
    title->setOverrideColor( video::SColor( 0, 0, 0, 0 ) );
    title->setOverrideFont( pGUIEnv->getFont( "media/fonts/bigfont.png" ) );
    title->setTextAlignment( gui::EGUIA_CENTER, gui::EGUIA_CENTER );

//    lstServer->addItem( text );

//unsigned int index;

    leftX = ( scrnWidth - width ) / 2;
    topY = scrnHeight / 30;
    width = 300;
//    wchar_t ip[256] = peer->GetLocalIP();

    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    // ipShare = pGUIEnv->addStaticText(peer->GetGuidFromSystemAddress( RakNet::UNASSIGNED_SYSTEM_ADDRESS ).ToString());

    // set up images for "button" images 256x64
    u32 incrY = scrnHeight / 6;
    u32 incrX = scrnWidth / 8;
    leftX = incrX;
    topY = incrY + 48;
    width = 256;
    height = 64;
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    //   Create button(s)
    btnCreate = pGUIEnv->addButton( sizer, 0, GUI_BTN_CREATE, L"" );
    btnCreate->setImage( pVideo->getTexture( "media/textures/buttons/create.png" ) );
    btnCreate->setUseAlphaChannel( true );
    btnCreate->setPressedImage( pVideo->getTexture( "media/textures/buttons/play1.png" ) );
    btnCreate->setDrawBorder( false );
    //   Join button(s)
    leftX += 560;
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    btnJoin = pGUIEnv->addButton( sizer, 0, GUI_BTN_JOIN, L"" );
    btnJoin->setImage( pVideo->getTexture( "media/textures/buttons/join.png" ) );
    btnJoin->setUseAlphaChannel( true );
    btnJoin->setPressedImage( pVideo->getTexture( "media/textures/buttons/play1.png" ) );
    btnJoin->setDrawBorder( false );

    // server box
    leftX = ( leftX - ((width/2) +150) );
    width = 300;
    RakNet::SocketDescriptor sd;

    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    edtServer = pGUIEnv->addEditBox( L"0", sizer );
    wchar_t text[256] = L"enter ip";
    cout << "***** hostAddress size " << strlen( sd.hostAddress ) << endl;
    mbstowcs( text, sd.hostAddress, strlen( sd.hostAddress ) );

    //   Exit button
    leftX += incrX;
    topY += incrY;
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    btnExit = pGUIEnv->addButton( sizer, 0, GUI_BTN_CANCEL, L"" );
    btnExit->setImage( pVideo->getTexture( "media/textures/buttons/quit2.png" ) );
    btnExit->setUseAlphaChannel( true );
//    pExitButtonImage = pGUIEnv->addImage( sizer );
    btnExit->setPressedImage( pVideo->getTexture( "media/textures/buttons/quit3.png" ) );
    btnExit->setDrawBorder( false );

    //! debug data display
    pDebugData = pGUIEnv->addStaticText(
                     L"",
                     core::rect<s32>( 0, scrnHeight - 64, 256, scrnHeight ),
                     true,
                     true,
                     0,
                     -1,
                     true );
    pDebugData->setOverrideColor( video::SColor( 255, 255, 255, 0 )  );
    // starting off invisible for this - no poly count
    pDebugData->setVisible( false );
    cout << "GameSelection initGUIComponents() success" << endl;
    return true;
}

bool GameSelection::initGUIData()
{
    std::cout << "GameSelection initGUIData() success" << std::endl;
    return true;
}

bool GameSelection::initNodes()
{
    return true;
}

bool GameSelection::initCameras()
{
    pCamera = pSceneMgr->addCameraSceneNode(
              );
    pCamera->setPosition( core::vector3df( 0, 0, 0 ) );
    pCamera->setTarget( core::vector3df( 0, 0, 1 ) );
    return true;
}

bool GameSelection::initWorld()
{
    return true;
}

void GameSelection::keyboardEvent( SEvent event )
{
    if ( event.KeyInput.PressedDown )
    {
        switch ( event.KeyInput.Key )
        {
//        case irr::KEY_F8:
//            pGameMgr->changeState( StartMenu::getInstance() );
//            break;
//        case irr::KEY_F9:
//            pGameMgr->changeState( GameSelection::getInstance() );
//            break;
//        case irr::KEY_F10:
//            pGameMgr->changeState( LevelSelection::getInstance() );
//            break;
//        case irr::KEY_F11:
//            pGameMgr->changeState( LvlDerelict::getInstance() );
//            break;
//        case irr::KEY_F12:
//            pGameMgr->changeState( LvlHopping::getInstance() );
//            break;
//        case irr::KEY_F7:
//            pGameMgr->changeState( GameSettings::getInstance() );
//            break;

        case irr::KEY_F2: // switch wire frame mode
//            terrain->setMaterialFlag(video::EMF_WIREFRAME, !terrain->getMaterial(0).Wireframe);
//            terrain->setMaterialFlag(video::EMF_POINTCLOUD, false);
            break;
        case irr::KEY_F3: // switch points only mode
//            terrain->setMaterialFlag(video::EMF_POINTCLOUD, !terrain->getMaterial(0).PointCloud);
//            terrain->setMaterialFlag(video::EMF_WIREFRAME, false);
            break;
        case irr::KEY_F4: // toggle detail map
//            terrain->setMaterialType(
//                terrain->getMaterial(0).MaterialType == video::EMT_SOLID ?
//                video::EMT_DETAIL_MAP : video::EMT_SOLID);
            break;
        default :
            break;
        }
    }
}

void GameSelection::mouseEvent( SEvent event )
{
}

void GameSelection::mouseGUIEvent( SEvent event )
{
    s32 id = event.GUIEvent.Caller->getID();
    switch ( event.GUIEvent.EventType )
    {
    case gui::EGET_BUTTON_CLICKED:
        switch ( id )
        {
        case GUI_BTN_CREATE:
            imgBg = pVideo->getTexture( "media/images/pleaseWait.png" );
            btnCreate->setVisible( false );
            btnJoin->setVisible( false );
            btnExit->setVisible( false );
            title->setVisible( false );
            edtServer->setVisible( false );

            update();

            pGameMgr->rakNetStateCS = 's';

            pGameMgr->changeState( LvlDerelict::getInstance() );
            break;
        case GUI_BTN_JOIN:
            imgBg = pVideo->getTexture( "media/images/pleaseWait.png" );
            btnCreate->setVisible( false );
            btnJoin->setVisible( false );
            btnExit->setVisible( false );
            title->setVisible( false );
            edtServer->setVisible( false );
            update();
            pGameMgr->rakNetStateCS = 'c';
            strcpy( pGameMgr->joinIP, Utils::stdString( edtServer->getText() ).c_str() );

            // Add required game JOINING related networking code here

            pGameMgr->changeState( LvlDerelict::getInstance() );
            break;
        case GUI_BTN_CANCEL:
            pGameMgr->changeState( LevelSelection::getInstance() );
            break;
        default:
            break;
        }
        break;
    default:
        break;
    }
}

void GameSelection::loadMouseCursor( )
{
    mouseCursor =  pVideo->getTexture( "media/cursors/pointercursor.png" );
    pVideo->makeColorKeyTexture( mouseCursor, core::position2d<s32>( 0, 0 ) );
    pDevice->getCursorControl()->setVisible( false );
}

//void GameSelection::resetButtons( )
//{
//    // set highlighed button to off
//    pPlayButtonHighImage->setVisible( false );
//    pOptionsButtonHighImage->setVisible( false );
//    pCreditsButtonHighImage->setVisible( false );
//    pExitButtonHighImage->setVisible( false );
//    // set normal buttons on
//    pPlayButtonImage->setVisible( true );
//    pOptionsButtonImage->setVisible( true );
//    pCreditsButtonImage->setVisible( true );
//    pExitButtonImage->setVisible( true );
//}
//
//bool GameSelection::mouseOver( )
//{
//    // resets if over another button
//    resetButtons( );
//    // get mouse position2d<s32>
//    mousePos = pDevice->getCursorControl()->getPosition();
//    // check if over Play button
//    if ( pPlayButtonImage->getRelativePosition().isPointInside( mousePos ) )
//    {
//        pPlayButtonHighImage->setVisible( true );
//        pPlayButtonImage->setVisible( false );
//        return true;
//    }
//    // check if over Options button
//    if ( pOptionsButtonImage->getRelativePosition().isPointInside( mousePos ) )
//    {
//        pOptionsButtonHighImage->setVisible( true );
//        pOptionsButtonImage->setVisible( false );
//        return true;
//    }
//    // check if over Credits button
//    if ( pCreditsButtonImage->getRelativePosition().isPointInside( mousePos ) )
//    {
//        pCreditsButtonHighImage->setVisible( true );
//        pCreditsButtonImage->setVisible( false );
//        return true;
//    }
//    // check if over Exit button
//    if ( pExitButtonImage->getRelativePosition().isPointInside( mousePos ) )
//    {
//        pExitButtonHighImage->setVisible( true );
//        pExitButtonImage->setVisible( false );
//        return true;
//    }
//    return false;
//}
//
//void GameSelection::mouseClicked( )
//{
//    // get mouse position2d<s32>
//    mousePos = pDevice->getCursorControl()->getPosition();
//    // check if over Exit button
////    if ( pExitButtonImage->getRelativePosition().isPointInside( mousePos ) )
////        pDevice->closeDevice();
////    // check if over Options button
////    else if ( pOptionsButtonImage->getRelativePosition().isPointInside( mousePos ) )
////        pStateManager->changeState( OptionsState::createSingleton() );
////    // check if over Play button
////    else if ( pPlayButtonImage->getRelativePosition().isPointInside( mousePos ) )
////        pStateManager->changeState( Level1::createSingleton() );
////    // check if over Play button
////    else if ( pCreditsButtonImage->getRelativePosition().isPointInside( mousePos ) )
////        pStateManager->changeState( PlayerOptions::createSingleton() );
//}

void GameSelection::displayMouse( )
{
    mousePos = pDevice->getCursorControl()->getPosition();
    pVideo->draw2DImage( mouseCursor, core::position2d<s32>( mousePos.X, mousePos.Y ),
                         core::rect<s32>( 0, 0, 16, 16 ), 0, video::SColor( 255, 255, 255, 255 ), true );
//    }
}

void GameSelection::freeResources( )
{
    // clear the gui environment unless there are elements to use in the next state
    pGUIEnv->clear();
    // same with the scene manager
    pSceneMgr->clear();
    cout << "GameSelection freeResources()" << endl;
}


void GameSelection::displayDebugData()
{
    char buffer[256]; // using sprintf for its easy formatting ability
    core::stringw wstr( "Debug - FPS: " );       // debug title
    wstr += core::stringw( pVideo->getFPS() );
    wstr += "\nPoly: ";
    wstr += core::stringw( pVideo->getPrimitiveCountDrawn() );
    // get camera position
//    if ( pCamera )
//    {
//        sprintf( buffer, "\nCam: X:%.0f Y:%.0f Z:%.0f",
//                 pCamera->getPosition().X,
//                 pCamera->getPosition().Y,
//                 pCamera->getPosition().Z );
//    }
//    else
    sprintf( buffer, "No Camera\n" );
    wstr += buffer;
    // get game time
    unsigned int seconds = gameTime / 1000;
    sprintf( buffer, "%02d:%02d:%02d", seconds / 60, seconds % 60, gameTime % 1000 );
    wstr += "\nTime: ";
    wstr += buffer;
    pDebugData->setText( wstr.c_str() );
}

void GameSelection::updateTimer()
{
    timeNow = pDevice->getTimer()->getTime();
    timeDelta = timeNow - timeLast;
    timeLast = timeNow;
    gameTime += timeDelta;
}

void GameSelection::pause()
{
}

void GameSelection::resume()
{
}


























