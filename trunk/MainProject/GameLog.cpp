#include "GameLog.h"

/** @brief initLog
  * @param fileName for log output
  *     NULL will output to console
  */
void GameLog::initLog( const char* fileName )						// Initializes Error Logging
{
    time_t rawtime;
    struct tm * timeinfo;
    time ( &rawtime );
    timeinfo = localtime ( &rawtime );
    if ( fileName )
    {
        gameLog = fopen( fileName, "w" );
    }
    if ( not gameLog )
    { // can't open file - output to console
        if ( fileName )     // gave a file name
            printf( "Access Denied to Logfile: %s\n", fileName );
        else
            printf( "Logging to the default console\n" );
    }
    print("%s\n Version%d.%d.%d\n -- Log Init at %s\n",
        "Game Name or Comment Here", MAJOR_VERSION, MINOR_VERSION, BUILD_NUMBER,
        asctime ( timeinfo ) );
}

void GameLog::closeLog( )
{
    print("\n -- Closing Log --\n" );
    clock_t now = clock();
    if ( now < CLOCKS_PER_SEC )
        print("\nApplication Active for %u msecs\n", now );
    else
        print("\nApplication Active for %u seconds\n", now / CLOCKS_PER_SEC );
    if ( gameLog )
    {
        fclose( gameLog );
    }
}

int GameLog::print( char* format, ... )
{
	int numChars = -1;
    try
    {
        // to substitute format tokens %s, %d etc
        va_list args;
        // set arg index to start of the list
        va_start( args, format );
        // if log is open
        if( gameLog )
        {
            // make format substitutions
            numChars = vfprintf( gameLog, format, args);
            fprintf( gameLog, "\n" );
            // ensure line is written -
            //TODO comment out line below if using in game loop, which you shouldn't be doing!
            fflush( gameLog );
        }
        else
        {
            vprintf( format, args );    // console output
            printf( "\n" );
        }
        va_end( args );					// end list ops and close
    }
    catch ( char *strError )
    {
        printf( "GameLog::print failed: %s", strError );
        return -1;
    }
	return numChars;				// return
}


