#include "PauseGUI.h"
#include "OptionsGUI.h"

//! define the static singleton pointer
PauseGUI PauseGUI::instance;
using namespace irr;

void PauseGUI::update()
{
    this->updateTimer();
    pVideo->beginScene( true, true, video::SColor( 255, 100, 101, 140 ) );
    //draw a bg image
//    pVideo->draw2DImage(bgImage,
//                        core::rect<s32>(0,0, pGameMgr->getScreenResolution().Width,pGameMgr->getScreenResolution().Height ),
//                        core::rect<s32>(0,0, bgImage->getSize().Width,bgImage->getSize().Height )
//                       );


    pSceneMgr->drawAll();
    pGUIEnv->drawAll();
#ifdef _DEBUG
    if( showDebugData )
        this->displayDebugData();
#endif
    pVideo->endScene();
}

bool PauseGUI::initState( GameMgr& game )
{
    // init the local game engine reference
    pGameMgr = &game;
    pDataMgr = pGameMgr->getDataMgr();
    pDevice = pGameMgr->getDevice();
    // local references to the other Irrlicht system pointers
    pVideo = pDevice->getVideoDriver();
    pSceneMgr = pDevice->getSceneManager();
    pGUIEnv = pDevice->getGUIEnvironment();
    // set or disable system mouse cursor - false as drawing own cursor
    pDevice->getCursorControl()->setVisible( false );
    // load the mouse cursor image
    //loadMouseCursor( );
    // init the keys array
    pGameMgr->clearKeys();
    // the order of the following function calls may be important
    // if there are dependencies so rearrange/remove as reqd.
    if ( not this->initData() )
        return false;
    if ( not this->initGUISkin() )
        return false;
    if ( not this->initGUIComponents() )
        return false;
    if ( not this->initGUIData() )
        return false;
    if ( not this->initWorld()    )
        return false;
    if ( not this->initNodes()    )
        return false;
    if ( not this->initCameras()  )
        return false;
    // msecs for the fade to complete
    fader->fadeIn( 3000 );
    cout << "PauseGUI initState() success  finally" << endl;
    return true;
}


bool PauseGUI::initData()
{
    timeNow = timeLast = gameTime = timeDelta = 0;
    desiredFPS = 60;
    showDebugData = false;
    cout << "PauseGUI initData() success" << endl;
    return true;
}

bool PauseGUI::initGUISkin()
{

    //bgImage = pVideo->getTexture("media/irrPauseGUIbg3.png");
    fader = pGUIEnv->addInOutFader();
    fader->setColor( video::SColor( 0, 0, 0, 0 ) );
    cout << "PauseGUI initGUISkin() success" << endl;
    return true;

}

bool PauseGUI::initGUIComponents()
{

#ifdef _DEBUG
    // load the font map
    gui::IGUIFont* font = pGUIEnv->getFont( "media/fonts/ConsoleFont.bmp" );
//    if ( ! font )
//
//        cout << "PauseGUI -- Failed to load font " << endl;
//    return false;
    // creating a skin that uses different )from the default) fonts and colours - RTM for details
    gui::IGUISkin* newskin = pGUIEnv->createSkin( gui::EGST_WINDOWS_CLASSIC );
    newskin->setFont( font );
    pGUIEnv->setSkin( newskin );
    newskin->drop();
    // add a static label for the debug data
    lblDebugData = pGUIEnv->addStaticText(
                       L"",
                       core::rect<s32>( 0, 0, 256, 64 ),
                       true,
                       true,
                       0,
                       -1,
                       true );
    if ( ! lblDebugData )
        return false;
    lblDebugData->setOverrideColor( video::SColor( 255, 0, 0, 0 )  );

#endif
    // this happens for both debug and release
    // readXMLSettings("data/DeviceOptions.xml");

    // position the buttons to the centre/middle of the GUI screen...

    int leftX = ( pGameMgr->getScreenResolution().Width / 2 ) - 50 , topY = pGameMgr->getScreenResolution().Height / 3 , width = 100, height = 32;
    // OK and Cancel
    cmdResume = pGUIEnv->addButton(
                    core::rect<s32>( leftX, topY, leftX + width, topY + height ),   // location ans size
                    0,                                                              // parent
                    GUI_CMD_RESUME,
                    L"Resume",
                    L"Click to Resume play" );


    topY = topY + 50;

    cmdOptions = pGUIEnv->addButton(
                     core::rect<s32>( leftX, topY, leftX + width, topY + height ),   // location ans size
                     0,                                                              // parent
                     GUI_CMD_OPTIONS,
                     L"Options",
                     L"Click to make changes to the game settings." );


    topY = topY + 50;


    cmdExitGame = pGUIEnv->addButton(
                      core::rect<s32>( leftX, topY, leftX + width, topY + height ),   // location ans size
                      0,                                                              // parent
                      GUI_CMD_EXITGAME,
                      L"Exit Game",
                      L"Click to Exit the game." );


    topY = topY + 50;


    cmdExitWindows = pGUIEnv->addButton(
                         core::rect<s32>( leftX, topY, leftX + width, topY + height ),   // location ans size
                         0,                                                              // parent
                         GUI_CMD_EXITWINDOWS,
                         L"Exit Windows",
                         L"Click to Exit the game." );

    topY = topY;

    cout << "PauseGUI initGUIComponents() success" << endl;
    return true;
}

bool PauseGUI::initGUIData()
{
    std::cout << "PauseGUI initGUIData() success" << std::endl;
    return true;
}
bool PauseGUI::initNodes()
{
    return true;
}

bool PauseGUI::initCameras()
{
    pCamera = pSceneMgr->addCameraSceneNode( );
    if ( ! pCamera )
    {
        cout << "PauseGUI initCameras() failed ***" << endl;
        return false;
    }
    // set position of camera and the camera target // default 0,0,0
    pCamera->setPosition( core::vector3df( 0.0f, 0.0f, -1024.0f ) );
    pCamera->setTarget( core::vector3df( 0.0f, 0.0f, 0.0f ) );
    pCamera->setFarValue( 4000.0f );    // default 2000
    cout << "PauseGUI initCameras() success" << endl;
    return true;
}
bool PauseGUI::initWorld()
{
    return true;
}


void PauseGUI::keyboardEvent( SEvent event )
{
    if ( event.KeyInput.PressedDown )
    {
        switch ( event.KeyInput.Key )
        {
        case irr::KEY_F1: // switch wire frame mode
#ifdef _DEBUG
            showDebugData = !showDebugData;
            if( showDebugData )
                lblDebugData->setVisible( true );
            else
                lblDebugData->setVisible( false );
#endif
            break;
        case irr::KEY_F2: // switch wire frame mode
//            terrain->setMaterialFlag( video::EMF_WIREFRAME, !terrain->getMaterial( 0 ).Wireframe );
//            terrain->setMaterialFlag( video::EMF_POINTCLOUD, false );
            break;
        case irr::KEY_F3: // switch points only mode
//            terrain->setMaterialFlag( video::EMF_POINTCLOUD, !terrain->getMaterial( 0 ).PointCloud );
//            terrain->setMaterialFlag( video::EMF_WIREFRAME, false );
            break;
        case irr::KEY_F4: // toggle detail map
//            terrain->setMaterialType(
//                terrain->getMaterial( 0 ).MaterialType == video::EMT_SOLID ?
//                video::EMT_DETAIL_MAP : video::EMT_SOLID );
            break;
        default :
            break;
        }
    }
}

void PauseGUI::mouseEvent( SEvent event )
{
}

void PauseGUI::mouseGUIEvent( SEvent event )
{
    // get the event ID number
    s32 id = event.GUIEvent.Caller->getID();
    switch ( event.GUIEvent.EventType )
    {
    case gui::EGET_BUTTON_CLICKED :
        if( id == GUI_CMD_RESUME )
        {
            // get the user settings and save to XML
//            cout << "Clicked OK and Selected item " << cboRenderer->getSelected() <<
//                    " " << Utils::stdString(cboRenderer->getItem(cboRenderer->getSelected())) << endl;
            pGameMgr->popState();

            // pGameMgr->running = false;
        }

        else if ( id == GUI_CMD_OPTIONS )
        {
            pGameMgr->pushState( OptionsGUI::getInstance() );
            cout << "Clicked Options" << endl;

            //pGameMgr->running = false;

        }
        else if ( id == GUI_CMD_EXITGAME )
        {

            cout << "exit Game" << endl;

            //pGameMgr->running = false;

        }
        else if ( id == GUI_CMD_EXITWINDOWS )
        {

            cout << "exit Game Window" << endl;

            pGameMgr->running = false;

        }
        break;

    default:
        break;
    }

}
void PauseGUI::freeResources( )
{
    pSceneMgr->clear();
    pGUIEnv->clear();
    cout << "PauseGUI freeResources()" << endl;
}


void PauseGUI::displayDebugData()
{
    char buffer[256]; // using sprintf for its easy formatting abilities
    core::stringw wstr( "Debug - FPS: " );       // debug title
    wstr += core::stringw( pVideo->getFPS() );
    wstr += "\nPoly: ";
    wstr += core::stringw( pVideo->getPrimitiveCountDrawn() );
    // get camera position
    if ( pCamera )
    {
        sprintf( buffer, "\nCam: X:%.0f Y:%.0f Z:%.0f",
                 pCamera->getPosition().X,
                 pCamera->getPosition().Y,
                 pCamera->getPosition().Z );
    }
    else
        sprintf( buffer, "No Camera\n" );
    wstr += buffer;
    // get game time
    unsigned int seconds = gameTime / 1000;
    sprintf( buffer, "%02d:%02d:%02d", seconds / 60, seconds % 60, gameTime % 1000 );
    wstr += "\nTime: ";
    wstr += buffer;
    lblDebugData->setText( wstr.c_str() );
}

void PauseGUI::updateTimer()
{
    timeNow = pDevice->getTimer()->getTime();
    timeDelta = timeNow - timeLast;
    timeLast = timeNow;
    gameTime += timeDelta;
}

void PauseGUI::pause()
{
//    if( pSound )
//        pSound->pause();
    cout << "*** PauseGUI pause() ***" << endl;
}

void PauseGUI::resume()
{
//    if( pSound )
//        pSound->pause();
    cout << "*** PauseGUI resume() ***" << endl;
}

























