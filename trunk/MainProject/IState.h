#ifndef ISTATE_H
#define ISTATE_H

// forward declaration for the GameMgr pointer
class GameMgr;
class DataMgr;
class Player;

//! an interface (class) to handle individual game states
/** contains pure virtual functions that must be implemented by derived concrete classes<br>
To use the GameMgr, all level classes must derive from this class.<br>
The initState() is guaranteed to be called first and freeResources() on close.<br>
The Game Engine will pass on key and mouse/controller events to the named functions,<br>
update() is called by the engine in it's run() function, see run()'s comments for details.
The irrlicht system pointers are available to all levels and should be set and tested in the initState().<br>
pSceneMgr and pGUIEnv will usually require clear()'ing before use if another level was loaded before.
*/
class IState
{
public:
    // allocate the resources used by the state
    virtual bool initState( GameMgr& game ) = 0;

    // free the resources used by the state
    virtual void freeResources( ) = 0;

    //! receive the key events not handled by the manager to process or ignore
    virtual void keyboardEvent( SEvent event ) = 0;

    //! mouse events in the game world
    virtual void mouseEvent( SEvent event ) = 0;

    //! mouse events on the 2D GUI compnents
    virtual void mouseGUIEvent( SEvent event ) = 0;

    //! centralised update/render
    virtual void update( ) = 0;

    // timer/s pause and resume. called by the game engine when it pauses the game
    //! timer pause
    virtual void pause() = 0;

    //! timer resume
    virtual void resume() = 0;

    // inline ctor
    IState() :
        pDevice( NULL ), pVideo( NULL ) , pSceneMgr( NULL ),
        pGUIEnv( NULL ), pGameMgr( NULL )
    { }
    // inline dtor
    virtual ~IState() { }

protected:
    // these references are initialised to NULL in the ctor
    //! local pointer to the main Irrlich device
    irr::IrrlichtDevice* pDevice;
    //! local pointer to the VideoDriver
    video::IVideoDriver* pVideo;
    //! local pointer to a Scene Manager
    scene::ISceneManager* pSceneMgr;
    //! local pointer to the 2D gui handler
    gui::IGUIEnvironment* pGUIEnv;
    //! a reference to the game engine
    GameMgr* pGameMgr;
    //! a reference to the game data manager
    DataMgr* pDataMgr;
    //! global player object
    Player* pPlayerObject;

private:

};


#endif
