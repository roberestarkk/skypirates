#ifndef GAME_MGR_H
#define GAME_MGR_H

#include <iostream>
using std::cout;
using std::endl;
#include <string>

// global include for all the irrlicht headers
#include "Irrlicht.h"
// everything in the Irrlicht Engine is in the 'irr' namespace
using namespace irr;
// the 5 sub namespaces of the 'irr' namespace
//using namespace core;
//using namespace scene;
//using namespace video;
//using namespace io;
//using namespace gui;

// define for tinyxml C++ wrapper
#ifndef TIXML_USE_TICPP
#define TIXML_USE_TICPP
#endif
// tinyxml++ header
#include "ticpp.h"

// irrklang sound library header
#include "irrKlang.h"
using namespace irrklang;

// string handling functions and string <-> number conversions
#include "Utils.h"
#include "DataMgr.h"

// interface class for individual states
#include "IState.h"
#include "../utils/StateStack.h"
#include "StartMenu.h"
#include "Player.h"

//! main class for game management.
/** reads the system config data file.
  * using irrklang for sound.
  * game levels to be accessed by class name and data loaded for each level from xml.
  * using CopperCube (IrrEdit) to create the world or world parts for individual loading.
  * loads each level based on the interface class IState
  */
class GameMgr : public IEventReceiver
{
public:
    //! inline default constructor
    GameMgr() :
        running( true ),
        pDevice( NULL ),    // the irrlicht device
        pVideo( NULL ),     // video environment
        pSceneMgr( NULL ),  // mesh controller
        pGUIEnv( NULL ),    // 2D gui elements
        pCurrentState( NULL ) // current active state
    {}

    //! a reference to the player object
    Player* playerObject;

    char rakNetStateCS;

    //! variable which holdeth the IP-eth of ye old servar.
    char joinIP[256];

    //! inline default destructor
    virtual ~GameMgr() {}

    //! initialise game engine
    /** init all the required systems
    \param fileName: the XML configuration data file name
    \return true if initialisation is OK, false if there is problem */
    bool initGameMgr( );

    void resetGameMgr( );

    //! close the current state and activate a new state
    /** Note that you can not return to the old state.
      * \param newState pointer to state to change over too. */
    void changeState( IState* newState );

    //! Pauses current state and leave on stack
    /** Place instance of new state on the stack
    \param state: the state to change too.
    \note calls pause() function of the current state */
    void pushState( IState* state );

    //! Close current state and remove from stack
    /** Return to previous instance on the stack - if any
    \note Calls resume() function of previous state.<br>
    Quits with an error message if no states on stack!! */
    void popState();


    //! main game loop - quitting the game is the only way out of here
    void run();

    //! part of main game loop - calls the update() on current active state
    void update();

    //! close and release all opened devices
    void freeResources();

    // the event handling functions
    //! Implement OnEvent which is a pure virtual function in IEventReceiver
    /** separate events into kbd, mouse in window or mouse on GUI object
    \param the event passed from Irrlicht's event handler
    \return for inheritance compliance, can be true or false as not checked */
    bool OnEvent( const SEvent& event );

    //! to detect keyboard presses.
    /** Each key can be toggled on or off
    \param key char to test if down or not
    \return from the array - true if key is down */
    bool getKey( unsigned char key );

    //! Empty the key event array
    /** call at least once at the level init, */
    void clearKeys();

    // the primary references to the systems once they are initialised
    // each level should take the references they require in their init()
    //! access the irrlicht device
    /** \return the irrlicht device reference */
    IrrlichtDevice* getDevice() const;

    //! video render device is stored as an irrlicht enum value
    /** \return The current value of device */
    irr::video::IVideoDriver* getVideoDriver() const;

    //! access the main irrlicht scene manager
    /** \return the scene manager reference */
    scene::ISceneManager* getSceneManager() const;

    //! access the irrlicht gui (2D overlay)
    /** \return the irrlicht gui reference */
    gui::IGUIEnvironment* getGUIEnvironment() const;

    //! access the loaded sound system
    /** \return the sound system reference */
    DataMgr* getDataMgr() const;

    //! access the loaded sound system
    /** \return the sound system reference */
    Player* getPlayerObject() const;

    //! screen dimensions
    /** \return The required value of screen Width and Height */
    irr::core::dimension2d<irr::u32> getScreenResolution();

    //! run full screen mode
    /** \return The current value of full screen */
    bool  isFullScreen();

    //! sound on or not
    /** \return The current value of sound */
    bool isSoundAvailable();

    //! game run time name for the window title bar
    /** \return The current game name */
    std::string getVersion();

    //! game run time name for the window title bar
    /** \param The new game name */
    void setVersion( std::string newVersion );

    //! clear the central data storage
    void clearDataStore( );

    //! add a (string) item to the central data store
    /** Wil overwrite existing keys of the same name.
    \param name: the section to use
    \param data: the string data to add to the section */
    void addDataStore( std::string name, std::string data );

    //! get an item by it's name
    /** \param name: the key name for the data. Case Sensitive!
    \return the string associated with the given name.
    The name is error checked and if not found a warning window
    is displayed showing the problem name */
    std::string getDataStore( std::string name );

    //! see if a name exists prior to access for example
    /** \param name: the key name for the data. Case Sensitive!
    \return true if the name exists */
    bool isNameInDataStore( std::string name );

    //! stay in game loop until this is set to false,
    /** init'd to true in initGameMgr() when all is successful.
    When set to false, run() exits and main() calls the freeResources()
    which in turn calls freeResources() on any active state.
    \note has public access via the pGameMgr reference, take care. */
    bool running;

    //! start up the sound engine
    bool initSound();

    //! only needed in 3D to update listener/camera position
    /**  use when objects have a sound and a distance allocated to them
    \param camPos: current camera/listener position
    \param camLookAt: obtain with cam->getTarget() - cam->getAbsolutePosition() */
    void updateSounds( core::vector3df camPos,
                       core::vector3df camLookAt,
                       core::vector3df velocity = irr::core::vector3df( 1, 0, 1 ),
                       core::vector3df upvector = irr::core::vector3df( 0, 1, 0 ) );

    //! play a sound at a given position
    /** \param fileName: the sound to play
    \param position: position to play at.
    \param loop: true to loop the sound - until explicitly stopped.
    \return a sound object or null */
    irrklang::ISound* play3DSound( const char* fileName,
                                   irr::core::vector3df position = irr::core::vector3df( 0, 0, 0 ),
                                   bool loop = false );

    //! stop a sound by name
    /** \param fileName: the sound to stop */
    void stopSound( const char* fileName );

    //! stop a sound by name
    /** \param fileName: the sound to stop */
    void stopAllSounds( );

    //! global volume control
    /** \param volume: 0 off to 1.0 full sound */
    void setVolume( float volume );

    //! global volume control
    /** \return reference to the sound engine */
    irrklang::ISoundEngine* getSoundEngine() const;

private:
    //! private copy constructor
    /** \param other Object to copy from */
    GameMgr( const GameMgr& other ) {}    //! the irrlicht device accessors

    //! private assignment operator
    /** \param other Object to assign from
    \return A reference to this */
    GameMgr& operator=( const GameMgr& other )
    {
        return *this;
    }

    // init the irrlicht system
    bool initIrrlicht();

    /// key array to track which keys are down at any time
    bool keys[irr::KEY_KEY_CODES_COUNT];

    //! the irrlicht device reference
    /** duplicate these references in a level and use the
      * getters from the engine to initialise them. */
    irr::IrrlichtDevice* pDevice;

    //! the irrlicht video reference
    video::IVideoDriver* pVideo;

    //! the irrlicht scene manager reference
    scene::ISceneManager* pSceneMgr;

    //! the irrlicht gui (2D overlay) reference
    gui::IGUIEnvironment* pGUIEnv;

    //! reference to the active state
    IState* pCurrentState;

    //! the sound engine reference
    DataMgr* pDataMgr;

    //! the current screen size
    irr::core::dimension2d<irr::u32> screenResolution;
    //! the type of video driver opengl, DX
    irr::video::E_DRIVER_TYPE videoDriverType;
    //! bits per pixel setting
    unsigned bpp;

    //! sound engine reference
    irrklang::ISoundEngine* pSound;

    //! true if the sound system initialised OK
    bool soundAvailable;
    //! true if the user wants sound on
    bool soundOn;

    bool isSoundOn();

    //! true if full screen required
    bool fullScreen;

    //! current game/level version - wstring is used for window title bar
    std::string version;

    //! to transfer string data between levels/states
    std::map<std::string, std::string> dataStore;

    StateStack<IState*> stateStack;

};

//! irrlicht device references
inline DataMgr* GameMgr::getDataMgr() const
{
    return pDataMgr;
}

//! player references
inline Player* GameMgr::getPlayerObject() const
{
    return playerObject;
}

//! irrlicht device references
inline IrrlichtDevice* GameMgr::getDevice() const
{
    return pDevice;
}

inline irr::video::IVideoDriver* GameMgr::getVideoDriver() const
{
    return pVideo;
}

inline scene::ISceneManager* GameMgr::getSceneManager() const
{
    return pSceneMgr;
}

inline gui::IGUIEnvironment* GameMgr::getGUIEnvironment() const
{
    return pGUIEnv;
}

//! convenience functions
inline std::string GameMgr::getVersion()
{
    return version;
}

inline void GameMgr::setVersion( std::string newVersion )
{
    version = newVersion;
}

inline irr::core::dimension2d<irr::u32> GameMgr::getScreenResolution()
{
    return screenResolution;
}

inline bool GameMgr::isFullScreen()
{
    return fullScreen;
}

inline irrklang::ISoundEngine* GameMgr::getSoundEngine() const
{
    return pSound;
}

inline bool GameMgr::isSoundAvailable()
{
    return soundAvailable;
}

inline bool GameMgr::isSoundOn()
{
    return soundOn;
}

#endif // GAMEENGINE_H
