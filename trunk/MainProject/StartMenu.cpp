#include "StartMenu.h"

//! define the static singleton pointer
StartMenu StartMenu::instance;
using namespace irr;

void StartMenu::update()
{
    this->updateTimer();
    pVideo->beginScene( true, true, video::SColor( 255, 100, 101, 140 ) );
    if ( imgBg )
        pVideo->draw2DImage(
            imgBg,
            core::rect< irr::s32 >( 0, 0, pGameMgr->getScreenResolution().Width, pGameMgr->getScreenResolution().Height ),
            core::rect< irr::s32 >( 0, 0, imgBg->getSize().Width, imgBg->getSize().Height ) );
    pSceneMgr->drawAll();
    pGUIEnv->drawAll();
//    mouseOver( );
    displayMouse( );
    pVideo->endScene();
#ifdef _DEBUG
    if( showDebugData )
        this->displayDebugData();
#endif
}

bool StartMenu::initSounds()
{
    cout << "initSounds() begun" << endl;
    pGameMgr->getSoundEngine()->removeAllSoundSources();
    music = NULL;
    music = pGameMgr->getSoundEngine()->addSoundSourceFromFile( "media/sounds/combination.wav" );
    if ( music )
    {
        std::cout << "bgMusic.wav loaded" << std::endl;
    }
    else
    {
        std::cout << "**** Music Not Loaded" << std::endl;
    }

    cout << "Found Combination.wav... " << endl;
    if ( music && soundEnabled)
    {
        pGameMgr->getSoundEngine()->play2D(  music, true); // looped

        std::cout << "Combination.wav Loaded" << std::endl;
//        return true;
    }
    else
    {
        std::cout << "**** Music Not Loaded" << std::endl;
//        return false;
    }

    cout << "sounds initialised" << endl;
    return true;

}



bool StartMenu::initState( GameMgr& game )
{
    cout << "StartMenu initState() start" << endl;
    // init the local game engine reference
    pGameMgr = &game;
    pDataMgr = pGameMgr->getDataMgr();
    pDevice = pGameMgr->getDevice();
    // local references to the other Irrlicht system pointers
    pVideo = pDevice->getVideoDriver();
    pSceneMgr = pDevice->getSceneManager();
    pGUIEnv = pDevice->getGUIEnvironment();
    // set or disable system mouse cursor - false as drawing own cursor
    pDevice->getCursorControl()->setVisible( false );
    // load the mouse cursor image
    loadMouseCursor( );
    // init the keys array
    pGameMgr->clearKeys();
    // the order of the following function calls may be important
    // if there are dependencies so rearrange/remove as reqd.
    if ( not this->initData() )
        return false;
    if ( not this->initGUISkin() )
        return false;
    if ( not this->initGUIComponents() )
        return false;
    if ( not this->initGUIData() )
        return false;
    if ( not this->initWorld()    )
        return false;
    if ( not this->initNodes()    )
        return false;
    if ( not this->initCameras()  )
        return false;
    if ( not this->initSounds()   )
        return false;
    // msecs for the fade to complete
    fader->fadeIn( 3000 );
    cout << "StartMenu initState() success" << endl;
    return true;
}

bool StartMenu::initData()
{
    timeNow = timeLast = gameTime = timeDelta = 0;
    desiredFPS = 60;
    showDebugData = false;
    imgBg = NULL;
    soundEnabled = true;
    soundEnabled =  Utils::toBoolean(pDataMgr->getSystemSetting( "soundon" ));
    cout << "StartMenu initData() success" << endl;
    return true;
}

bool StartMenu::initGUISkin()
{
    gui::IGUIFont* pFontGUI;
    //! load background image - it is drawn in the update function
    imgBg = pVideo->getTexture( "media/images/CombinedLogo.png" );
    //! set fader
    fader = pDevice->getGUIEnvironment()->addInOutFader();
    // the colour to start from
    fader->setColor( video::SColor( 0, 0, 0, 0 ) );
    //! font for text labels
    pFontGUI = pGUIEnv->getFont( "media/fonts/Perpetua24.bmp" );
    // skin renderers - EGST_WINDOWS_CLASSIC, EGST_WINDOWS_METALLIC or EGST_BURNING_SKIN
    gui::IGUISkin* pSkin = pGUIEnv->createSkin( gui::EGST_WINDOWS_METALLIC );
    // the default font - used unless the object overrides it
    pSkin->setFont( pFontGUI, gui::EGDF_DEFAULT );
    // not changing tooltip font so not keeping a reference to it
    pSkin->setFont( pGUIEnv->getFont( "media/fonts/ConsoleFont.bmp" ), gui::EGDF_TOOLTIP );
    // the GUI environment will manage the pointer
    pGUIEnv->setSkin( pSkin );
    // must drop() as used a create...() function, createSkin().
    pSkin->drop();
    cout << "StartMenu initGUISkin() success" << endl;
    return true;
}

bool StartMenu::initGUIComponents()
{
    //! set up some common variables for the gui components
    u32 scrnWidth = pGameMgr->getScreenResolution().Width;
    u32 scrnHeight = pGameMgr->getScreenResolution().Height;
    // starting values for the first component, the title
    // object start top, width and height
    signed topY = ( scrnHeight / 3 ) * 2, width = 480, height = 48;
    // centred on the screen
    signed leftX = ( scrnWidth - width ) / 2;
    // all components req. a dimension this is first used on the title
    core::rect<s32> sizer( leftX, topY, leftX + width, topY + height );
    //! set title text and position
    // text, size, border, wordwrap, parent, id, background
    title = pGUIEnv->addStaticText ( L"v0.4", sizer, false, false, 0, -1, false );
    // diferent colour and font for the title
    title->setOverrideColor( video::SColor( 0, 0, 0, 0 ) );
    title->setOverrideFont( pGUIEnv->getFont( "media/fonts/bigfont.png" ) );
    title->setTextAlignment( gui::EGUIA_CENTER, gui::EGUIA_CENTER );
    // set up images for "button" images 256x64
//    u32 incrY = scrnHeight / 6;
    u32 incrX = scrnWidth / 4;
    leftX = incrX / incrX;
    topY = pGameMgr->getScreenResolution().Height - ( pGameMgr->getScreenResolution().Height / 10 );
    width = 256;
    height = 64;
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    //   Play button(s)
    btnPlay = pGUIEnv->addButton( sizer, 0, GUI_BTN_PLAY, L"" );
    btnPlay->setImage( pVideo->getTexture( "media/textures/buttons/play2.png" ) );
    btnPlay->setUseAlphaChannel( true );
    btnPlay->setPressedImage( pVideo->getTexture( "media/textures/buttons/play1.png" ) );
    btnPlay->setDrawBorder( false );
    //   Options button
    leftX += incrX;
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    btnOptions = pGUIEnv->addButton( sizer, 0, GUI_BTN_OPTIONS, L"" );
    btnOptions->setImage( pVideo->getTexture( "media/textures/buttons/options2.png" ) );
    btnOptions->setUseAlphaChannel( true );
//    pOptionsButtonImage = pGUIEnv->addImage( sizer );
    btnOptions->setPressedImage( pVideo->getTexture( "media/textures/buttons/options1.png" ) );
    btnOptions->setDrawBorder( false );
    //   Credits button
    leftX += incrX;
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    btnCredits = pGUIEnv->addButton( sizer, 0, GUI_BTN_HELP, L"" );
    btnCredits->setImage( pVideo->getTexture( "media/textures/buttons/help2.png" ) );
    btnCredits->setUseAlphaChannel( true );
//    pCreditsButtonImage = pGUIEnv->addImage( sizer );
    btnCredits->setPressedImage( pVideo->getTexture( "media/textures/buttons/help1.png" ) );
    btnCredits->setDrawBorder( false );
    //   Exit button
    leftX += incrX;
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    btnExit = pGUIEnv->addButton( sizer, 0, GUI_BTN_CANCEL, L"" );
    btnExit->setImage( pVideo->getTexture( "media/textures/buttons/quit2.png" ) );
    btnExit->setUseAlphaChannel( true );
//    pExitButtonImage = pGUIEnv->addImage( sizer );
    btnExit->setPressedImage( pVideo->getTexture( "media/textures/buttons/quit3.png" ) );
    btnExit->setDrawBorder( false );
    //! debug data display
    pDebugData = pGUIEnv->addStaticText(
                     L"",
                     core::rect<s32>( 0, scrnHeight - 64, 256, scrnHeight ),
                     true,
                     true,
                     0,
                     -1,
                     true );
    pDebugData->setOverrideColor( video::SColor( 255, 255, 255, 0 )  );
    // starting off invisible for this - no poly count
    pDebugData->setVisible( false );
    cout << "StartMenu initGUIComponents() success" << endl;
    return true;
}

bool StartMenu::initGUIData()
{
    std::cout << "StartMenu initGUIData() success" << std::endl;
    return true;
}

bool StartMenu::initNodes()
{
    return true;
}

bool StartMenu::initCameras()
{
    cout << "Initting Cameras" << endl;
    pCamera = pSceneMgr->addCameraSceneNode();
    cout << "1" << endl;
    pCamera->setPosition( core::vector3df( 0, 0, 0 ) );
    cout << "2" << endl;
    pCamera->setTarget( core::vector3df( 0, 0, 1 ) );
    cout << "3" << endl;
    return true;
}

bool StartMenu::initWorld()
{
    return true;
}

void StartMenu::keyboardEvent( SEvent event )
{
    if ( event.KeyInput.PressedDown )
    {
        switch ( event.KeyInput.Key )
        {
//        case irr::KEY_F8:
//            pGameMgr->changeState( StartMenu::getInstance() );
//            break;
//        case irr::KEY_F9:
//            pGameMgr->changeState( GameSelection::getInstance() );
//            break;
//        case irr::KEY_F10:
//            pGameMgr->changeState( LevelSelection::getInstance() );
//            break;
//        case irr::KEY_F11:
//            pGameMgr->changeState( LvlDerelict::getInstance() );
//            break;
//        case irr::KEY_F12:
//            pGameMgr->changeState( LvlHopping::getInstance() );
//            break;
//        case irr::KEY_F7:
//            pGameMgr->changeState( GameSettings::getInstance() );
//            break;

        case irr::KEY_F2: // switch wire frame mode
//            terrain->setMaterialFlag(video::EMF_WIREFRAME, !terrain->getMaterial(0).Wireframe);
//            terrain->setMaterialFlag(video::EMF_POINTCLOUD, false);
            break;
        case irr::KEY_F3: // switch points only mode
//            terrain->setMaterialFlag(video::EMF_POINTCLOUD, !terrain->getMaterial(0).PointCloud);
//            terrain->setMaterialFlag(video::EMF_WIREFRAME, false);
            break;
        case irr::KEY_F4: // toggle detail map
//            terrain->setMaterialType(
//                terrain->getMaterial(0).MaterialType == video::EMT_SOLID ?
//                video::EMT_DETAIL_MAP : video::EMT_SOLID);
            break;
        case irr::KEY_ESCAPE:
            pGameMgr->running = false;
            break;
        default :
            break;
        }
    }
}

void StartMenu::mouseEvent( SEvent event )
{
}

void StartMenu::mouseGUIEvent( SEvent event )
{
    s32 id = event.GUIEvent.Caller->getID();
    switch ( event.GUIEvent.EventType )
    {
    case gui::EGET_BUTTON_CLICKED:
        switch ( id )
        {
        case GUI_BTN_PLAY:
            imgBg = pVideo->getTexture( "media/images/pleaseWait.png" );
            btnPlay->setVisible( false );
            btnOptions->setVisible( false );
            btnCredits->setVisible( false );
            btnExit->setVisible( false );
            title->setVisible( false );
            update();
            pGameMgr->changeState( LevelSelection::getInstance() );
            break;
        case GUI_BTN_OPTIONS:
            pGameMgr->pushState( GameSettings::getInstance() );
            break;
        case GUI_BTN_HELP:
            pGameMgr->changeState( GameHelp::getInstance() );
            break;
        case GUI_BTN_CANCEL:
            pGameMgr->running = false;
            break;
        default:
            break;
        }
        break;
    default:
        break;
    }
}

void StartMenu::loadMouseCursor( )
{
    mouseCursor =  pVideo->getTexture( "media/cursors/pointercursor.png" );
    pVideo->makeColorKeyTexture( mouseCursor, core::position2d<s32>( 0, 0 ) );
    pDevice->getCursorControl()->setVisible( false );
}

//void StartMenu::resetButtons( )
//{
//    // set highlighed button to off
//    pPlayButtonHighImage->setVisible( false );
//    pOptionsButtonHighImage->setVisible( false );
//    pCreditsButtonHighImage->setVisible( false );
//    pExitButtonHighImage->setVisible( false );
//    // set normal buttons on
//    pPlayButtonImage->setVisible( true );
//    pOptionsButtonImage->setVisible( true );
//    pCreditsButtonImage->setVisible( true );
//    pExitButtonImage->setVisible( true );
//}
//
//bool StartMenu::mouseOver( )
//{
//    // resets if over another button
//    resetButtons( );
//    // get mouse position2d<s32>
//    mousePos = pDevice->getCursorControl()->getPosition();
//    // check if over Play button
//    if ( pPlayButtonImage->getRelativePosition().isPointInside( mousePos ) )
//    {
//        pPlayButtonHighImage->setVisible( true );
//        pPlayButtonImage->setVisible( false );
//        return true;
//    }
//    // check if over Options button
//    if ( pOptionsButtonImage->getRelativePosition().isPointInside( mousePos ) )
//    {
//        pOptionsButtonHighImage->setVisible( true );
//        pOptionsButtonImage->setVisible( false );
//        return true;
//    }
//    // check if over Credits button
//    if ( pCreditsButtonImage->getRelativePosition().isPointInside( mousePos ) )
//    {
//        pCreditsButtonHighImage->setVisible( true );
//        pCreditsButtonImage->setVisible( false );
//        return true;
//    }
//    // check if over Exit button
//    if ( pExitButtonImage->getRelativePosition().isPointInside( mousePos ) )
//    {
//        pExitButtonHighImage->setVisible( true );
//        pExitButtonImage->setVisible( false );
//        return true;
//    }
//    return false;
//}
//
//void StartMenu::mouseClicked( )
//{
//    // get mouse position2d<s32>
//    mousePos = pDevice->getCursorControl()->getPosition();
//    // check if over Exit button
////    if ( pExitButtonImage->getRelativePosition().isPointInside( mousePos ) )
////        pDevice->closeDevice();
////    // check if over Options button
////    else if ( pOptionsButtonImage->getRelativePosition().isPointInside( mousePos ) )
////        pStateManager->changeState( OptionsState::createSingleton() );
////    // check if over Play button
////    else if ( pPlayButtonImage->getRelativePosition().isPointInside( mousePos ) )
////        pStateManager->changeState( Level1::createSingleton() );
////    // check if over Play button
////    else if ( pCreditsButtonImage->getRelativePosition().isPointInside( mousePos ) )
////        pStateManager->changeState( PlayerOptions::createSingleton() );
//}

void StartMenu::displayMouse( )
{
    mousePos = pDevice->getCursorControl()->getPosition();
    pVideo->draw2DImage( mouseCursor, core::position2d<s32>( mousePos.X, mousePos.Y ),
                         core::rect<s32>( 0, 0, 16, 16 ), 0, video::SColor( 255, 255, 255, 255 ), true );
//    }
}

void StartMenu::freeResources( )
{
    // clear the gui environment unless there are elements to use in the next state
    pGUIEnv->clear();
    // same with the scene manager
    pSceneMgr->clear();
    cout << "StartMenu freeResources()" << endl;
}


void StartMenu::displayDebugData()
{
    char buffer[256]; // using sprintf for its easy formatting ability
    core::stringw wstr( "Debug - FPS: " );       // debug title
    wstr += core::stringw( pVideo->getFPS() );
    wstr += "\nPoly: ";
    wstr += core::stringw( pVideo->getPrimitiveCountDrawn() );
    // get camera position
//    if ( pCamera )
//    {
//        sprintf( buffer, "\nCam: X:%.0f Y:%.0f Z:%.0f",
//                 pCamera->getPosition().X,
//                 pCamera->getPosition().Y,
//                 pCamera->getPosition().Z );
//    }
//    else
    sprintf( buffer, "No Camera\n" );
    wstr += buffer;
    // get game time
    unsigned int seconds = gameTime / 1000;
    sprintf( buffer, "%02d:%02d:%02d", seconds / 60, seconds % 60, gameTime % 1000 );
    wstr += "\nTime: ";
    wstr += buffer;
    pDebugData->setText( wstr.c_str() );
}

void StartMenu::updateTimer()
{
    timeNow = pDevice->getTimer()->getTime();
    timeDelta = timeNow - timeLast;
    timeLast = timeNow;
    gameTime += timeDelta;
}

void StartMenu::pause()
{
}

void StartMenu::resume()
{
}


























