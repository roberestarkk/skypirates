#include "LvlDerelict.h"



// define the static singleton pointer
LvlDerelict LvlDerelict::instance;
using namespace irr;
using namespace RakNet;

// define namspaces used
using namespace irr;
using namespace core;
using namespace video;
using namespace scene;
using namespace gui;
using namespace io;

void LvlDerelict::update()
{
    // begins everything
    this->updateTimer();
    rakUpdate(); // Called before any draw functions (need to find out what to draw from the other machines first)
    // tldr; don't move that shit
    this->createParticleImpacts();
    // tried to keep mesh from looking downwards, failed due to mesh being child of camera and lacking getAbsoluteRotation().
//    pCamera->setRotation(pCamera->getRotation()*core::vector3df(0,1,1));
    pVideo->beginScene( true, true, video::SColor( 255, 0, 0, 0 ) );
    //pPlayerObject->GetNetworkIDManager();
    //pPlayerObject->GetNetworkID();
    pSceneMgr->drawAll();
    pGUIEnv->drawAll();
//    cout << soundEnabled << " sound enabled" << endl;
    // pause code (if pressed the pause button)
    if( isPaused )
    {
        pVideo->draw2DImage(
            iguiwindowimage,
            core::position2d< s32 >( 0, pGameMgr->getScreenResolution().Height / 9 ),
            core::rect< irr::s32 >( 0, pGameMgr->getScreenResolution().Height / 9,
                                    pGameMgr->getScreenResolution().Width,
                                    pGameMgr->getScreenResolution().Height - ( pGameMgr->getScreenResolution().Height / 9 ) ),
            0, video::SColor( 150, 255, 255, 255 ), true );

    }

    if ( sendAnimation != MOVE_JUMP && jumpDelay )
    {
        jumpDelay = false;
    }

    // animation idle check code
    if( gameTime > idleTime)
    {
        newAnimation = ACTION_IDLE;
    }

// crosshair code

    // DON'T DELETE THE CROSSHAIRS
    // THE CROSSHAIRS ARE IMPORTANT
    core::position2d<s32> mousePos = pDevice->getCursorControl()->getPosition();

    if ( isPaused )
    {
        pVideo->draw2DImage( mousePause, core::position2d<s32>( mousePos.X, mousePos.Y ),
                             core::rect<s32>( 0, 0, 16, 16 ), 0, video::SColor( 255, 255, 255, 255 ), true );
    }
    else
    {
        pVideo->draw2DImage(
            mouseCursor,
            core::position2d<s32>( mousePos.X - 8, mousePos.Y - 8 ),
            core::rect<s32>( 0, 0, 16, 16 ),
            0,
            video::SColor( 255, 255, 255, 255 ),
            true );
    }
//	gun->setPosition( pCamera->getPosition() + core::vector3df(0,-25,-30) );
//    gun->setRotation( pCamera->getRotation() );



    // IP code, shows your IP so you can give it out to others
    guidmy = peer->GetMyGUID();
    RakNet::SystemAddress guidIP = peer->GetSystemAddressFromGuid( guidmy );
//    cout << "********* GUID IP " << ip << " **********" << endl;
//    cout << "********* GUID shit index " << guidIP.systemIndex << " **********" << endl;
//    cout << "********* GUID shit ip" << guidIP.ToString() << " **********" << endl;
//    cout << "********* GUID shit intern" << peer->GetInternalID().ToString() << " **********" << endl;


    // reload time code (if realod has been pressed)
    if ( reloadTime != 90000000 && reloadTime > gameTime )
    {
        isFiring = false;

    }

    if( reloadTime != 90000000 && reloadTime <= gameTime )
    {
        pPlayerObject->reload();
        reloadTime = 90000000;

    }

    // respawn timer, when you get killed
    if( startRespawn <= gameTime && startRespawn != 90000000 )
    {
        startGame();
        startRespawn = 90000000;
    }

    if ( gunTimer <= gameTime && gunTimer != 90000000 )
    {
        rifleOutline->setVisible( false );
        pistolOutline->setVisible( false );
        gattlingOutline->setVisible( false );
        blunderbussOutline->setVisible( false );
    }

    // each time you move, you can see more of the world
    pVideo->endScene();

    // checks and updates ammo and clip count
    tempstring = core::stringw( pPlayerObject->getClip() );
    tempstring += core::stringw( "/ " );
    tempstring += core::stringw( pPlayerObject->getAmmo() );
    ammo->setText( tempstring.c_str() );

    // checks team score/kills
    tempstring = core::stringw( imperialKills );
    imperialCount->setText( tempstring.c_str() );

    tempstring = core::stringw( rebelKills );
    rebelCount->setText( tempstring.c_str() );

    // checks health
    tempstring = core::stringw( pPlayerObject->getHealth() );
    health->setText( tempstring.c_str() );

    // check if can fire gun
    if( isFiring )
    {
        // fiyah ze veapon!
        shoot( pPlayerObject->getSelectedWeapon() );
    }


    // draws health cog & colours
    if( pPlayerObject->getHealth() > 75 )
    {
        healthCogImage = pVideo->getTexture( "media/images/healthTopRightmuckfull.png" );
    }
    else if( pPlayerObject->getHealth() > 50 && pPlayerObject->getHealth() < 75 )
    {
        healthCogImage = pVideo->getTexture( "media/images/healthTopRightmuck75.png" );
    }
    else if( pPlayerObject->getHealth() > 25 && pPlayerObject->getHealth() < 50 )
    {
        healthCogImage = pVideo->getTexture( "media/images/healthTopRightmuck50.png" );
    }
    else if( pPlayerObject->getHealth() < 25 )
    {
        healthCogImage = pVideo->getTexture( "media/images/healthTopRightmuck25.png" );
    }
//    if(  pPlayerObject->getHealth() < 20 )
//    {
//        healthCogImage = pVideo->getTexture("media/images/healthTopRight20.png");
//    }
    healthCog->setImage( healthCogImage );
    // how does I transparency?
    healthCog->setUseAlphaChannel( true );
    healthCog->draw();


    //cout << "******** " << serverCount << endl;
    // update camera
    tempRotation = pCamera->getRotation();
    tempRotation.X = ( -tempRotation.X );
    tempRotation.Y = 0;
    tempRotation.Z = 0;

    // loads game entities
    // testing all the entities in the entityArray to see whether they've been picked up.
    // the number of entities in the array is hard coded.
    for ( unsigned index = 0; index <= 512 ; ++index )
    {
        entityBox = &entityArray[index];
        tempVectorEntity = pCamera->getAbsolutePosition();
        tempVectorEntity2 = entityBox->getPosition();
        distance = tempVectorEntity.getDistanceFrom( tempVectorEntity2 );
        testEntityPickup( entityBox );
        entityBox = NULL;
    }


//    playerMesh->setRotation(tempRotation);


//
//  tempPosition = pCamera->getPosition();
//
//    tempPosition.X += 0;
//    tempPosition.Y  += -35;
//    tempPosition.Z  += -10;
//
//    playerMesh->setPosition(tempPosition);
    // checks sound then tries for 3D sound
    if( pGameMgr->isSoundAvailable() )
    {
        // the direction the listener looks into for stereo effects
        // not using pCamera->getTarget() as large scale scene
        float x = 1, y = pCamera->getPosition().Y, z = 1;
        if ( pCamera->getPosition().X > pCamera->getTarget().X )
            x = -1;
        if ( pCamera->getPosition().Z > pCamera->getTarget().Z )
            z = -1;
        core::vector3df lookDirection( x, y, z );
        pGameMgr->updateSounds( pCamera->getPosition(), lookDirection );
    }
    int y = pCamera->getAbsolutePosition().Y;

    // checks if player is dead or has fallen off the world then respawns
    if ( y < -5000 || pPlayerObject->isDead() == true  )
    {
        if( iAmDied || y < -5000 )
            killPlayer();
    }
    if ( ( theBell <= gameTime ) && pPlayerObject->isDead() == true )
    {
        respawnPlayer();
    }
    pPlayerObject->setPosition( pCamera->getAbsolutePosition() );

    started = true;

// if running in debug mode, show the debug data
#ifdef _DEBUG
    if( showDebugData )
        this->debugData();
#endif
    // if we quit/disconnect then go back to main menu
    if( disconnect )
    {
        initGuns( 1 );
        pGameMgr->changeState( StartMenu::getInstance() );
    }


}


void LvlDerelict::createParticleImpacts()
{
    u32 now = pDevice->getTimer()->getTime();

    for ( s32 i = 0; i < ( s32 )Impacts.size(); ++i )
        if ( now > Impacts[i].when )
        {
            // create smoke particle system
            scene::IParticleSystemSceneNode* pas = 0;

            pas = pSceneMgr->addParticleSystemSceneNode( false, 0, -1, Impacts[i].pos );
            // emmitter code
            scene::IParticleEmitter* em = pas->createBoxEmitter(
                                              core::aabbox3d<f32>( -5, -5, -5, 5, 5, 5 ),
                                              Impacts[i].outVector,
                                              20,
                                              40,
                                              video::SColor( 0, 255, 255, 255 ),
                                              video::SColor( 0, 255, 255, 255 ),
                                              1200,
                                              1600,
                                              20,
                                              core::dimension2d<f32>( 10.0f, 10.0f ),
                                              core::dimension2d<f32>( 10.0f, 10.0f ) );

            pas->setEmitter( em );
            em->drop();
            // an array for the emmitters
            for ( unsigned index = 0; index < flamesArray.size(); ++index )
            {
                scene::IParticleAffector* paf = flamesArray[ index ]->createFadeOutParticleAffector();
                pas->addAffector( paf );
                paf->drop();
            }
            // sets the material/image for the emmitter
            pas->setMaterialFlag( video::EMF_LIGHTING, false );
            pas->setMaterialFlag( video::EMF_ZWRITE_ENABLE, false );
            pas->setMaterialTexture( 0, pVideo->getTexture( "media/images/smoke.bmp" ) );
            pas->setMaterialType( video::EMT_TRANSPARENT_VERTEX_ALPHA );
            // animation for emmitter
            scene::ISceneNodeAnimator* anim = pSceneMgr->createDeleteAnimator( 2000 );
            pas->addAnimator( anim );
            anim->drop();

//            ISound* sound =
            // sounnd of emmitter
            if(soundEnabled)
                pGameMgr->getSoundEngine()->play2D( impactSound );

            //! Randomised rebounds sounds to occur each impact
//            int randomRebounds= srand(5);
//            switch(randomRebounds)
//            {
//                case 1:
//                pGameMgr->getSoundEngine()->play2D( impactSound );
//                break;
//                case 2:
//                pGameMgr->getSoundEngine()->play2D( impactTwoSound );
//                break;
//                case 3:
//                pGameMgr->getSoundEngine()->play2D( impactThreeSound );
//                break;
//                case 4:
//                pGameMgr->getSoundEngine()->play2D( impactFourSound );
//                break;
//                case 5:
//                pGameMgr->getSoundEngine()->play2D( impactFiveSound );
//                break;
//            }

//            if (sound)
//            {
//                // adjust max value a bit to make to sound of an impact louder
//                sound->setMinDistance(400);
//                sound->drop();
//            }

            // delete entry
            // deletes emmitter after animation has ended
            Impacts.erase( i );
            i--;
        }
}


bool LvlDerelict::killPlayer()
{
    cout << "killPlayer() begun" << endl;
    // set health to 0
    pPlayerObject->setDead( true ) ;
    // death sound plays
    if(soundEnabled)
        pGameMgr->getSoundEngine()->play2D( deathSound );
    theBell = gameTime + 650;
    iAmDied = 0;
    pPlayerObject->dropFlag();
    if ( pPlayerObject->getTeam() != pPlayerObject->Imperial )
    {
        entityArray[250].setEntityTimer(gameTime - 2);
        testEntityPickup( &entityArray[250] );
        entityArray[250].getEntityMesh()->setVisible(true);
    }
    else
    {
        entityArray[251].setEntityTimer(gameTime - 2);
        testEntityPickup( &entityArray[251] );
        entityArray[251].getEntityMesh()->setVisible(true);
    }
    pPlayerObject->setWeaponAvailability( 1, true );
    pPlayerObject->setWeaponAvailability( 2, false );
    pPlayerObject->setWeaponAvailability( 3, false );
    pPlayerObject->setWeaponAvailability( 4, false );

    // deletes camera and guns
    pCamera->removeAll();
    gun2 = NULL;
    pCamera->remove();
    // restarts/respawn
    initCameras();

    return true;
}


bool LvlDerelict::respawnPlayer()
{
    // adds score
    cout << "respawnPlayer() begun" << endl;
    iAmDied = 1;
    initCollisionDetection();
    // resets ammo
    pPlayerObject->setSelectedWeapon( 1 );
    pPlayerObject->setAmmo( 30 );
    pPlayerObject->setClip( pPlayerObject->MAX_CLIP_RIFLE );
    pPlayerObject->setSelectedWeapon( 2 );
    pPlayerObject->setAmmo( 50 );
    pPlayerObject->setClip( pPlayerObject->MAX_CLIP_PISTOL );
    pPlayerObject->setSelectedWeapon( 3 );
    pPlayerObject->setAmmo( 200 );
    pPlayerObject->setClip( pPlayerObject->MAX_CLIP_GATLING );
    pPlayerObject->setSelectedWeapon( 4 );
    pPlayerObject->setAmmo( 20 );
    pPlayerObject->setClip( pPlayerObject->MAX_CLIP_BLUNDERBUSS );
    pPlayerObject->setSelectedWeapon( 1 );
    pPlayerObject->health = 100;
    initGuns( 1 );

    pPlayerObject->setDead( false );
    clientDiedOnce = false;
    return true;
}


void LvlDerelict::startGame()
{
    // kills and respawns player
    killPlayer();
    respawnPlayer();
    // resets team scores
    imperialKills = 0;
    rebelKills = 0;
    //gunTimer = 0;
}

void LvlDerelict::reloadTimer()
{

    if(reloadTime != 90000000)
        return;

        if(soundEnabled)
        {
            pGameMgr->getSoundEngine()->play2D( reloadCallSound );
            pGameMgr->getSoundEngine()->play2D( reloadSound );
        }

    // checks weapon selected then stops firing and then able to fire after the timer
    switch ( pPlayerObject->weaponSelected )
    {
    case 1:
        if( pPlayerObject->MAX_CLIP_RIFLE )
        {
            reloadTime = gameTime + 2000;
            isFiring = false;
            //pPlayerObject->reload();

        }

        break;
    case 2:
        if( pPlayerObject->MAX_CLIP_PISTOL )
        {
            reloadTime = gameTime + 2000;
            //pPlayerObject->reload();
            isFiring = false;
        }

        break;
    case 3:
        if( pPlayerObject->MAX_CLIP_GATLING )
        {
            reloadTime = gameTime + 5000;
            //pPlayerObject->reload();
            isFiring = false;
        }

        break;
    case 4:
        if( pPlayerObject->MAX_CLIP_BLUNDERBUSS )
        {
            reloadTime = gameTime + 2000;
            //pPlayerObject->reload();
            isFiring = false;
        }


        break;

    default:
        break;
    }

            // play reloading sound


}


void LvlDerelict::shoot( int selected, int incoming, core::vector3df position, core::vector3df endPoint )
{
    if(isPaused)
        return;
        // if you are not reloading, you can shoot
        if(reloadTime != 90000000)
        return;

    // if you are not dead, you can shoot
    // if you have ammo, you can shoot
    if( pPlayerObject->isDead() == false )
    {
        if( pPlayerObject->getClip() <= 0 )
        {
            if(soundEnabled)
                pGameMgr->getSoundEngine()->play2D( gunEmptySound );
            isFiring = false;
            return;
        }
        if ( cooldown  <= gameTime || incoming == 1 )
        {
            if ( ! pCamera || ! worldCollisionSelector )
                return;

            core::vector3df start;
            core::vector3df end;
            if( !incoming )
            {

                // get line of camera
                // shoots bullet from gun
                start = gun->getAbsolutePosition();
                // in case of dual-wielding pistols, swap the start position to the other gun occasionally(alternately)
                // trajectory of bullet
                end = ( pCamera->getTarget() - start );
                end.normalize();
                start += end * 8.0f;
                end = start + ( end * pCamera->getFarValue() );
                //play a sound to show that the gun has been fired, regardless of impact
                // fire from the other gun if dual-wielding
                if( ( selected == 2 ) and ( primaryWeaponFired == false ) )
                {
                    primaryWeaponFired = true;
                }
                else if( ( selected == 2 ) and ( primaryWeaponFired == true ) )
                {
                    start = gun2->getAbsolutePosition();
                    //            start += end * 8.0f;
                    primaryWeaponFired = false;
                }

                // reset end to the new spread(less accurate) end point of it's trajectory.
                // bullet spread
                end = spreadBullet( end );

                sStart = start;
                sEnd = end;
                shooting = true;
                guidmy = peer->GetMyGUID();
            }
            else
            {
                start = position;
                end = endPoint;
                cout << "making boolets" << endl;
                cout << start.X << endl;
                cout << start.Y << endl;
                cout << start.Z << endl;
                cout << end.X << endl;
                cout << end.Y << endl;
                cout << end.Z << endl;

            }

            // in case there is an impact
            // when impacting an object
            SParticleImpact imp;
            imp.when = 0;
            core::triangle3df triangle;
            core::line3d<f32> line( start, end );

            // test to see whether the bullet hits another player
            imp = testBulletPlayerCollision( line );
            if( imp.when == 1 )
            {
                end = imp.pos;
            }

            // get intersection point with map
            if( imp.when != 1 )
            {
                const scene::ISceneNode* hitNode;
                if ( pSceneMgr->getSceneCollisionManager()->getCollisionPoint(
                            line, worldCollisionSelector, end, triangle, hitNode ) )
                {
                    // collides with building
                    core::vector3df out = triangle.getNormal();
                    out.setLength( 0.03f );

                    imp.when = 1;
                    imp.outVector = out;
                    imp.pos = end;
                }

                //	else
                //	{
                for ( unsigned index = 0; index < spheresArray.size(); ++index )
                {
                    if ( pSceneMgr->getSceneCollisionManager()->getCollisionPoint(
                                line, spheresArray[ index ], end, triangle, hitNode ) )
                    {
                        if( const_cast<scene::ISceneNode*>( hitNode )->isVisible( ) )
                        {
                            cout << "Hit Sphere " << hitNode->getID() << endl;
                            flamesArray[ index ]->setPosition( hitNode->getPosition() );
                            flamesArray[ index ]->setVisible( true );
                            const_cast<scene::ISceneNode*>( hitNode )->setVisible( false );
                        }
                    }
                }

            }


            if( imp.when != 1 )
                imp.pos = end;
            f32 length = ( f32 )( imp.pos - start ).getLength();
            const f32 speed = 16.8f;
            u32 time = ( u32 )( length / speed );

            //	}
            // create fire ball/bullet
            scene::ISceneNode* node = 0;
            node = pSceneMgr->addBillboardSceneNode( 0,
                    core::dimension2d<f32>( 25, 25 ), start );

            node->setMaterialFlag( video::EMF_LIGHTING, false );
            node->setMaterialType( video::EMT_TRANSPARENT_ADD_COLOR );
            anim = 0;

            switch ( selected )
            {
            case 1:
                node->setMaterialTexture( 0, pVideo->getTexture( "media/images/fireball.bmp" ) );
                if(soundEnabled)
                    pGameMgr->getSoundEngine()->play2D( shotFourSound );
                // set flight line
                anim = pSceneMgr->createFlyStraightAnimator( start, end, time );
                node->addAnimator( anim );
                anim->drop();
                break;
            case 2:
                node->setMaterialTexture( 0, pVideo->getTexture( "media/images/fireball2.bmp" ) );
                if(soundEnabled)
                    pGameMgr->getSoundEngine()->play2D( shotSound );

                // set flight line
                anim = pSceneMgr->createFlyStraightAnimator( start, end, time );
                node->addAnimator( anim );
                anim->drop();
                break;
            case 3:
                node->setMaterialTexture( 0, pVideo->getTexture( "media/images/fireball3.bmp" ) );
                if(soundEnabled)
                    pGameMgr->getSoundEngine()->play2D( shotThreeSound );

                // set flight line
                anim = pSceneMgr->createFlyStraightAnimator( start, end, time );
                node->addAnimator( anim );
                anim->drop();
                break;
            case 4:
                node->setMaterialTexture( 0, pVideo->getTexture( "media/images/fireball4.bmp" ) );
                if(soundEnabled)
                    pGameMgr->getSoundEngine()->play2D( shotTwoSound );

                // set flight line
                anim = pSceneMgr->createFlyStraightAnimator( start, end, time );
                node->addAnimator( anim );
                anim->drop();
                break;

            default:
                break;
            }
            // animation
            if( !incoming )
            {
                // firing animation plays
                if( pPlayerObject->getSelectedWeapon() == 1 )
                {
                    gun->setFrameLoop( 180, 195 );
                    gun->animateJoints();
                    gun->setAnimationSpeed( 40 );
                    gun->setLoopMode( false );
                }
                if( pPlayerObject->getSelectedWeapon() == 2 )
                {
                    if( primaryWeaponFired )
                    {
                        gun->setFrameLoop( 200, 210 );
                        gun->animateJoints();
                        gun->setAnimationSpeed( 40 );
                        gun->setLoopMode( false );
                    }
                    else
                    {
                        gun2->setFrameLoop( 200, 210 );
                        gun2->animateJoints();
                        gun2->setAnimationSpeed( 40 );
                        gun2->setLoopMode( false );
                    }
                }

                if( pPlayerObject->getSelectedWeapon() == 3 )
                {
//                    gun->setFrameLoop( 210, 210 );
//                    gun->animateJoints();
//                    gun->setAnimationSpeed( 40 );
//                    gun->setLoopMode( false );
                }

                if( pPlayerObject->getSelectedWeapon() == 4 )
                {
                    gun->setFrameLoop( 180, 195 );
                    gun->animateJoints();
                    gun->setAnimationSpeed( 40 );
                    gun->setLoopMode( false );
                }
            }

            anim = pSceneMgr->createDeleteAnimator( time );
            node->addAnimator( anim );
            anim->drop();

            if ( imp.when )
            {
                // create impact note
                imp.when = pDevice->getTimer()->getTime() + ( time - 100 );
                Impacts.push_back( imp );
            }
            // shooting cooldown for each weapon
            // ------ may change for balancing issues
            if( !incoming )
            {
                if( pPlayerObject->getSelectedWeapon() == 1 )
                {
                    cooldown = gameTime + 500;
                }
                else if( pPlayerObject->getSelectedWeapon() == 2 )
                {
                    cooldown = gameTime + 250;
                }
                else if( pPlayerObject->getSelectedWeapon() == 3 )
                {
                    cooldown = gameTime + 100;
                }
                else if( pPlayerObject->getSelectedWeapon() == 4 )
                {
                    cooldown = gameTime + 800;
                }
                pPlayerObject->setClip( pPlayerObject->getClip() - 1 );
            }
            else
                incoming = 0;
        }
    }
}

void LvlDerelict::flashWeapons()
{
    if( pPlayerObject->getWeaponAvailability( 1 ) )
        rifleOutline->setVisible( true );
    if( pPlayerObject->getWeaponAvailability( 2 ) )
        pistolOutline->setVisible( true );
    if( pPlayerObject->getWeaponAvailability( 3 ) )
        gattlingOutline->setVisible( true );
    if( pPlayerObject->getWeaponAvailability( 4 ) )
        blunderbussOutline->setVisible( true );

    gunTimer = gameTime + 1000;
}


bool LvlDerelict::testEntityPickup( Entity* entityBox )
{
    // TODO test whether the player is inside the bounding box of the pickup, and if-so, set the pickup to visible for a while and add the effect to the player.
    //ammoRespawn = timeDelta + 2000;

    if( distance < 90 ) // the hard-coded radius of pickup-ableness
    {
        // pick up flag (based on team)
        if ( entityBox->getType() == 'f' )
        {
            if ( entityBox->getSubtype() == 'i' )
            {
                if ( pPlayerObject->getTeam() == pPlayerObject->Rebel )
                {
                    pPlayerObject->pickupFlag( entityBox->getValue() );
                    entityBox->getEntityMesh()->setVisible( false );
                    entityBox->setEntityTimer(99999);
                }
                else if(pPlayerObject->getTeam() == pPlayerObject->Imperial && pPlayerObject->carryingFlag == true && !clientsNeedUpdate)
                {
                    cout << "RebelFlag Dropped" << endl;
                    pPlayerObject->dropFlag();
                    entityArray[251].setEntityTimer(gameTime + 2000);
                    if(isServer)
                    {
                        cout << "You Is Servar, Applying Score change." << endl;
                        imperialKills += entityArray[251].getValue();
                        clientsNeedUpdate = true;
                    }

                }
            }
            if ( entityBox->getSubtype() == 'r' )
            {
                if ( pPlayerObject->getTeam() == pPlayerObject->Imperial )
                {
                    pPlayerObject->pickupFlag( entityBox->getValue() );
                    entityBox->getEntityMesh()->setVisible( false );
                    entityBox->setEntityTimer(99999);
                }
                else if(pPlayerObject->getTeam() == pPlayerObject->Rebel && pPlayerObject->carryingFlag == true)
                {
                    cout << "ImperialFlag Dropped" << endl;
                    pPlayerObject->dropFlag();
                    entityArray[250].setEntityTimer(gameTime + 2000);
                    if(!isServer)
                    {
                        cout << "You Is Client, Setting Commit True." << endl;
                        clientScoreUpdate = true;
                    }
                }

            }
        }
        // health box
        if ( entityBox->getType() == 'b' )
        {
            if( entityBox->getSubtype() == 'h' )
            {
                if( entityBox->getEntityMesh()->isVisible() )
                {
                    pPlayerObject->pickupHealth( healthDrop.getValue() );
                    entityBox->getEntityMesh()->setVisible( false );
                    entityBox->setEntityTimer( gameTime + 15000 );
                    if(soundEnabled)
                        pGameMgr->getSoundEngine()->play2D( healthSound );
                }
            }
        }
        // ammo box
        if( entityBox->getType() == 'a' )
        {
            if( entityBox->getSubtype() == 'r' )
            {
                if( entityBox->getEntityMesh()->isVisible() )
                {
                    if ( !pPlayerObject->getWeaponAvailability( 1 ) )
                    {
                        pPlayerObject->setWeaponAvailability( 1, true );

                        flashWeapons();
                    }
                    else
                    {
                        pPlayerObject->pickupAmmo( rifleAmmoDrop.getValue(), 1 );
                    }
                    entityBox->getEntityMesh()->setVisible( false );
                    entityBox->setEntityTimer( gameTime + 8000 );
                    if(soundEnabled)
                        pGameMgr->getSoundEngine()->play2D( pickupSoundTwo );

                }


            }
            else if( entityBox->getSubtype() == 'p' )
            {
                if( entityBox->getEntityMesh()->isVisible() )
                {
                    if ( !pPlayerObject->getWeaponAvailability( 2 ) )
                    {
                        pPlayerObject->setWeaponAvailability( 2, true );

                        flashWeapons();
                    }
                    else
                    {
                        pPlayerObject->pickupAmmo( pistolAmmoDrop.getValue(), 2 );
                    }
                    entityBox->getEntityMesh()->setVisible( false );
                    entityBox->setEntityTimer( gameTime + 8000 );
                    if(soundEnabled)
                        pGameMgr->getSoundEngine()->play2D( pickupSoundOne );

                }


            }
            else if( entityBox->getSubtype() == 'g' )
            {
                if( entityBox->getEntityMesh()->isVisible() )
                {
                    if ( !pPlayerObject->getWeaponAvailability( 3 ) )
                    {
                        pPlayerObject->setWeaponAvailability( 3, true );

                        flashWeapons();
                    }
                    else
                    {
                        pPlayerObject->pickupAmmo( gattlingAmmoDrop.getValue(), 3 );
                    }
                    entityBox->getEntityMesh()->setVisible( false );
                    entityBox->setEntityTimer( gameTime + 8000 );
                    if(soundEnabled)
                        pGameMgr->getSoundEngine()->play2D( pickupSoundFour );

                }


            }
            else if( entityBox->getSubtype() == 'b' )
            {
                if( entityBox->getEntityMesh()->isVisible() )
                {
                    if ( !pPlayerObject->getWeaponAvailability( 4 ) )
                    {
                        pPlayerObject->setWeaponAvailability( 4, true );

                        flashWeapons();
                    }
                    else
                    {
                        pPlayerObject->pickupAmmo( blunderbussAmmoDrop.getValue(), 4 );
                    }
                    entityBox->getEntityMesh()->setVisible( false );
                    entityBox->setEntityTimer( gameTime + 8000 );
                    if(soundEnabled)
                        pGameMgr->getSoundEngine()->play2D( pickupSoundThree );

                }
            }
        }
    }

    // respawning of the drops
    if ( entityBox->getType() )
    {
        if( entityBox->getEntityMesh()->isVisible() == false && entityBox->getEntityTimer() <= gameTime && entityBox->getEntityTimer() != 99999)
        {
            entityBox->getEntityMesh()->setVisible( true );
        }
    }
    return true;
}


LvlDerelict::SParticleImpact LvlDerelict::testBulletPlayerCollision( core::line3d<f32> line )
{
    collidedNode = pSceneMgr->getSceneCollisionManager()->getSceneNodeFromRayBB( line );
//    cout << "BLERBLERBLERBLER: " << collidedNode->getID() << endl;

    SParticleImpact imp;
    imp.when = 0;
    core::triangle3df triangle;

    // get intersection point with enemy
    const scene::ISceneNode* hitNode;
    if ( pSceneMgr->getSceneCollisionManager()->getCollisionPoint(
                line, enemyCharacterSelector, line.end, triangle, hitNode ) )
    {
        // collides with character
        core::vector3df out = triangle.getNormal();
        out.setLength( 0.03f );

        imp.when = 1;
        imp.outVector = out;
        imp.pos = line.end;
        if(soundEnabled)
            pGameMgr->getSoundEngine()->play2D( hitSound );
        enemyHit = true;

        cout << "enemy dededed" << endl;
    }



    return imp;
}

core::vector3df LvlDerelict::spreadBullet( core::vector3df end )
{
    int randX, randY, randZ = 0;
    // random accuracy within bounds
    if( pPlayerObject->getSelectedWeapon() == 1 )
    {
        randX = randFloat( -200 , 200 );
        randY = randFloat( -200 , 200 );
        randZ = randFloat( -200 , 200 );
    }
    else if( pPlayerObject->getSelectedWeapon() == 2 )
    {
        randX = randFloat( -400 , 400 );
        randY = randFloat( -400 , 400 );
        randZ = randFloat( -400 , 400 );
    }
    else if( pPlayerObject->getSelectedWeapon() == 3 )
    {
        randX = randFloat( -1500 , 1500 );
        randY = randFloat( -1500 , 1500 );
        randZ = randFloat( -1500 , 1500 );
    }
    else if( pPlayerObject->getSelectedWeapon() == 4 )
    {
        randX = randFloat( -450 , 450 );
        randY = randFloat( -450 , 450 );
        randZ = randFloat( -450 , 450 );
    }


    core::vector3df randomiserValues = core::vector3df( randX, randY, randZ );
//    cout << "End: " << end.X << ", " << end.Y <<", " << end.Z << endl;
//
    end += randomiserValues;
//    cout << "EndNOW: " << end.X << ", " << end.Y <<", " << end.Z << endl;

    return end;
}

//{ Raytrace Code
//    // This call is all you need to perform ray/triangle collision on every scene node
//    // that has a triangle selector It finds the nearest
//    // collision point/triangle, and returns the scene node containing that point.
//    // Irrlicht provides other types of selection, including ray/triangle selector,
//    // ray/box and ellipse/triangle selector, plus associated helpers.
//    // See the methods of ISceneCollisionManager
//    core::line3d<f32> ray;
//    ray.start = pCamera->getPosition();
//    ray.end = ray.start + (pCamera->getTarget() - ray.start).normalize() * 1000.0f;
//
//    // Tracks the current intersection point with the level or a mesh
//    core::vector3df intersection;
//    // Used to show which triangle has been hit
//    core::triangle3df hitTriangle;
//
//    scene::ISceneNode * selectedSceneNode =
//        pSceneMgr->getSceneCollisionManager()->getSceneNodeAndCollisionPointFromRay(
//                ray,
//                intersection,   // This will be the position of the collision
//                hitTriangle,    // This will be the triangle hit in the collision
//                0,      // All nodes are pickable
//                0 );    // Check the entire scene (this is actually the implicit default)
//
//    // If the ray hit anything, move the billboard to the collision position
//    // and draw the triangle that was hit.
//    if(selectedSceneNode)
//    {
////        bill->setPosition(intersection);
//
//        // We need to reset the transform before doing our own rendering.
//        pVideo->setTransform(video::ETS_WORLD, core::matrix4());
////        pVideo->setMaterial(material);
//        pVideo->draw3DTriangle(hitTriangle, video::SColor(0,255,0,0));
//    }
//}

void LvlDerelict::selectAnimation()
{
    // test what the other player is currently doing, then animate it.
    if( currentAction != newAnimation )
    {
        switch ( newAnimation )
        {
        case MOVE_FORWARD:
            switch ( newWeapon )
            {
            case 1:
                friendMesh->setFrameLoop( 25, 45 );
                animationTimer = (friendMesh->getEndFrame() - friendMesh->getStartFrame()) / friendMesh->getAnimationSpeed();
                break;
            case 2:
                friendMesh->setFrameLoop( 930, 960 );
                animationTimer = (friendMesh->getEndFrame() - friendMesh->getStartFrame()) / friendMesh->getAnimationSpeed();

                break;
            case 3:
                friendMesh->setFrameLoop( 330, 365 );
                animationTimer = (friendMesh->getEndFrame() - friendMesh->getStartFrame()) / friendMesh->getAnimationSpeed();

                break;
            case 4:
                friendMesh->setFrameLoop( 645, 665 );
                animationTimer = (friendMesh->getEndFrame() - friendMesh->getStartFrame()) / friendMesh->getAnimationSpeed();


                break;

            default:
                break;
            }

            friendMesh->setLoopMode( false );
            friendMesh->animateJoints();
            currentAction = MOVE_FORWARD;
            idleTime = gameTime + animationTimer;
            break;
        case MOVE_BACKWARD:
            switch ( newWeapon )
            {
            case 1:
                friendMesh->setFrameLoop( 61, 81 );
                animationTimer = (friendMesh->getEndFrame() - friendMesh->getStartFrame()) / friendMesh->getAnimationSpeed();
                break;
            case 2:
                friendMesh->setFrameLoop( 992, 1018 );
                animationTimer = (friendMesh->getEndFrame() - friendMesh->getStartFrame()) / friendMesh->getAnimationSpeed();
                break;
            case 3:
                friendMesh->setFrameLoop( 387, 404 );
                animationTimer = (friendMesh->getEndFrame() - friendMesh->getStartFrame()) / friendMesh->getAnimationSpeed();

                break;
            case 4:
                friendMesh->setFrameLoop( 681, 701 );
                animationTimer = (friendMesh->getEndFrame() - friendMesh->getStartFrame()) / friendMesh->getAnimationSpeed();

                break;

            default:
                break;
            }

            friendMesh->setLoopMode( false );
            friendMesh->animateJoints();
            currentAction = MOVE_BACKWARD;
            idleTime = gameTime + animationTimer;
            break;
        case MOVE_LEFT:
            switch ( newWeapon )
            {
            case 1:
                friendMesh->setFrameLoop( 160, 180 );
                animationTimer = (friendMesh->getEndFrame() - friendMesh->getStartFrame()) / friendMesh->getAnimationSpeed();
                break;
            case 2:
                friendMesh->setFrameLoop( 930, 960 );
                animationTimer = (friendMesh->getEndFrame() - friendMesh->getStartFrame()) / friendMesh->getAnimationSpeed();

                break;
            case 3:
                friendMesh->setFrameLoop( 160, 180 );
                animationTimer = (friendMesh->getEndFrame() - friendMesh->getStartFrame()) / friendMesh->getAnimationSpeed();
                break;
            case 4:
                friendMesh->setFrameLoop( 780, 800 );
                animationTimer = (friendMesh->getEndFrame() - friendMesh->getStartFrame()) / friendMesh->getAnimationSpeed();

                break;

            default:
                break;
            }

            friendMesh->setLoopMode( false );
            friendMesh->animateJoints();
            currentAction = MOVE_LEFT;
            idleTime = gameTime + animationTimer;
            break;
        case MOVE_RIGHT:
            switch ( newWeapon )
            {
            case 1:
                friendMesh->setFrameLoop( 180, 160 );
                animationTimer = (friendMesh->getEndFrame() - friendMesh->getStartFrame()) / friendMesh->getAnimationSpeed();
                break;
            case 2:
                friendMesh->setFrameLoop( 960, 930 );
                animationTimer = (friendMesh->getEndFrame() - friendMesh->getStartFrame()) / friendMesh->getAnimationSpeed();

                break;
            case 3:
                friendMesh->setFrameLoop( 180, 160 );
                animationTimer = (friendMesh->getEndFrame() - friendMesh->getStartFrame()) / friendMesh->getAnimationSpeed();
                break;
            case 4:
                friendMesh->setFrameLoop( 800, 780 );
                animationTimer = (friendMesh->getEndFrame() - friendMesh->getStartFrame()) / friendMesh->getAnimationSpeed();

                break;

            default:
                break;
            }

            friendMesh->setLoopMode( false );
            friendMesh->animateJoints();
            currentAction = MOVE_RIGHT;
            idleTime = gameTime + animationTimer;
            break;
        case MOVE_JUMP:
            switch ( newWeapon )
            {
            case 1:
                friendMesh->setFrameLoop( 140, 150 );
                animationTimer = (friendMesh->getEndFrame() - friendMesh->getStartFrame()) / friendMesh->getAnimationSpeed();
                break;
            case 2:
                friendMesh->setFrameLoop( 1065, 1095 );
                animationTimer = (friendMesh->getEndFrame() - friendMesh->getStartFrame()) / friendMesh->getAnimationSpeed();
                break;
            case 3:
                friendMesh->setFrameLoop( 405, 430 );
                animationTimer = (friendMesh->getEndFrame() - friendMesh->getStartFrame()) / friendMesh->getAnimationSpeed();

                break;
            case 4:
                friendMesh->setFrameLoop( 760, 770 );
                animationTimer = (friendMesh->getEndFrame() - friendMesh->getStartFrame()) / friendMesh->getAnimationSpeed();

                break;

            default:
                break;
            }

            friendMesh->setLoopMode( false );
            friendMesh->animateJoints();
            currentAction = MOVE_JUMP;
            idleTime = gameTime + animationTimer;
            break;
        case ACTION_RELOAD:
            switch ( newWeapon )
            {
            case 1:
                friendMesh->setFrameLoop( 200, 250 );
                animationTimer = (friendMesh->getEndFrame() - friendMesh->getStartFrame()) / friendMesh->getAnimationSpeed();
                break;
            case 2:
                friendMesh->setFrameLoop( 1120, 1180 );
                animationTimer = (friendMesh->getEndFrame() - friendMesh->getStartFrame()) / friendMesh->getAnimationSpeed();
                break;
            case 3:
                friendMesh->setFrameLoop( 520, 600 );
                animationTimer = (friendMesh->getEndFrame() - friendMesh->getStartFrame()) / friendMesh->getAnimationSpeed();

                break;
            case 4:
                friendMesh->setFrameLoop( 830, 855 );
                animationTimer = (friendMesh->getEndFrame() - friendMesh->getStartFrame()) / friendMesh->getAnimationSpeed();

                break;

            default:
                break;
            }
            friendMesh->setLoopMode( false );
            friendMesh->animateJoints();
            currentAction = ACTION_RELOAD;
            idleTime = gameTime + animationTimer;
            break;
        case ACTION_IDLE:
            switch ( newWeapon )
            {
            case 1:
                friendMesh->setFrameLoop( 95, 134 );
                break;
            case 2:
                friendMesh->setFrameLoop( 1025, 1064 );
                break;
            case 3:
                friendMesh->setFrameLoop( 435, 484 );

                break;
            case 4:
                friendMesh->setFrameLoop( 715, 754 );

                break;

            default:
                break;
            }

            friendMesh->setLoopMode( false );
            friendMesh->animateJoints();
            currentAction = ACTION_RELOAD;
            break;
        }
    }
}

bool LvlDerelict::initState( GameMgr & game )
{
    cout << "initState() begun" << endl;
    // set the local pointer to the game engine reference address
    pGameMgr = &game;
    // init the other Irrlicht system pointers - eventually in State Manager
    // looks through computer to find video card
    pDevice = pGameMgr->getDevice();
    pVideo = pGameMgr->getVideoDriver();
    pSceneMgr = pGameMgr->getSceneManager();
    pGUIEnv = pGameMgr->getGUIEnvironment();
    pDataMgr = pGameMgr->getDataMgr();
    pPlayerObject = pGameMgr->getPlayerObject();
//    pSound = pGameMgr->getSoundEngine();
    // initialise mouse cursor image
    mouseCursor = pVideo->getTexture( "media/cursors/crosshair.png" );
    mousePause = pVideo->getTexture( "media/cursors/pointercursor.png" );

    pVideo->makeColorKeyTexture( mousePause, core::position2d<s32>( 0, 0 ) );
    // set the transparent colour - the pixel in pos 0,0
    pVideo->makeColorKeyTexture( mouseCursor, core::position2d<s32>( 0, 0 ) );
    // disable system mouse cursor to draw custom cursor
    pDevice->getCursorControl()->setVisible( false );
    // init the keys array/keyboard
    pGameMgr->clearKeys();
    cout << "Run Init Functions" << endl;
    isPaused = false;
    // the order of the following function calls may be important
    // loads/checks each function works
    // if there are dependencies so rearrange/remove as reqd.
    if ( not this->initData() )
        return false;
    if ( not this->initCameras() )
        return false;
    if ( not this->initWorld() )
        return false;
    if ( not this->initEntities() )
        return false;
    if ( not this->initCollisionDetection() )
        return false;
    if ( not this->initGuns( 1 ) )
        return false;
    if ( not this->initGUI() )
        return false;
    if ( not this->rakConnect() )
        return false;
    if ( not this->initHUD() )
        return false;

    if ( pGameMgr->isSoundAvailable() )
        this->initSounds();
    else
        std::cout << "**** init() Sound Engine Not Loaded" << std::endl;
//        createParticleImpacts();


    cout << "LvlDerelict init() success" << endl;
    return true;
}

bool LvlDerelict::initData()
{
    cout << "initData() begun" << endl;
    timeNow = timeLast = gameTime = timeDelta = 0;
    desiredFPS = 60;
    showDebugData = true;
    weaponSelection = 1;
    iAmDied = 1;
    primaryWeaponFired = true;
    soundEnabled = true;
    soundEnabled =  Utils::toBoolean(pDataMgr->getSystemSetting( "soundon" ));
    // gratuitous comment
    if( ! pPlayerObject )
        cout << "Predicted Problem" << endl;
    pPlayerObject->setSelectedWeapon( 1 );
    pPlayerObject->setAmmo( 100 );
    pPlayerObject->setClip( pPlayerObject->MAX_CLIP_RIFLE );
    pPlayerObject->setSelectedWeapon( 2 );
    pPlayerObject->setAmmo( 500 );
    pPlayerObject->setClip( pPlayerObject->MAX_CLIP_PISTOL );
    pPlayerObject->setSelectedWeapon( 3 );
    pPlayerObject->setAmmo( 600 );
    pPlayerObject->setClip( pPlayerObject->MAX_CLIP_GATLING );
    pPlayerObject->setSelectedWeapon( 4 );
    pPlayerObject->setAmmo( 50 );
    pPlayerObject->setClip( pPlayerObject->MAX_CLIP_BLUNDERBUSS );
    pPlayerObject->setSelectedWeapon( 1 );

    imperialKills = 0;
    rebelKills = 0;
    imperialPlayers = 0;
    rebelPlayers = 0;
    serverCount = 0;

    jumpDelay = false;
    clientDiedOnce = false;
    clientsNeedUpdate = false;
    clientScoreUpdate = false;

    pPlayerObject->health = 100;
    pPlayerObject->dropFlag();
    conNum = 0;
    gameStarted = false;
    gameButton = false;
    timeDelay = 0;
    reloadTime = 0;
    startRespawn = 90000000;
    // death timer
    theBell = gameTime + 650;
    enemyHit = false;
    cameraSize = core::vector3df( 10.0f, 60.0f, 10.0f );
    started = false;
    cout << "LvlDerelict initData() success" << endl;
    billName = NULL;
    disconnect = false;
    return true;
}

bool LvlDerelict::initEntities()
{
    cout << "initEntites() begun" << endl;
    //{ HealthDrop Initialisation
    healthDrop.setPosition( core::vector3df( 2204, 550, -267 ) );
    healthDrop.setRotation( core::vector3df( 0, 0, 0 ) );
    healthDrop.setType( 'b' ); // 'b' for "body".
    healthDrop.setSubtype( 'h' ); // 'h' for "health".
    healthDrop.setValue( 50 ); // an ammo drop the size of two weapon-specific maximum clip-sizes.
    healthMesh = pSceneMgr->addMeshSceneNode( pSceneMgr->getMesh( "media/meshes/health.3ds" ), 0, 20, healthDrop.getPosition(), healthDrop.getRotation() );
    healthMesh->setMaterialTexture( 0, pVideo->getTexture( "media/textures/healthSurface_Color.png" ) );
    healthMesh->setMaterialFlag( video::EMF_LIGHTING, false );
    healthMesh->setMaterialType( video::EMT_SOLID );
    healthDrop.setEntityMesh( healthMesh );
    entityArray[1] = healthDrop;
    healthDrop.setPosition( core::vector3df( -2131, 550, -267 ) );
    healthMesh2 = pSceneMgr->addMeshSceneNode( pSceneMgr->getMesh( "media/meshes/health.3ds" ), 0, 20, healthDrop.getPosition(), healthDrop.getRotation() );
    healthMesh2->setMaterialTexture( 0, pVideo->getTexture( "media/textures/healthSurface_Color.png" ) );
    healthMesh2->setMaterialFlag( video::EMF_LIGHTING, false );
    healthMesh2->setMaterialType( video::EMT_SOLID );
    healthDrop.setEntityMesh( healthMesh2 );
    entityArray[2] = healthDrop;
    healthDrop.setPosition( core::vector3df( 0, 0, 0 ) );
    healthMesh3 = pSceneMgr->addMeshSceneNode( pSceneMgr->getMesh( "media/meshes/health.3ds" ), 0, 20, healthDrop.getPosition(), healthDrop.getRotation() );
    healthMesh3->setMaterialTexture( 0, pVideo->getTexture( "media/textures/healthSurface_Color.png" ) );
    healthMesh3->setMaterialFlag( video::EMF_LIGHTING, false );
    healthMesh3->setMaterialType( video::EMT_SOLID );
    healthDrop.setEntityMesh( healthMesh3 );
    entityArray[3] = healthDrop;
    //}
    //{ RifleAmmoDrop Initialisation
    rifleAmmoDrop.setPosition( core::vector3df( -920, 200, 211 ) );
    rifleAmmoDrop.setRotation( core::vector3df( 0, 0, 0 ) );
    rifleAmmoDrop.setType( 'a' ); // 'a' for "ammo".
    rifleAmmoDrop.setSubtype( 'r' ); // 'r' for "rifle".
    rifleAmmoDrop.setValue( pPlayerObject->MAX_CLIP_RIFLE * 2 ); // an ammo drop the size of two weapon-specific maximum clip-sizes.
    rifleAmmoMesh = pSceneMgr->addMeshSceneNode( pSceneMgr->getMesh( pPlayerObject->getWeaponMesh( 1 ) ), 0, 20, rifleAmmoDrop.getPosition(), rifleAmmoDrop.getRotation() );
    rifleAmmoMesh->setMaterialTexture( 0, pVideo->getTexture( pPlayerObject->getWeaponTexture( 1 ) ) );
    rifleAmmoMesh->setMaterialFlag( video::EMF_LIGHTING, false );
    rifleAmmoMesh->setMaterialType( video::EMT_SOLID );
    rifleAmmoDrop.setEntityMesh( rifleAmmoMesh );
    entityArray[50] = rifleAmmoDrop;
    //}
    //{ PistolAmmoDrop Initialisation
    pistolAmmoDrop.setPosition( core::vector3df( -5, 550, 374 ) );
    pistolAmmoDrop.setRotation( core::vector3df( 0, 0, 0 ) );
    pistolAmmoDrop.setType( 'a' ); // 'a' for "ammo".
    pistolAmmoDrop.setSubtype( 'p' ); // 'p' for "pistol".
    pistolAmmoDrop.setValue( pPlayerObject->MAX_CLIP_PISTOL * 2 ); // an ammo drop the size of two weapon-specific maximum clip-sizes.
    pistolAmmoMesh = pSceneMgr->addMeshSceneNode( pSceneMgr->getMesh( pPlayerObject->getWeaponMesh( 2 ) ), 0, 20, pistolAmmoDrop.getPosition(), pistolAmmoDrop.getRotation() );
    pistolAmmoMesh->setMaterialTexture( 0, pVideo->getTexture( pPlayerObject->getWeaponTexture( 2 ) ) );
    pistolAmmoMesh->setMaterialFlag( video::EMF_LIGHTING, false );
    pistolAmmoMesh->setMaterialType( video::EMT_SOLID );
    pistolAmmoDrop.setEntityMesh( pistolAmmoMesh );
    entityArray[100] = pistolAmmoDrop;
    //}
    //{ GattlingAmmoDrop Initialisation
    gattlingAmmoDrop.setPosition( core::vector3df( 75, 550, -590 ) );
    gattlingAmmoDrop.setRotation( core::vector3df( 0, 0, 0 ) );
    gattlingAmmoDrop.setType( 'a' ); // 'a' for "ammo".
    gattlingAmmoDrop.setSubtype( 'g' ); // 'g' for "gattling".
    gattlingAmmoDrop.setValue( pPlayerObject->MAX_CLIP_GATLING * 2 ); // an ammo drop the size of two weapon-specific maximum clip-sizes.
    gattlingAmmoMesh = pSceneMgr->addMeshSceneNode( pSceneMgr->getMesh( pPlayerObject->getWeaponMesh( 3 ) ), 0, 20, gattlingAmmoDrop.getPosition(), gattlingAmmoDrop.getRotation() );
    gattlingAmmoMesh->setMaterialTexture( 0, pVideo->getTexture( pPlayerObject->getWeaponTexture( 3 ) ) );
    gattlingAmmoMesh->setMaterialFlag( video::EMF_LIGHTING, false );
    gattlingAmmoMesh->setMaterialType( video::EMT_SOLID );
    gattlingAmmoDrop.setEntityMesh( gattlingAmmoMesh );
    entityArray[150] = gattlingAmmoDrop;
    //}
    //{ BlunderbussAmmoDrop Initialisation
    blunderbussAmmoDrop.setPosition( core::vector3df( 1091, 200, 148 ) );
    blunderbussAmmoDrop.setRotation( core::vector3df( 0, 0, 0 ) );
    blunderbussAmmoDrop.setType( 'a' ); // 'a' for "ammo".
    blunderbussAmmoDrop.setSubtype( 'b' ); // 'b' for "blunderbuss".
    blunderbussAmmoDrop.setValue( pPlayerObject->MAX_CLIP_BLUNDERBUSS * 2 ); // an ammo drop the size of two weapon-specific maximum clip-sizes.
    blunderbussAmmoMesh = pSceneMgr->addMeshSceneNode( pSceneMgr->getMesh( pPlayerObject->getWeaponMesh( 4 ) ), 0, 20, blunderbussAmmoDrop.getPosition(), blunderbussAmmoDrop.getRotation() );
    blunderbussAmmoMesh->setMaterialTexture( 0, pVideo->getTexture( pPlayerObject->getWeaponTexture( 4 ) ) );
    blunderbussAmmoMesh->setMaterialFlag( video::EMF_LIGHTING, false );
    blunderbussAmmoMesh->setMaterialType( video::EMT_SOLID );
    blunderbussAmmoDrop.setEntityMesh( blunderbussAmmoMesh );
    entityArray[200] = blunderbussAmmoDrop;
    //}
    //{ ObjectiveFlag Initialisation
    objectiveFlagDrop.setPosition( core::vector3df( 2835, 406, 117 ) );
    objectiveFlagDrop.setRotation( core::vector3df( 0, 0, 0 ) );
    objectiveFlagDrop.setType( 'f' ); // 'f' for "flag".
    objectiveFlagDrop.setSubtype( 'i' ); // 'i' for "imperial".
    objectiveFlagDrop.setValue( 5 ); // a 'true' value expressed as an int.
    imperialFlagMesh = pSceneMgr->addMeshSceneNode( pSceneMgr->getMesh( "media/exportedFlags/skylordsFlag.3ds" ), 0, 20, objectiveFlagDrop.getPosition(), objectiveFlagDrop.getRotation() );
    imperialFlagMesh->setMaterialTexture( 0, pVideo->getTexture( "media/exportedFlags/skylordSurface_Color.png" ) );
    imperialFlagMesh->setMaterialFlag( video::EMF_LIGHTING, false );
    imperialFlagMesh->setMaterialType( video::EMT_SOLID );
    objectiveFlagDrop.setEntityMesh( imperialFlagMesh );
    entityArray[250] = objectiveFlagDrop;
    objectiveFlagDrop.setPosition( core::vector3df( -2735, 406, 117 ) );
    objectiveFlagDrop.setSubtype( 'r' ); // 'r' for "rebel".
    rebelFlagMesh = pSceneMgr->addMeshSceneNode( pSceneMgr->getMesh( "media/exportedFlags/jorgan'sFlag.3ds" ), 0, 20, objectiveFlagDrop.getPosition(), objectiveFlagDrop.getRotation() );
    rebelFlagMesh->setMaterialTexture( 0, pVideo->getTexture( "media/exportedFlags/jorganSurface_Color.png" ) );
    rebelFlagMesh->setMaterialFlag( video::EMF_LIGHTING, false );
    rebelFlagMesh->setMaterialType( video::EMT_SOLID );
    objectiveFlagDrop.setEntityMesh( rebelFlagMesh );
    entityArray[251] = objectiveFlagDrop;

    //}
    return true;
}


bool LvlDerelict::initCameras()
{
    cout << "initCameras() begun" << endl;
    SKeyMap keyMap[10];
    keyMap[0].Action = EKA_MOVE_FORWARD;
    keyMap[0].KeyCode = KEY_UP;
    keyMap[1].Action = EKA_MOVE_FORWARD;
    keyMap[1].KeyCode = KEY_KEY_W;

    keyMap[2].Action = EKA_MOVE_BACKWARD;
    keyMap[2].KeyCode = KEY_DOWN;
    keyMap[3].Action = EKA_MOVE_BACKWARD;
    keyMap[3].KeyCode = KEY_KEY_S;

    keyMap[4].Action = EKA_STRAFE_LEFT;
    keyMap[4].KeyCode = KEY_LEFT;
    keyMap[5].Action = EKA_STRAFE_LEFT;
    keyMap[5].KeyCode = KEY_KEY_A;

    keyMap[6].Action = EKA_STRAFE_RIGHT;
    keyMap[6].KeyCode = KEY_RIGHT;
    keyMap[7].Action = EKA_STRAFE_RIGHT;
    keyMap[7].KeyCode = KEY_KEY_D;

    keyMap[8].Action = EKA_JUMP_UP;
    keyMap[8].KeyCode = KEY_SPACE;

    keyMap[9].Action = EKA_CROUCH;
    keyMap[9].KeyCode = KEY_CONTROL;
    // creates camera
    pCamera = pSceneMgr->addCameraSceneNodeFPS(
                  0,          // parent
                  100.0f,     // rotate speed
                  0.35f,       // move speed
                  0,         // node ID
                  keyMap,     // key array
                  10,          // array size
                  true,       // no vertical
                  3.1f        // jump speed
              );

    if ( pCamera == NULL )
        throw "addCameraSceneNodeFPS() failed";
    // set position of camera and the target
    if( pPlayerObject->getTeam() )
        pCamera->setPosition( core::vector3df( 2717, 1095, 10 ) );
    else
        pCamera->setPosition( core::vector3df( -2679, 1095, -22 ) );
    //  -3528.0f, 750.0f, -3.0f
    pPlayerObject->setPosition( pCamera->getAbsolutePosition() );
    pCamera->setTarget( core::vector3df( -201, 784, 109 ) );    // 33.0f, 615.0f, 55.0
    pCamera->bindTargetAndRotation( true );
    pPlayerObject->setRotation( pCamera->getRotation() );
    pCamera->setFarValue( 40000.0f );
    pCamera->setNearValue( 0.01f );

//    playerMesh = pSceneMgr->addMeshSceneNode(pSceneMgr->getMesh("media/meshes/staticcharactermesh.obj"), 0, 20, core::vector3df(0,-35,-10));
//    pCamera->addChild(playerMesh);
    cout << "LvlDerelict initCameras() success" << endl;
    return true;
}

bool LvlDerelict::initGuns( int selected )
{
    cout << "initGuns() begun" << endl;
    // --IMPORTANT--
    // The position vector changes the direction offput of the weapon @@in relation to the direction the player is facing@@ thusly:
    // (  right-left  ,  up-down  ,  forward-back  )

    // This is true for at least the Gattling... If someone finds differently for some reason, make a note of it.

    if( ( gun2 != NULL ) )
    {
        gun2->remove();
        gun2 = NULL;
    }
    switch( selected )
    {
    case 1:
        gun = pSceneMgr->addAnimatedMeshSceneNode( pSceneMgr->getMesh( pPlayerObject->getWeaponMesh( 1 ) ),
                0,
                10,
                core::vector3df( 10, -10, 50 ),  // location in the world pCamera->getPosition() +  core::vector3df(250,-100,25)     (-10,-275,-16)
                core::vector3df( 0, 0, 0 ),    // rotation
                core::vector3df( 1, 1, 1 )    // scale x, y and z
                                                 );

        gun->setMaterialTexture( 0, pVideo->getTexture( "media/textures/rifleTexture.png" ) );
        gun->setMaterialTexture( 1, pVideo->getTexture( "media/textures/rifleAO.png" ) );
        pCamera->addChild( gun );
        pPlayerObject->setSelectedWeapon( 1 );

        gun->setFrameLoop( 180, 180 );
        break;
    case 2:
        gun = pSceneMgr->addAnimatedMeshSceneNode( pSceneMgr->getMesh( pPlayerObject->getWeaponMesh( 2 ) ),
                0,
                11,
                core::vector3df( 10, -10, 40 ),  // location in the world pCamera->getPosition() +  core::vector3df(250,-100,25)     (-10,-275,-16)
                core::vector3df( 0, 0, 0 ),    // rotation
                core::vector3df( 1, 1, 1 )    // scale x, y and z   --- Needs to be stupidly small to scale right.
                                                 );
        gun->setMaterialTexture( 0, pVideo->getTexture( "media/textures/pistolTexture.png" ) );
        gun->setMaterialTexture( 1, pVideo->getTexture( "media/textures/pistolAO.png" ) );

        gun2 = pSceneMgr->addAnimatedMeshSceneNode( pSceneMgr->getMesh( pPlayerObject->getWeaponMesh( 2 ) ),
                0,
                12,
                core::vector3df( -10, -10, 40 ),  // location in the world pCamera->getPosition() +  core::vector3df(250,-100,25)     (-10,-275,-16)
                core::vector3df( 0, 0, 0 ),    // rotation
                core::vector3df( 1, 1, 1 )    // scale x, y and z   --- Needs to be stupidly small to scale right.
                                                  );
        gun2->setMaterialTexture( 0, pVideo->getTexture( "media/textures/pistolTexture.png" ) );
        gun2->setMaterialTexture( 1, pVideo->getTexture( "media/textures/pistolAO.png" ) );

        pCamera->addChild( gun );
        pCamera->addChild( gun2 );
        pPlayerObject->setSelectedWeapon( 2 );


        gun->setFrameLoop( 200, 200 );
        gun2->setFrameLoop( 200, 200 );
        break;
    case 3:

        gun = pSceneMgr->addAnimatedMeshSceneNode( pSceneMgr->getMesh( pPlayerObject->getWeaponMesh( 3 ) ),
                0,
                13,
                core::vector3df( 30, -25, 0 ),  // location in the world pCamera->getPosition() +  core::vector3df(250,-100,25)     (-10,-275,-16)
                core::vector3df( 0, 0, 0 ),    // rotation
                core::vector3df( 1, 1, 1 )    // scale x, y and z   --- Needs to be stupidly small to scale right.

                                                 );
        gun->setMaterialTexture( 0, pVideo->getTexture( "media/textures/gattlingTexture.png" ) );
        gun->setMaterialTexture( 1, pVideo->getTexture( "media/textures/gattlingAO.png" ) );
        pCamera->addChild( gun );
        pPlayerObject->setSelectedWeapon( 3 );


        gun->setFrameLoop( 210, 210 );
        break;
    case 4:

        gun = pSceneMgr->addAnimatedMeshSceneNode( pSceneMgr->getMesh( pPlayerObject->getWeaponMesh( 4 ) ),
                0,
                14,
                core::vector3df( 10, -10, 35 ),  // location in the world pCamera->getPosition() +  core::vector3df(250,-100,25)     (-10,-275,-16)
                core::vector3df( 0, 0, 0 ),    // rotation
                core::vector3df( 1, 1, 1 )    // scale x, y and z   --- Needs to be stupidly small to scale right.
                                                 );
        gun->setMaterialTexture( 0, pVideo->getTexture( "media/textures/blunderbussTexture.png" ) );
        gun->setMaterialTexture( 1, pVideo->getTexture( "media/textures/blunderbussAO.png" ) );
        pCamera->addChild( gun );
        pPlayerObject->setSelectedWeapon( 4 );

        gun->setFrameLoop( 180, 180 );
        break;

    default:
        break;
    }

//    gun = pSceneMgr->addMeshSceneNode(pSceneMgr->getMesh("media/meshes/Gunv1.3ds"),
//                                      pCamera,
//                                      -1,
//                                      core::vector3df(8,-5,30),        // location in the world pCamera->getPosition() +  core::vector3df(250,-100,25)     (-10,-275,-16)
//                                      core::vector3df(0,180,0),        // rotation
//                                      core::vector3df( 1.0f, 1.0f, 1.0f )      // scale x, y and z
//                                     );

//    pCamera->addChild(gun);
//    gun->addChild(pCamera);

    gun->setMaterialFlag( video::EMF_BILINEAR_FILTER, true );
    gun->setMaterialFlag( video::EMF_TRILINEAR_FILTER, true );
    gun->setMaterialFlag( video::EMF_ANISOTROPIC_FILTER, true );
    gun->setMaterialFlag( video::EMF_ANTI_ALIASING, true );
    gun->setMaterialFlag( video::EMF_BACK_FACE_CULLING, true );
    gun->setMaterialFlag( video::EMF_FRONT_FACE_CULLING, false );
    gun->setMaterialFlag( video::EMF_GOURAUD_SHADING, true );
    gun->setMaterialFlag( video::EMF_NORMALIZE_NORMALS, true );
    if( pVideo->getDriverType() == EDT_DIRECT3D8 || pVideo->getDriverType() == EDT_DIRECT3D9 )
    {
        gun->setMaterialType( video::EMT_SOLID );
        gun->setMaterialFlag( video::EMF_LIGHTING, false );
    }
    else
    {
        gun->setMaterialType( video::EMT_LIGHTMAP_LIGHTING );
        gun->setMaterialFlag( video::EMF_LIGHTING, true );
    }

    if( gun2 )
    {
        gun2->setMaterialFlag( video::EMF_BILINEAR_FILTER, true );
        gun2->setMaterialFlag( video::EMF_TRILINEAR_FILTER, true );
        gun2->setMaterialFlag( video::EMF_ANISOTROPIC_FILTER, true );
        gun2->setMaterialFlag( video::EMF_ANTI_ALIASING, true );
        gun2->setMaterialFlag( video::EMF_BACK_FACE_CULLING, true );
        gun2->setMaterialFlag( video::EMF_FRONT_FACE_CULLING, false );
        gun2->setMaterialFlag( video::EMF_GOURAUD_SHADING, true );
        gun2->setMaterialFlag( video::EMF_NORMALIZE_NORMALS, true );
        if( pVideo->getDriverType() == EDT_DIRECT3D8 || pVideo->getDriverType() == EDT_DIRECT3D9 )
        {
            gun2->setMaterialType( video::EMT_SOLID );
            gun2->setMaterialFlag( video::EMF_LIGHTING, false );
        }
        else
        {
            gun2->setMaterialType( video::EMT_LIGHTMAP_LIGHTING );
            gun2->setMaterialFlag( video::EMF_LIGHTING, true );
        }
    }

    if( gun )
    {
        cout << "addGun() SUCCESS" << endl;
    }
    else
    {
        cout << "addGun() failed" << endl;
    }

    return true;
}

bool LvlDerelict::initWorld()
{
    cout << "LvlDerelict::initWorld() begun" << endl;

    pSceneMgr->addLightSceneNode( 0, core::vector3df( -5000.0f, -10000.0f, -5000.0f ), video::SColorf( 255.0f, 255.0f, 255.0f ), 10000.0f, 1 );
    pSceneMgr->addLightSceneNode( 0, core::vector3df( 5000.0f, 10000.0f, 5000.0f ), video::SColorf( 255.0f, 255.0f, 255.0f ), 10000.0f, 1 );
//    pSceneMgr->addLightSceneNode( 0, core::vector3df( 2500.0f, 5000.0f, 2500.0f ), video::SColorf( 255.0f, 255.0f, 255.0f ), 5000.0f, 1 );

    // Load the world meshes and textures
    pSceneMgr->loadScene( "media/levels/derelict/level.irr" );

    /* Initialise the player mesh
     * 1. Add the player mesh as an AnimatedMeshSceneNode.
     * 2. Set the the player mesh to play an idle animation between two specified frames.
     * 3. Normal/Parallax maps cannot be used on animated meshes.
     *    Use a Lightmap material type instead, so that the mesh is still affected by lights.
     * 4. Set the material flags of the mesh.
     * 5. Apply textures to the mesh's material. First texture is the colour channel. Second texture is the lightmap channel (with baked AO).
     * 6. Add the mesh to the player mesh array.
     */
    friendMesh = pSceneMgr->addAnimatedMeshSceneNode( pSceneMgr->getMesh( "media/meshes/reconfiguredAnimations/playerCharacter.b3d" ), 0, 21, core::vector3df( 0, 0, 0 ), core::vector3df( 0, 180, 0 ) );
    friendMesh->setFrameLoop( 95, 134 );
    friendMesh->animateJoints();
    friendMesh->setMaterialFlag( video::EMF_BILINEAR_FILTER, true );
    friendMesh->setMaterialFlag( video::EMF_TRILINEAR_FILTER, true );
    friendMesh->setMaterialFlag( video::EMF_ANISOTROPIC_FILTER, true );
    friendMesh->setMaterialFlag( video::EMF_ANTI_ALIASING, true );
    friendMesh->setMaterialFlag( video::EMF_BACK_FACE_CULLING, true );
    friendMesh->setMaterialFlag( video::EMF_FRONT_FACE_CULLING, false );
    friendMesh->setMaterialFlag( video::EMF_GOURAUD_SHADING, true );
    friendMesh->setMaterialFlag( video::EMF_NORMALIZE_NORMALS, true );




    // Lightmaps with Dynamic Lighting do not work with the Direct3D shader API in Irrlicht.
    // Disable dynamic lighting and use a solid material type if Direct3D is in use.
    if( pVideo->getDriverType() == EDT_DIRECT3D8 || pVideo->getDriverType() == EDT_DIRECT3D9 )
    {
        friendMesh->setMaterialType( video::EMT_SOLID );
        friendMesh->setMaterialFlag( video::EMF_LIGHTING, false );
        friendMesh->setMaterialTexture( 0, pVideo->addTexture( "media/textures/SCM_ColorAO.jpg", pVideo->createImageFromFile( "media/textures/SCM_ColorAO.jpg" ) ) );
    }
    else
    {
        friendMesh->setMaterialType( video::EMT_LIGHTMAP_LIGHTING );
        friendMesh->setMaterialFlag( video::EMF_LIGHTING, true );
        friendMesh->setMaterialTexture( 0, pVideo->addTexture( "media/textures/SCM_Color.jpg", pVideo->createImageFromFile( "media/textures/SCM_Color.jpg" ) ) );
        friendMesh->setMaterialTexture( 1, pVideo->addTexture( "media/textures/SCM_Ambient_Occlusion.jpg", pVideo->createImageFromFile( "media/textures/SCM_Ambient_Occlusion.jpg" ) ) );
    }


    pSceneMgr->addSkyDomeSceneNode( pVideo->getTexture( "media/textures/sky/skyDome5.png" ), 16, 8, 1.0, 2.0, 1000.0f, 0, 10 );

    // Add a cloud scene node and assign it to a layer
    for (  int index = 0; index < MAX_CLOUD_LAYERS; index++ )
    {
        cloudLayer[ index ] = new scene::CCloudSceneNode( pSceneMgr->getRootSceneNode(), pSceneMgr, 0 );
    }

    // Apply different translation values to each layer
    cloudLayer[0]->setTranslation( core::vector2d<f32>( 0.005f, 0.002f ) );
    cloudLayer[1]->setTranslation( core::vector2d<f32>( 0.003f, 0.003f ) );
    cloudLayer[2]->setTranslation( core::vector2d<f32>( 0.007f, 0.001f ) );

    // Assign different textures to each layer
    cloudLayer[0]->getMaterial( 0 ).setTexture( 0, pVideo->getTexture( "media/clouds/cloud01.png" ) );
    cloudLayer[1]->getMaterial( 0 ).setTexture( 0, pVideo->getTexture( "media/clouds/cloud02.png" ) );
    cloudLayer[2]->getMaterial( 0 ).setTexture( 0, pVideo->getTexture( "media/clouds/cloud03.png" ) );

    // Apply different heights to each layer
    cloudLayer[0]->setCloudHeight( 0.5f, 0.1f, -0.05f );
    cloudLayer[1]->setCloudHeight( 0.4f, 0.05f, -0.1f );
    cloudLayer[2]->setCloudHeight( 0.35f, 0.0f, -0.15f );

    // Modify the texture scale
    cloudLayer[1]->setTextureScale( 0.5f );
    cloudLayer[2]->setTextureScale( 0.1f );

    // add fog, the terrain is fog enabled (can use dark for distance)
    pVideo->setFog(
        video::SColor( 0, 0xd0, 0xd0, 0xd0 ), // colour - light grey
        video::EFT_FOG_LINEAR,              // type
        4000,                               // linear start of fog
        12000,                              // linear end
        0.01,                               // density
        false,                              // vertex fog
        true );                               // range based vertex fog - if supported


//    playerMesh->setVisible(false);
    cout << "LvlDerelict initWorld() success" << endl;
    return true;
}

bool LvlDerelict::initCollisionDetection()
{
    cout << "initCollisionDetection() begun" << endl;
    // Create a meta triangle selector to hold several triangle selectors.
    scene::IMetaTriangleSelector * meta = pSceneMgr->createMetaTriangleSelector();

    worldCollisionSelector = pSceneMgr->createMetaTriangleSelector();
    enemyCharacterSelector = pSceneMgr->createMetaTriangleSelector();

    /*
    	Now we will find all the nodes in the scene and create triangle
    	selectors for all suitable nodes.  Typically, you would want to make a
    	more informed decision about which nodes to performs collision checks
    	on; you could capture that information in the node name or Id.
    	*/
    core::array<scene::ISceneNode *> nodes;
    // Find all nodes
    pSceneMgr->getSceneNodesFromType( scene::ESNT_ANY, nodes );

    for ( u32 i = 0; i < nodes.size(); ++i )
    {
        scene::ISceneNode * node = nodes[i];
        scene::ITriangleSelector * selector = 0;

        switch( node->getType() )
        {
        case scene::ESNT_CUBE:
        case scene::ESNT_ANIMATED_MESH:
            // Because the selector won't animate with the mesh,
            // and is only being used for camera collision, we'll just use an approximate
            // bounding box instead of ((scene::IAnimatedMeshSceneNode*)node)->getMesh(0)
//                selector = pSceneMgr->createTriangleSelectorFromBoundingBox(node);
            if( node->getID() != 20 )
                selector = pSceneMgr->createTriangleSelector( ( ( scene::IAnimatedMeshSceneNode* )node )->getMesh(), node );
            if( node->getID() == friendMesh->getID() )
            {
                selector = pSceneMgr->createTriangleSelector( ( ( scene::IAnimatedMeshSceneNode* )node )->getMesh(), node );

                enemyCharacterSelector->addTriangleSelector( selector );
                cout << "initCollisionDetection() found enemy *** " << node->getID() << endl;
            }
            if( node->getID() == 2 )
            {
//                target = ( scene::IAnimatedMeshSceneNode* )node;
//                target->setAnimationSpeed( 12 );
                cout << "initCollisionDetection() found mesh *** " << node->getID() << endl;
            }
            break;

        case scene::ESNT_SPHERE: // Derived from IMeshSceneNode
            selector = pSceneMgr->createTriangleSelector( ( ( scene::IMeshSceneNode* )node )->getMesh(), node );
            if( node->getID() == 4 )
            {
                spheresArray.push_back( selector );
                cout << "initCollisionDetection() found sphere *** " << targetCount << endl;
                ++targetCount;
            }
            break;
        case scene::ESNT_MESH:
            if ( node->getID() == 20 )
            {
                // do nothing
            }
            else
            {
                if( node->getID() == 1 )
                {
                    selector = pSceneMgr->createTriangleSelector( ( ( scene::IMeshSceneNode* )node )->getMesh(), node );

                    worldCollisionSelector->addTriangleSelector( selector );
                    cout << "initCollisionDetection() found building *** " << node->getID() << endl;
                }
                if( node->getID() == friendMesh->getID() )
                {
                    selector = pSceneMgr->createTriangleSelector( ( ( scene::IMeshSceneNode* )node )->getMesh(), node );

                    enemyCharacterSelector->addTriangleSelector( selector );
                    cout << "initCollisionDetection() found enemy *** " << node->getID() << endl;
                }
                else
                {
                    selector = pSceneMgr->createTriangleSelector( ( ( scene::IMeshSceneNode* )node )->getMesh(), node );
                }

            }
            break;

        case scene::ESNT_TERRAIN:
            selector = pSceneMgr->createTerrainTriangleSelector( ( scene::ITerrainSceneNode* )node );
            break;

        case scene::ESNT_OCTREE:
            selector = pSceneMgr->createOctreeTriangleSelector( ( ( scene::IMeshSceneNode* )node )->getMesh(), node );
            break;

        default:
            // Don't create a selector for this node type
            break;
        }
        // if it's a part of the static scenery
        if( selector )
        {
            // Add it to the meta selector, which will take a reference to it
            meta->addTriangleSelector( selector );
            // And drop the reference to it, so that the meta selector owns it.
            selector->drop();
        }
    }

    lvlOne = dynamic_cast<scene::IMeshSceneNode*>( worldCollisionSelector );

    /*
    Now that the mesh scene nodes have had triangle selectors created and added
    to the meta selector, create a collision response animator from that meta selector.
    */
    scene::ISceneNodeAnimator* anim = pSceneMgr->createCollisionResponseAnimator(
                                          meta, pCamera, cameraSize,                // ellipsoid Radius default vector3df(30, 60, 30)
                                          core::vector3df( 0.0f, -9.0f, 0.0f ),                               // gravityPerSecond default vector3df(0,-100.0f, 0)
                                          core::vector3df( 0.0f, 30.0f, 0.0f ) );                              // ellipsoid Translation default vector3df(0, 0, 0)
    meta->drop(); // I'm done with the meta selector now

    pCamera->addAnimator( anim );
    anim->drop(); // I'm done with the animator now

    cout << "LvlDerelict initCollisionDetection() success" << endl;
    return true;
}

bool LvlDerelict::initGUI()
{
    cout << "initGUI() begun" << endl;
    gui::IGUIFont* font = pGUIEnv->getFont( "media/fonts/ConsoleFont.bmp" );
    if ( font == 0 )
        throw "getFont() failed.";
    gui::IGUISkin* newskin = pGUIEnv->createSkin( gui::EGST_WINDOWS_CLASSIC );
    newskin->setFont( font );
    pGUIEnv->setSkin( newskin );
    newskin->drop();
    unsigned y = pGameMgr->getScreenResolution().Height - 64;
    unsigned x = pGameMgr->getScreenResolution().Width / 2;
    lblDebugData = pGUIEnv->addStaticText(
                       L"",
                       core::rect<s32>( x - 256, y, x + 256, y + 64 ),
                       true,
                       true,
                       0,
                       -1,
                       true );
    if ( lblDebugData == 0 )
        throw "getFont() failed.";
    lblDebugData->setOverrideColor( video::SColor( 255, 0, 255, 255 )  );
    if(showDebugData == false)
        lblDebugData->setVisible(false);
    // this happens for both debug and release
    // readXMLSettings("data/DeviceOptions.xml");

    // PauseGUI Overlay
    // position the buttons to the centre/middle of the GUI screen...
    //

    // DO NOT PUT THIS HERE, THIS IS A TERRIBLE IDEA
    // TERRIBLE TERRIBLE IDEA
    // TERRIBLE

//    int leftX = ( pGameMgr->getScreenResolution().Width / 2 ) - 50 , topY = pGameMgr->getScreenResolution().Height / 3 , width = 100, height = 32;
//    // OK and Cancel
//    cmdResume = pGUIEnv->addButton(
//                    core::rect<s32>( leftX, topY, leftX + width, topY + height ),   // location ans size
//                    0,                                                              // parent
//                    GUI_CMD_RESUME,
//                    L"Resume",
//                    L"Click to Resume play" );
//
//
//    topY = topY + 50;
//
//    cmdOptions = pGUIEnv->addButton(
//                     core::rect<s32>( leftX, topY, leftX + width, topY + height ),   // location ans size
//                     0,                                                              // parent
//                     GUI_CMD_OPTIONS,
//                     L"Options",
//                     L"Click to make changes to the game settings." );
//
//
//    topY = topY + 50;
//
//
//    cmdExitGame = pGUIEnv->addButton(
//                      core::rect<s32>( leftX, topY, leftX + width, topY + height ),   // location ans size
//                      0,                                                              // parent
//                      GUI_CMD_EXITGAME,
//                      L"Exit Game",
//                      L"Click to Exit the game." );
//
//
//    topY = topY + 50;
//
//
//    cmdExitWindows = pGUIEnv->addButton(
//                         core::rect<s32>( leftX, topY, leftX + width, topY + height ),   // location ans size
//                         0,                                                              // parent
//                         GUI_CMD_EXITWINDOWS,
//                         L"Exit Windows",
//                         L"Click to Exit the game." );
//
//    topY = topY;


// BAD

    //! load background image - it is drawn in the update function

    iguiwindowimage  = pVideo->getTexture( "media/images/options.png" );

    cout << "LvlDerelict initGUI() success" << endl;

    //! set up some common variables for the gui components
    u32 scrnWidth = pGameMgr->getScreenResolution().Width;
    u32 scrnHeight = pGameMgr->getScreenResolution().Height;
    // object start top, width and height
    signed topY = scrnHeight / 3, width = 199, height = 50;
    // centred on the screen
    signed leftX = ( scrnWidth - width ) / 2;
    // all components req. a dimension this is first used on the title
    core::rect<s32> sizer( leftX, topY, leftX + width, topY + height );
    // set up images for "button" images 199x50
    u32 incrY = scrnHeight / 12;
    u32 incrX = ( scrnWidth / 2 ) - ( width / 2 );
    leftX = incrX;
    width = 199;
    height = 50;
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );

    //   Reume button(s)
    btnResume = pGUIEnv->addButton( sizer, 0, GUI_BTN_RESUME, L"" );
    btnResume->setImage( pVideo->getTexture( "media/textures/InGameButtons/resume.png" ) );
    btnResume->setVisible(false);
    //   Options button
    topY += incrY;
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    btnOptions = pGUIEnv->addButton( sizer, 0, GUI_BTN_OPTIONS, L"" );
    btnOptions->setImage( pVideo->getTexture( "media/textures/InGameButtons/options.png" ) );
    btnOptions->setVisible(false);
    //   Exit Game button
    topY += incrY;
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    btnExitGame = pGUIEnv->addButton( sizer, 0, GUI_BTN_EXIT_GAME, L"" );
    btnExitGame->setImage( pVideo->getTexture( "media/textures/InGameButtons/mainmenu.png" ) );
    btnExitGame->setVisible(false);
    //   Exit Window button
    topY += incrY;
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    btnExitWindow = pGUIEnv->addButton( sizer, 0, GUI_BTN_EXIT_WINDOW, L"" );
    btnExitWindow->setImage( pVideo->getTexture( "media/textures/InGameButtons/quit.png" ) );
    btnExitWindow->setVisible(false);


    /*
        Options Screen
    */

    //   back button
    topY += incrY;
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    btnBack = pGUIEnv->addButton( sizer, 0, GUI_BTN_BACK, L"" );
    btnBack->setImage( pVideo->getTexture( "media/textures/InGameButtons/back.png" ) );
    btnBack->setVisible(false);


    //! sound volume scroll bar
    topY  = ( scrnHeight / 2 );
    leftX = ( scrnWidth / 2 ) / 2;
    width = scrnWidth / 2;
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    scrVolume = pGUIEnv->addScrollBar( true, sizer, 0, GUI_SCR_VOLUME );
    scrVolume->setVisible( false );


//! create play sounds check box
    topY  = ( scrnHeight / 2 );
    leftX = scrnWidth - ( ( scrnWidth / 2 ) / 2 );
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    chkSoundOn = pGUIEnv->addCheckBox( false, sizer, 0,  GUI_CHK_SOUND_ON, L"Sound On" );
    chkSoundOn->setToolTipText( L"Turn sound effects on or off" );
    chkSoundOn->setChecked(soundEnabled);
    chkSoundOn->setVisible( false );

    return true;
}

bool LvlDerelict::initHUD()
{
    cout << "initHUD() begun" << endl;


    // size code
    u32 scrnWidth = pGameMgr->getScreenResolution().Width;
//    u32 scrnHeight = pGameMgr->getScreenResolution().Height;
    // common distance from L/R edges of screen
    signed margin = scrnWidth / 32;
    // starting values for the first component, the title
    // object start top, width and height
    signed topY = 12, width = 320, height = 48;
    // centred on the screen
    signed leftX = ( scrnWidth - width ) / 2;
    // all components req. a dimension this is first used on the title
    core::rect<s32> sizer( leftX, topY, leftX + width, topY + height );

    cout << "frikkin 1" << endl;
    // your IP
    topY = 10;
    width = 160;
    leftX = ( scrnWidth / 2 ) - ( width / 2 );
    height = 40;
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    cout << "frikkin 2" << endl;

//    wchar_t wcstring[256];
//    char cString[256];
////    tempstring =
    std::string convertStr = peer->GetInternalID().ToString();
//    strcpy( cString, convertStr );
//    mbstowcs( wcstring*, tempstring, strlen( tempstring ) + 1 );
    cout << "frikkin 3" << endl;

    const char* temp = convertStr.c_str();
    wchar_t wcstring[256];
    mbstowcs( wcstring, temp, strlen( temp ) + 1 );
    cout << "frikkin 4" << endl;
    lblIP = pGUIEnv->addStaticText( L"", sizer, false, true, 0, -1, false );
    lblIP->setText( wcstring );
    cout << "frikkin 5" << endl;
    //putting colour here
    video::SColor colour;

    // pistol outline
    leftX = ( margin * 12 );
    topY = pGameMgr->getScreenResolution().Height - 40;
    width = 78;
    height = 24;
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    pistolOutline = pGUIEnv->addImage( sizer, 0, -1, 0 );
    pistolOutlineImage = pVideo->getTexture( "media/images/PistolBottomMid.png" );
    pistolOutline->setImage( pistolOutlineImage );
    pistolOutline->setUseAlphaChannel( true );
    //pistolOutline->draw();

    // rifle outline
    leftX = ( margin * 6 );
    topY = pGameMgr->getScreenResolution().Height - 40;
    width = 171;
    height = 24;
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    rifleOutline = pGUIEnv->addImage( sizer, 0, -1, 0 );
    rifleOutlineImage = pVideo->getTexture( "media/images/RifleBottomMid.png" );
    rifleOutline->setImage( rifleOutlineImage );
    rifleOutline->setUseAlphaChannel( true );
    //rifleOutline->draw();

    // gattling outline
    leftX = ( margin * 15 );
    topY = pGameMgr->getScreenResolution().Height - 50;
    width = 169;
    height = 39;
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    gattlingOutline = pGUIEnv->addImage( sizer, 0, -1, 0 );
    gattlingOutlineImage = pVideo->getTexture( "media/images/GattlingBottomMid.png" );
    gattlingOutline->setImage( gattlingOutlineImage );
    gattlingOutline->setUseAlphaChannel( true );
    //gattlingOutline->draw();

    // blunderbuss outline
    leftX = ( margin * 21 );
    topY = pGameMgr->getScreenResolution().Height - 50;
    width = 170;
    height = 40;
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    blunderbussOutline = pGUIEnv->addImage( sizer, 0, -1, 0 );
    blunderbussOutlineImage = pVideo->getTexture( "media/images/BlunderBottomMid.png" );
    blunderbussOutline->setImage( blunderbussOutlineImage );
    blunderbussOutline->setUseAlphaChannel( true );
    //blunderbussOutline->draw();



    // Team insignia
    leftX = margin - 35;
    topY = 10;
    width = 40;
    height = 40;
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    insigniaOne = pGUIEnv->addImage( sizer, 0, -1, 0 );
    insigniaOneImage = pVideo->getTexture( "media/images/imperialnavysmall.png" );
    insigniaOne->setImage( insigniaOneImage );
    insigniaOne->setUseAlphaChannel( true );
    insigniaOne->draw();

    //


    leftX = margin - 35;
    topY = 60;
    width = 40;
    height = 40;
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    insigniaTwo = pGUIEnv->addImage( sizer, 0, -1, 0 );
    insigniaTwoImage = pVideo->getTexture( "media/images/jorganspiratessmall.png" );
    insigniaTwo->setImage( insigniaTwoImage );
    insigniaTwo->setUseAlphaChannel( true );
    insigniaTwo->draw();

    // team kills
    // imperial


    leftX = margin + 15;
    topY = 10;
    width = 60;
    height = 60;
//    char ammoStr[256];
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    tempstring = core::stringw( imperialKills );
//    strcpy(ammoStr,tempstring);
    imperialCount = pGUIEnv->addStaticText( L"", sizer, false, true, 0, -1, false ) ;
    imperialCount->setText( tempstring.c_str() );
    colour.set( 255, 0, 0, 255 );
    imperialCount->setOverrideColor( colour );
    imperialCount->setOverrideFont( pGUIEnv->getFont( "media/fonts/bigfont.png" ) );


    // rebel



    leftX = margin + 15;
    topY = 60;
    width = 60;
    height = 60;
//    char ammoStr[256];
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    tempstring += core::stringw( rebelKills );
//    strcpy(ammoStr,tempstring);
    rebelCount = pGUIEnv->addStaticText( L"", sizer, false, true, 0, -1, false ) ;
    rebelCount->setText( tempstring.c_str() );
    colour.set( 255, 255, 0, 0 );
    rebelCount->setOverrideColor( colour );
    rebelCount->setOverrideFont( pGUIEnv->getFont( "media/fonts/bigfont.png" ) );


    // ammo code
    // set title text and position

    leftX = margin - 30;
    topY = pGameMgr->getScreenResolution().Height - 195;
    width = 183;
    height = 195;
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    ammoCog = pGUIEnv->addImage( sizer, 0, -1, 0 );
    ammoCogImage = pVideo->getTexture( "media/images/ammoBottomLeft.png" );
    ammoCog->setImage( ammoCogImage );
    ammoCog->setUseAlphaChannel( true );
    ammoCog->draw();

    // text, size, border, wordwrap, parent, id, background
    leftX = margin + 40;
    topY = pGameMgr->getScreenResolution().Height - 140;
    width = 90;
    height = 60;
//    char ammoStr[256];
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    tempstring += core::stringw( pPlayerObject->getClip() );
//    strcpy(ammoStr,tempstring);
    ammo = pGUIEnv->addStaticText( L"", sizer, false, true, 0, -1, false );
    ammo->setText( tempstring.c_str() );
    ammo->setOverrideFont( pGUIEnv->getFont( "media/fonts/bigfont.png" ) );

    cout << pPlayerObject->getAmmo() << endl;

    leftX = ( margin * 30 ); //
    topY = 30;
    width = 60;
    height = 60;
//    char ammoStr[256];
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    tempstring += core::stringw( pPlayerObject->getHealth() );
//    strcpy(ammoStr,tempstring);
    health = pGUIEnv->addStaticText( L"", sizer, false, true, 0, -1, false );
    health->setText( tempstring.c_str() );
//    video::SColor colour;
    colour.set( 255, 255, 0, 0 );
    health->setOverrideColor( colour );
    health->setOverrideFont( pGUIEnv->getFont( "media/fonts/bigfont.png" ) );
    cout << pPlayerObject->getHealth() << endl;

    leftX = ( margin * 32 ) - 172;
    topY = 0;
    width = 172;
    height = 167;
    sizer = core::rect<s32>( leftX, topY, leftX + width, topY + height );
    ammo->setOverrideColor( video::SColor( 255, 255, 255, 0 )  );
    gui::IGUIFont* font = pGUIEnv->getFont( "media/fonts/perpetua18.png" );
    gui::IGUISkin* newskin = pGUIEnv->createSkin( gui::EGST_WINDOWS_CLASSIC );
    newskin->setFont( font );
    pGUIEnv->setSkin( newskin );
//    unsigned y = pGameMgr->getScreenResolution().Height - 64;



    healthCog = pGUIEnv->addImage( sizer, 0, -1, 0 );
    // what the f is a video texture?
    // healthCogImage = pVideo->getTexture("media/images/healthTopRight.png");
    // ill come back to this

    healthCog->setImage( healthCogImage );
    // how does I transparency?
    // moved the rest to update



    // first attempt
//    newskin->drawImage( pGUIEnv,
//                            "media/images/HealthTopRight.png",
//                            core::vector2d<s32>( pGameMgr->getScreenResolution().Width, y )
//                            );

    newskin->drop();
//    unsigned y = pGameMgr->getScreenResolution().Height - 64;
//    lblDebugData = pGUIEnv->addStaticText(
//                       L"",
//                       core::rect<s32>( 0, y, 256, y + 64 ),
//                       true,
//                       true,
//                       0,
//                       -2,
//                       true );
//    if ( lblDebugData == 0 )
//        throw "getFont() failed.";
//    lblDebugData->setOverrideColor( video::SColor( 255, 0, 255, 255 )  );
    cout << "LvlDerelict initHUD() success" << endl;
//


    rifleOutline->setVisible( true );
    pistolOutline->setVisible( false );
    gattlingOutline->setVisible( false );
    blunderbussOutline->setVisible( false );

    gunTimer = gameTime + 1000;

    return true;
}

bool LvlDerelict::initSounds()
{
    cout << "initSounds() begun" << endl;
    pGameMgr->getSoundEngine()->removeAllSoundSources();
    lvlOneBgMusic = NULL;
    lvlOneBgMusic = pGameMgr->getSoundEngine()->addSoundSourceFromFile( "media/sounds/sonera.wav" );
    if ( lvlOneBgMusic )
    {
        std::cout << "bgMusic.wav loaded" << std::endl;
    }
    else
    {
        std::cout << "**** Music Not Loaded" << std::endl;
    }

    cout << "Found Combination.wav... " << endl;
    if ( lvlOneBgMusic && soundEnabled)
    {
        pGameMgr->getSoundEngine()->play2D(  lvlOneBgMusic, true); // looped

        std::cout << "Combination.wav Loaded" << std::endl;
//        return true;
    }
    else
    {
        std::cout << "**** Music Not Loaded" << std::endl;
//        return false;
    }

//    lvlOneBgMusic = NULL;
//    lvlOneBgMusic = pGameMgr->getSoundEngine()->addSoundSourceFromFile( "media/sounds/Combination.wav" );
//    if ( lvlOneBgMusic )
//    {
//        std::cout << "bgMusic.wav loaded" << std::endl;
//    }
//    else
//    {
//        std::cout << "**** Music Not Loaded" << std::endl;
//    }

//    lvlTwoBgMusic = NULL;
//    lvlTwoBgMusic = pGameMgr->play3DSound(
//                "");
//    cout << "Found ... " << endl;
//    if ( lvlTwoBgMusic )
//    {
//        lvlTwoBgMusic->setMinDistance( 100.0f );
//        lvlTwoBgMusic->setMaxDistance( 1000.0f );
//        lvlTwoBgMusic->setPlaybackSpeed( 0.25f );
//        lvlTwoBgMusic->setIsPaused( false );
//        std::cout << "" << std::endl;
////        return true;
//    }
//    else
//    {
//        std::cout << "**** Music Not Loaded" << std::endl;
////        return false;
//    }

//    titleBgMusic = NULL;
//    titleBgMusic = pGameMgr->play3DSound(
//                "");
//    cout << "Found ... " << endl;
//    if ( titleBgMusic )
//    {
//        titleBgMusic->setMinDistance( 100.0f );
//        titleBgMusic->setMaxDistance( 1000.0f );
//        titleBgMusic->setPlaybackSpeed( 0.25f );
//        titleBgMusic->setIsPaused( false );
//        std::cout << "" << std::endl;
////        return true;
//    }
//    else
//    {
//        std::cout << "**** Music Not Loaded" << std::endl;
////        return false;
//    }

//    cannonShotSound = NULL;
//    cannonShotSound = pGameMgr->getSoundEngine()->addSoundSourceFromFile( "" );
//    if ( cannonShotSound )
//    {
//        std::cout << "..." << std::endl;
////        return true;
//    }
//    else
//    {
//        std::cout << "**** Sound Not Loaded" << std::endl;
////        return false;
//    }

    deathSound = NULL;
    deathSound = pGameMgr->getSoundEngine()->addSoundSourceFromFile( "media/sounds/wilhelm.wav" );
    if ( deathSound )
    {
        std::cout << "pDied.wav loaded" << std::endl;
//        return true;
    }
    else
    {
        std::cout << "**** Sound Not Loaded" << std::endl;
//        return false;
    }

//    flagTakenSound = NULL;
//    flagTakenSound = pGameMgr->getSoundEngine()->addSoundSourceFromFile( "" );
//    if ( flagTakenSound )
//    {
//        std::cout << "..." << std::endl;
////        return true;
//    }
//    else
//    {
//        std::cout << "**** Sound Not Loaded" << std::endl;
////        return false;
//    }

//    flagResetSound = NULL;
//    flagResetSound = pGameMgr->getSoundEngine()->addSoundSourceFromFile( "" );
//    if ( flagResetSound )
//    {
//        std::cout << "..." << std::endl;
////        return true;
//    }
//    else
//    {
//        std::cout << "**** Sound Not Loaded" << std::endl;
////        return false;
//    }

//    buttonPressSound = NULL;
//    buttonPressSound = pGameMgr->getSoundEngine()->addSoundSourceFromFile( "" );
//    if ( buttonPressSound )
//    {
//        std::cout << "..." << std::endl;
////        return true;
//    }
//    else
//    {
//        std::cout << "**** Sound Not Loaded" << std::endl;
////        return false;
//    }

//    victorySound = NULL;
//    victorySound = pGameMgr->getSoundEngine()->addSoundSourceFromFile( "" );
//    if ( victorySound )
//    {
//        std::cout << "..." << std::endl;
////        return true;
//    }
//    else
//    {
//        std::cout << "**** Sound Not Loaded" << std::endl;
////        return false;
//    }

//    defeatSound = NULL;
//    defeatSound = pGameMgr->getSoundEngine()->addSoundSourceFromFile( "" );
//    if ( defeatSound )
//    {
//        std::cout << "..." << std::endl;
////        return true;
//    }
//    else
//    {
//        std::cout << "**** Sound Not Loaded" << std::endl;
////        return false;
//    }

    reloadSound = NULL;
    reloadSound = pGameMgr->getSoundEngine()->addSoundSourceFromFile( "media/sounds/reloadBasic.wav" );
    if ( reloadSound )
    {
        std::cout << "..." << std::endl;
//        return true;
    }
    else
    {
        std::cout << "**** Sound Not Loaded" << std::endl;
//        return false;
    }


    impactSound = NULL;
    impactSound = pGameMgr->getSoundEngine()->addSoundSourceFromFile( "media/sounds/impactOne.wav" );
    if ( impactSound )
    {
        std::cout << "explosion.wav Loaded" << std::endl;
//        return true;
    }
    else
    {
        std::cout << "**** Sound Not Loaded" << std::endl;
//        return false;
    }

    impactTwoSound = NULL;
    impactTwoSound = pGameMgr->getSoundEngine()->addSoundSourceFromFile( "media/sounds/impactTwo.wav" );
    if ( impactTwoSound )
    {
        std::cout << "explosion.wav Loaded" << std::endl;
//        return true;
    }
    else
    {
        std::cout << "**** Sound Not Loaded" << std::endl;
//        return false;
    }

    impactThreeSound = NULL;
    impactThreeSound = pGameMgr->getSoundEngine()->addSoundSourceFromFile( "media/sounds/impactThree.wav" );
    if ( impactThreeSound )
    {
        std::cout << "explosion.wav Loaded" << std::endl;
//        return true;
    }
    else
    {
        std::cout << "**** Sound Not Loaded" << std::endl;
//        return false;
    }

    impactFourSound = NULL;
    impactFourSound = pGameMgr->getSoundEngine()->addSoundSourceFromFile( "media/sounds/impactFour.wav" );
    if ( impactFourSound )
    {
        std::cout << "explosion.wav Loaded" << std::endl;
//        return true;
    }
    else
    {
        std::cout << "**** Sound Not Loaded" << std::endl;
//        return false;
    }

    impactFiveSound = NULL;
    impactFiveSound = pGameMgr->getSoundEngine()->addSoundSourceFromFile( "media/sounds/impactFive.wav" );
    if ( impactFiveSound )
    {
        std::cout << "explosion.wav Loaded" << std::endl;
//        return true;
    }
    else
    {
        std::cout << "**** Sound Not Loaded" << std::endl;
//        return false;
    }



    shotSound = NULL;
    shotSound = pGameMgr->getSoundEngine()->addSoundSourceFromFile( "media/sounds/pistolShot.wav" );
    if ( shotSound )
    {
        std::cout << "pistolShot.wav Loaded" << std::endl;
//        return true;
    }
    else
    {
        std::cout << "**** Sound Not Loaded" << std::endl;
//        return false;
    }

    shotTwoSound = NULL;
    shotTwoSound = pGameMgr->getSoundEngine()->addSoundSourceFromFile( "media/sounds/rifleShot.wav" );
    if ( shotTwoSound )
    {
        std::cout << "rifleShot.wav Loaded" << std::endl;
//        return true;
    }
    else
    {
        std::cout << "**** Sound Not Loaded" << std::endl;
//        return false;
    }

    shotThreeSound = NULL;
    shotThreeSound = pGameMgr->getSoundEngine()->addSoundSourceFromFile( "media/sounds/minigun.wav" );
    if ( shotThreeSound )
    {
        std::cout << "minigun.wav Loaded" << std::endl;
//        return true;
    }
    else
    {
        std::cout << "**** Sound Not Loaded" << std::endl;
//        return false;
    }

    shotFourSound = NULL;
    shotFourSound = pGameMgr->getSoundEngine()->addSoundSourceFromFile( "media/sounds/blunshot.wav" );
    if ( shotFourSound )
    {
        std::cout << "blunshot.wav Loaded" << std::endl;
//        return true;
    }
    else
    {
        std::cout << "**** Sound Not Loaded" << std::endl;
//        return false;
    }


    reloadCallSound = NULL;
    reloadCallSound = pGameMgr->getSoundEngine()->addSoundSourceFromFile( "media/sounds/pReloading.wav" );
    if ( reloadCallSound )
    {
        std::cout << "Reloading call loaded" << std::endl;
//        return true;
    }
    else
    {
        std::cout << "Reloading call  Sound Not Loaded" << std::endl;
//        return false;
    }
    healthSound = NULL;
    healthSound = pGameMgr->getSoundEngine()->addSoundSourceFromFile( "media/sounds/health.wav" );
    if ( healthSound )
    {
        std::cout << "health sound loaded" << std::endl;
//        return true;
    }
    else
    {
        std::cout << "**** Sound Not Loaded" << std::endl;
//        return false;
    }


    pickupSoundOne = NULL;
    pickupSoundOne = pGameMgr->getSoundEngine()->addSoundSourceFromFile( "media/sounds/PistolPickup.wav" );
    if ( pickupSoundOne )
    {
        std::cout << "Pistol Pickup Loaded" << std::endl;
//        return true;
    }
    else
    {
        std::cout << "pistol pickup Sound Not Loaded" << std::endl;
//        return false;
    }

    pickupSoundTwo = NULL;
    pickupSoundTwo = pGameMgr->getSoundEngine()->addSoundSourceFromFile( "media/sounds/RiflePickup.wav" );
    if ( pickupSoundTwo )
    {
        std::cout << "RiflePickup loaded" << std::endl;
//        return true;
    }
    else
    {
        std::cout << "RiflePickup Sound Not Loaded" << std::endl;
//        return false;
    }

    pickupSoundThree = NULL;
    pickupSoundThree = pGameMgr->getSoundEngine()->addSoundSourceFromFile( "media/sounds/BlunPickup.wav" );
    if ( pickupSoundThree )
    {
        std::cout << "BlunPickup Loaded" << std::endl;
//        return true;
    }
    else
    {
        std::cout << "*BlunPickup Sound Not Loaded" << std::endl;
//        return false;
    }

    pickupSoundFour = NULL;
    pickupSoundFour = pGameMgr->getSoundEngine()->addSoundSourceFromFile( "media/sounds/MinigunPickup.wav" );
    if ( pickupSoundFour )
    {
        std::cout << "MinigunPickup Loaded" << std::endl;
//        return true;
    }
    else
    {
        std::cout << "MinigunPickup Sound Not Loaded" << std::endl;
//        return false;
    }

    jumpSound = NULL;
    jumpSound = pGameMgr->getSoundEngine()->addSoundSourceFromFile( "media/sounds/pJump.wav" );
    if ( jumpSound )
    {
        std::cout << "Jump Loaded" << std::endl;
//        return true;
    }
    else
    {
        std::cout << "Jump Sound Not Loaded" << std::endl;
//        return false;
    }

    hitSound = NULL;
    hitSound = pGameMgr->getSoundEngine()->addSoundSourceFromFile( "media/sounds/pHit.wav" );
    if ( hitSound )
    {
        std::cout << "hit Loaded" << std::endl;
//        return true;
    }
    else
    {
        std::cout << "hit Sound Not Loaded" << std::endl;
//        return false;
    }
    gunEmptySound = NULL;
    gunEmptySound = pGameMgr->getSoundEngine()->addSoundSourceFromFile( "media/sounds/pEmpty.wav" );
    if ( gunEmptySound )
    {
        std::cout << "empty Loaded" << std::endl;
//        return true;
    }
    else
    {
        std::cout << "empty Sound Not Loaded" << std::endl;
//        return false;
    }

    cout << "sounds initialised" << endl;
    return true;

}

void LvlDerelict::keyboardEvent( SEvent event )
{



    if ( event.KeyInput.PressedDown )
    {
        switch ( event.KeyInput.Key )
        {
//        case irr::KEY_F8:
//            pGameMgr->changeState( StartMenu::getInstance() );
//            break;
//        case irr::KEY_F9:
//            pGameMgr->changeState( GameSelection::getInstance() );
//            break;
//        case irr::KEY_F10:
//            pGameMgr->changeState( LevelSelection::getInstance() );
//            break;
//        case irr::KEY_F11:
//            pGameMgr->changeState( LvlDerelict::getInstance() );
//            break;
//        case irr::KEY_F12:
//            pGameMgr->changeState( LvlHopping::getInstance() );
//            break;
//        case irr::KEY_F7:
//            pGameMgr->changeState( GameSettings::getInstance() );
//            break;
//
//        case irr::KEY_KEY_Q:
//            disconnect = true;
//            break;

        case irr::KEY_F2: // toggle debug label on/off
            showDebugData = !showDebugData;
            if( showDebugData )
                lblDebugData->setVisible( true );
            else
                lblDebugData->setVisible( false );
            break;
        case irr::KEY_F3: // switch wire frame mode
            /*
            //{ levelMesh
            cout << "Handled Keystroke" << endl;
            levelMesh->setMaterialFlag( video::EMF_WIREFRAME, !levelMesh->getMaterial(0).Wireframe );
            //            levelMesh->setMaterialFlag(video::EMF_WIREFRAME, true);
            cout << "Handled material flag switch(wireframe)" << endl;
            levelMesh->setMaterialFlag( video::EMF_POINTCLOUD, false );
            cout << "Handled setting of material flag(pointcloud)" << endl;
            //}
            */
            //{ ship1Mesh
            cout << "Handled Keystroke" << endl;
            ship1Mesh->setMaterialFlag( video::EMF_WIREFRAME, !ship1Mesh->getMaterial( 0 ).Wireframe );
//            ship1Mesh->setMaterialFlag(video::EMF_WIREFRAME, true);
            cout << "Handled material flag switch(wireframe)" << endl;
            ship1Mesh->setMaterialFlag( video::EMF_POINTCLOUD, false );
            cout << "Handled setting of material flag(pointcloud)" << endl;
            //}
            //{ ship2Mesh
            cout << "Handled Keystroke" << endl;
            ship2Mesh->setMaterialFlag( video::EMF_WIREFRAME, !ship2Mesh->getMaterial( 0 ).Wireframe );
//            ship2Mesh->setMaterialFlag(video::EMF_WIREFRAME, true);
            cout << "Handled material flag switch(wireframe)" << endl;
            ship2Mesh->setMaterialFlag( video::EMF_POINTCLOUD, false );
            cout << "Handled setting of material flag(pointcloud)" << endl;
            //}
            break;
        case irr::KEY_F4: // switch points only mode
            /*
            //{ levelMesh
            levelMesh->setMaterialFlag( video::EMF_POINTCLOUD, !levelMesh->getMaterial( 0 ).PointCloud );
            levelMesh->setMaterialFlag( video::EMF_WIREFRAME, false );
            //}
            */
            //{ ship1Mesh
            ship1Mesh->setMaterialFlag( video::EMF_POINTCLOUD, !ship1Mesh->getMaterial( 0 ).PointCloud );
            ship1Mesh->setMaterialFlag( video::EMF_WIREFRAME, false );
            //}
            //{ ship2Mesh
            ship2Mesh->setMaterialFlag( video::EMF_POINTCLOUD, !ship2Mesh->getMaterial( 0 ).PointCloud );
            ship2Mesh->setMaterialFlag( video::EMF_WIREFRAME, false );
            //}
            break;
        case irr::KEY_F5: // toggle detail map
            /*
            //{ levelMesh
            levelMesh->setMaterialType(
                levelMesh->getMaterial( 0 ).MaterialType == video::EMT_SOLID ?
                video::EMT_DETAIL_MAP : video::EMT_SOLID );
            //}
            //{ ship1Mesh
            levelMesh->setMaterialType(
                ship1Mesh->getMaterial( 0 ).MaterialType == video::EMT_SOLID ?
                video::EMT_DETAIL_MAP : video::EMT_SOLID );
            //}
            //{ ship2mesh
            levelMesh->setMaterialType(
                ship2Mesh->getMaterial( 0 ).MaterialType == video::EMT_SOLID ?
                video::EMT_DETAIL_MAP : video::EMT_SOLID );
            //}
            */
            break;



        case irr::KEY_KEY_P: // set P to Pause
            //pGameMgr->pushState( PauseGUI::getInstance() );

            if ( PauseCamera )
            {
                isPaused = !isPaused;
                PauseCamera->remove();
                pSceneMgr->setActiveCamera( pCamera );
                PauseCamera = NULL;
                cout << "REMOVED PAUSE CAMERA" << endl;

                btnResume->setVisible(false);

                btnOptions->setVisible(false);

                btnExitGame->setVisible(false);

                btnExitWindow->setVisible(false);



            }

            else
            {
                PauseCamera = pSceneMgr->addCameraSceneNode(
                                  0,              // parent
                                  //core::vector3df( 0, 0, 0 ), // position
                                  pCamera->getAbsolutePosition(),
                                  //core::vector3df( 0, 0, 100 ), // lookat
                                  pCamera->getTarget(),
                                  -1, false
                              );
                pSceneMgr->setActiveCamera( PauseCamera );
                isPaused = !isPaused;

                btnResume->setVisible(true);

                btnOptions->setVisible(true);

                btnExitGame->setVisible(true);

                btnExitWindow->setVisible(true);

            }





//            if (isPaused == true)
//            {
//                pSceneMgr->setActiveCamera(PauseCamera);
//
//
//            }
//
//            else
//            {
//                pSceneMgr->setActiveCamera(pCamera);
//            }



            break;

        case irr::KEY_ESCAPE:
//            disconnect = true;

            if ( isPaused )
            {
                isPaused = false;
                PauseCamera->remove();
                pSceneMgr->setActiveCamera( pCamera );
                PauseCamera = NULL;
                cout << "REMOVED PAUSE CAMERA" << endl;

                btnResume->setVisible(false);

                btnOptions->setVisible(false);

                btnExitGame->setVisible(false);

                btnExitWindow->setVisible(false);

                btnBack->setVisible( false );
                scrVolume->setVisible( false );
                chkSoundOn->setVisible( false );
                pGUIEnv->removeFocus(btnResume);
                pGUIEnv->removeFocus(btnBack);
                pGUIEnv->removeFocus(btnOptions);
                if(soundEnabled != chkSoundOn->isChecked())
                {
                    soundEnabled = chkSoundOn->isChecked();
                    initSounds();
                }

            }

            else
            {
                PauseCamera = pSceneMgr->addCameraSceneNode(
                                  0,              // parent
                                  //core::vector3df( 0, 0, 0 ), // position
                                  pCamera->getAbsolutePosition(),
                                  //core::vector3df( 0, 0, 100 ), // lookat
                                  pCamera->getTarget(),
                                  -1, false
                              );
                pSceneMgr->setActiveCamera( PauseCamera );
                isPaused = true;

                btnResume->setVisible(true);

                btnOptions->setVisible(true);

                btnExitGame->setVisible(true);

                btnExitWindow->setVisible(true);

            }

            break;

            if(isPaused)
                return;

        case irr::KEY_KEY_1: // set selected weapon to the first one
            if ( pPlayerObject->getWeaponAvailability( 1 ) )
            {
                weaponSelection = 1;
                pPlayerObject->setSelectedWeapon( weaponSelection );
                pCamera->removeChild( gun );
                initGuns( 1 );
                gunTimer = gameTime + 1000;
            }

            flashWeapons();

            break;
        case irr::KEY_KEY_2: // set selected weapon to the first one
            if ( pPlayerObject->getWeaponAvailability( 2 ) )
            {
                weaponSelection = 2;
                pPlayerObject->setSelectedWeapon( weaponSelection );
                pCamera->removeChild( gun );
                initGuns( 2 );
                gunTimer = gameTime + 1000;
            }

            flashWeapons();

            break;
        case irr::KEY_KEY_3: // set selected weapon to the first one
            if ( pPlayerObject->getWeaponAvailability( 3 ) )
            {
                weaponSelection = 3;
                pPlayerObject->setSelectedWeapon( weaponSelection );
                pCamera->removeChild( gun );
                initGuns( 3 );
                gunTimer = gameTime + 1000;
            }

            flashWeapons();

            break;
        case irr::KEY_KEY_4: // set selected weapon to the first one
            if ( pPlayerObject->getWeaponAvailability( 4 ) )
            {
                weaponSelection = 4;
                pPlayerObject->setSelectedWeapon( weaponSelection );
                pCamera->removeChild( gun );
                initGuns( 4 );
                gunTimer = gameTime + 1000;
            }

            flashWeapons();

            break;
//        case irr::KEY_KEY_C: // create second camera to look at pCamera
//            if ( secondCamera )
//            {
//                secondCamera->remove();
//                pSceneMgr->setActiveCamera( pCamera );
//                secondCamera = NULL;
//                cout << "REMOVED SECOND CAMERA" << endl;
//            }
//            else if ( !secondCamera )
//            {
//                SKeyMap keyMap[9];
//                keyMap[0].Action = EKA_MOVE_FORWARD;
//                keyMap[0].KeyCode = KEY_UP;
//                keyMap[1].Action = EKA_MOVE_FORWARD;
//                keyMap[1].KeyCode = KEY_KEY_W;
//
//                keyMap[2].Action = EKA_MOVE_BACKWARD;
//                keyMap[2].KeyCode = KEY_DOWN;
//                keyMap[3].Action = EKA_MOVE_BACKWARD;
//                keyMap[3].KeyCode = KEY_KEY_S;
//
//                keyMap[4].Action = EKA_STRAFE_LEFT;
//                keyMap[4].KeyCode = KEY_LEFT;
//                keyMap[5].Action = EKA_STRAFE_LEFT;
//                keyMap[5].KeyCode = KEY_KEY_A;
//
//                keyMap[6].Action = EKA_STRAFE_RIGHT;
//                keyMap[6].KeyCode = KEY_RIGHT;
//                keyMap[7].Action = EKA_STRAFE_RIGHT;
//                keyMap[7].KeyCode = KEY_KEY_D;
//
//                keyMap[8].Action = EKA_JUMP_UP;
//                keyMap[8].KeyCode = KEY_SPACE;
//
//
//                secondCamera = pSceneMgr->addCameraSceneNodeFPS(
//                                   0,          // parent
//                                   100.0f,     // rotate speed
//                                   0.5f,       // move speed
//                                   0,         // node ID
//                                   keyMap,     // key array
//                                   9,          // array size
//                                   false,       // no vertical
//                                   8.0f        // jump speed
//                               );
//
//                if ( secondCamera == NULL )
//                    throw "addSecondCameraSceneNodeFPS() failed";
//                // set position of camera and the target
//                secondCamera->setPosition( pCamera->getAbsolutePosition() + core::vector3df( 0, 25, 0 ) );               //  -3528.0f, 750.0f, -3.0f
//                secondCamera->setTarget( core::vector3df( 0, 0, 0 ) );    // 33.0f, 615.0f, 55.0
//                secondCamera->setFarValue( 40000.0f );
//                secondCamera->setNearValue( 0.01f );
//                pSceneMgr->setActiveCamera( secondCamera );
//            }
//            break;

        case irr::KEY_HOME:
            if( isServer )
            {
                gameButton = true;
            }
            break;

        case irr::KEY_KEY_R:
            reloadTimer();
            sendAnimation = ACTION_RELOAD;
            break;

        case irr::KEY_KEY_W:
            sendAnimation = MOVE_FORWARD;
            break;
        case irr::KEY_KEY_S:
            sendAnimation = MOVE_BACKWARD;
            break;
        case irr::KEY_KEY_A:
            sendAnimation = MOVE_LEFT;
            break;
        case irr::KEY_KEY_D:
            sendAnimation = MOVE_RIGHT;
            break;
        case irr::KEY_SPACE:
            if( !isPaused)
            {

                if ( !pGameMgr->getKey( irr::KEY_SPACE ) )
                {

                    if(soundEnabled == true && jumpDelay == false)
                        pGameMgr->getSoundEngine()->play2D( jumpSound );
                    jumpDelay = true;
                }
                sendAnimation = MOVE_JUMP;


            }
            break;




        default :
            break;
        }
    }
}

void LvlDerelict::mouseEvent( SEvent event )
{
//    if ( event.MouseInput.Event == EMIE_LMOUSE_PRESSED_DOWN )
//    {
//        // shoot
//        shoot(weaponSelection);
//        cout << "*** Shot fired ***" << endl;
//    }

    if ( event.MouseInput.Event == EMIE_LMOUSE_PRESSED_DOWN )
    {
        if( pPlayerObject->getClip() <= 0 )
        {
            if(soundEnabled)
                pGameMgr->getSoundEngine()->play2D( gunEmptySound );
            isFiring = false;
        }
        else
        {
//            // fire ze veapon
//            if( pPlayerObject->getSelectedWeapon() == 3 )
//            {
//                gun->setFrameLoop( 1, 20 );
//                gun->animateJoints();
//                gun->setAnimationSpeed( 40 );
//                gun->setLoopMode( true );
//
//            }
//            if( pPlayerObject->getSelectedWeapon() == 3 )
//            {
//                // waste 2 seconds of time somehow(running animations but not proceeding further down this piece of code)
////            warmUp = timeNow+75;
////            while(timeNow<=warmUp)
////            {
////            }
//            }



            // set isFiring to true



            isFiring = true;
            shoot( pPlayerObject->getSelectedWeapon() );
        }


    }

    if ( event.MouseInput.Event == EMIE_LMOUSE_LEFT_UP )
    {
        // set isFiring to false
        isFiring = false;
        if( pPlayerObject->getClip() <= 0 )
        {

        }
        else
        {
//            if( pPlayerObject->getSelectedWeapon() == 3 )
//            {
//                gun->animateJoints();
//                gun->setFrameLoop( 1, 5 );
//                gun->setAnimationSpeed( 40 );
//                gun->setFrameLoop( 5, 10 );
//                gun->setAnimationSpeed( 20 );
//                gun->setFrameLoop( 10, 15 );
//                gun->setAnimationSpeed( 10 );
//                gun->setFrameLoop( 15, 20 );
//                gun->setAnimationSpeed( 5 );
//                gun->setLoopMode( false );
//            }
        }

    }

    if( event.MouseInput.Event == EMIE_RMOUSE_DOUBLE_CLICK )
    {
        killPlayer();
        respawnPlayer();
    }


//    // Handle the repeated shooting
//    while (event.MouseInput.isLeftPressed())
//    {
//        shoot(weaponSelection);
//        cout << "**** Shot Fired ****" << endl;
//    }

}

void LvlDerelict::mouseGUIEvent( SEvent event )
{

    s32 id = event.GUIEvent.Caller->getID();
    switch ( event.GUIEvent.EventType )
    {
    case gui::EGET_BUTTON_CLICKED:
        switch ( id )
        {
        case GUI_BTN_RESUME:
//                isPaused = !isPaused;
//                PauseCamera->remove();
//                PauseCamera->removeAll();
//                pSceneMgr->setActiveCamera( pCamera );
//                PauseCamera = NULL;
//                cout << "REMOVED PAUSE CAMERA" << endl;
//
//                btnResume->setVisible(false);
//
//                btnOptions->setVisible(false);
//
//                btnExitGame->setVisible(false);
//
//                btnExitWindow->setVisible(false);
//
//                btnBack->setVisible( false );
//                scrVolume->setVisible( false );
//                chkSoundOn->setVisible( false );
//                    // deletes camera and guns

            if ( isPaused )
            {
                isPaused = false;
                PauseCamera->remove();
                pSceneMgr->setActiveCamera( pCamera );
                PauseCamera = NULL;
                cout << "REMOVED PAUSE CAMERA" << endl;

                btnResume->setVisible(false);

                btnOptions->setVisible(false);

                btnExitGame->setVisible(false);

                btnExitWindow->setVisible(false);

                btnBack->setVisible( false );
                scrVolume->setVisible( false );
                chkSoundOn->setVisible( false );
                pGUIEnv->removeFocus(btnResume);

                if(soundEnabled != chkSoundOn->isChecked())
                {
                    soundEnabled = chkSoundOn->isChecked();
                    initSounds();
                }

            }

            break;
        case GUI_BTN_OPTIONS:
            btnResume->setVisible( false );
            btnOptions->setVisible( false );
            btnBack->setVisible( true );
            //scrVolume->setVisible( true );
            chkSoundOn->setVisible( true );
            btnExitGame->setVisible( false );
            btnExitWindow->setVisible( false );
            break;
        case GUI_BTN_BACK:
            btnResume->setVisible( true );
            btnOptions->setVisible( true );
            btnBack->setVisible( false );
            scrVolume->setVisible( false );
            chkSoundOn->setVisible( false );
            btnExitGame->setVisible( true );
            btnExitWindow->setVisible( true );

            if(soundEnabled != chkSoundOn->isChecked())
            {
                soundEnabled = chkSoundOn->isChecked();
                initSounds();
            }

            break;
        case GUI_BTN_EXIT_GAME:
            pGameMgr->changeState( StartMenu::getInstance() );

            break;
        case GUI_BTN_EXIT_WINDOW:

            pGameMgr->running = false;

            break;
        case GUI_CHK_SOUND_ON:
//            soundEnabled = chkSoundOn->isChecked();
//            initSounds();

            break;
        default:
            break;
        }
        break;
    default:
        break;
    }

}

void LvlDerelict::clearKeys()
{
    for ( int x = 0; x < KEY_KEY_CODES_COUNT; ++x )
        keys[ x ] = false;
}

bool LvlDerelict::getKey( unsigned char key )
{
    return keys[ key ];
}

void LvlDerelict::freeResources( )
{
    cout << "freeResources() begun" << endl;
    if ( music )
        music->drop();

    for (  int index = 0; index < MAX_CLOUD_LAYERS; index++ )
    {
        if( cloudLayer[ index ] ) cloudLayer[ index ]->drop();
    }

    /*
    if ( cloudLayer1 )
        cloudLayer1->drop();
    if ( cloudLayer2 )
        cloudLayer2->drop();
    if ( cloudLayer3 )
        cloudLayer3->drop();
    */


    pSceneMgr->clear();
    pGUIEnv->clear();

    RakNet::RakPeerInterface::DestroyInstance( peer );


    cout << "LvlDerelict freeResources()" << endl;
}

void LvlDerelict::debugData()
{
    char buffer[256]; // using sprintf for its easy formatting ability
    core::stringw wstr( "FPS: " );       // debug title
    wstr += core::stringw( pVideo->getFPS() );
    wstr += "  Poly: ";
    wstr += core::stringw( pVideo->getPrimitiveCountDrawn() );
    // get game time
    unsigned int seconds = gameTime / 1000;
    sprintf( buffer, "  %02d:%02d", seconds / 60, seconds % 60 );
    wstr += buffer;

    // get camera position
    if ( pCamera )
    {
        sprintf( buffer, "\nCam: X:%.0f Y:%.0f Z:%.0f",
                 pCamera->getPosition().X,
                 pCamera->getPosition().Y,
                 pCamera->getPosition().Z );
        wstr += buffer;
        sprintf( buffer, "\nTgt: X:%.0f Y:%.0f Z:%.0f",
                 pCamera->getTarget().X,
                 pCamera->getTarget().Y,
                 pCamera->getTarget().Z );
        wstr += buffer;
//        sprintf( buffer, "\nRot: X:%.0f Y:%.0f Z:%.0f",
//                 playerMesh->getRotation().X,
//                 playerMesh->getRotation().Y,
//                 playerMesh->getRotation().Z );
    }
    else
        sprintf( buffer, "No Camera\n" );
    wstr += buffer;
    lblDebugData->setText( wstr.c_str() );
}

void LvlDerelict::updateTimer()
{
    timeNow = pDevice->getTimer()->getTime();
    timeDelta = timeNow - timeLast;
    timeLast = timeNow;
    gameTime += timeDelta;
}

void LvlDerelict::pause()
{
//    if( pSound )
//        pSound->pause();
    cout << "*** LvlDerelict pause() ***" << endl;
}

void LvlDerelict::resume()
{
//    if( pSound )
//        pSound->pause();
    cout << "*** LvlDerelict resume() ***" << endl;
}

bool LvlDerelict::rakConnect()
{



    peer = RakNet::RakPeerInterface::GetInstance();

    str[0] = pGameMgr->rakNetStateCS;


    if ( ( str[0] == 'c' ) || ( str[0] == 'C' ) )
    {
        RakNet::SocketDescriptor sd ;
        peer->Startup( 1, &sd, 1 );
        isServer = false;
    }
    else
    {
        RakNet::SocketDescriptor sd( SERVER_PORT, 0 );
        peer->Startup( MAX_CLIENTS, &sd, 1 );
        isServer = true;
    }

    cout << "Basic connection deployed!" << endl;

    if ( isServer )
    {
        printf( "Starting the server.\n" );
        // We need to let the server accept incoming connections from the clients
        peer->SetMaximumIncomingConnections( MAX_CLIENTS );
        //RakNet::RakPeerInterface->GetLocalIP( unsigned int index )
        //-peer->AdvertiseSystem( peer->GetLocalIP( 0 ), SERVER_PORT, 0, 0, 0 );
        serverCount += 1;
        pPlayerObject->setTeam( pPlayerObject->Imperial );
        imperialPlayers++;
    }
    else
    {
//        printf("Enter server IP or hit enter for 127.0.0.1\n");
//        gets(str);

//        if ( pGameMgr->joinIP[0] == 0 )
//        {
//            strcpy( pGameMgr->joinIP, "127.0.0.1" );
//        }
//        printf( "Starting the client.\n" );
        peer->Connect( pGameMgr->joinIP, SERVER_PORT, 0, 0 );

        pPlayerObject->setTeam( pPlayerObject->Rebel );

    }



    peer->SetMaximumIncomingConnections( MAX_CLIENTS );
    peer->SetTimeoutTime( 1000, RakNet::UNASSIGNED_SYSTEM_ADDRESS );

    printf( "Our guid is %s\n", peer->GetGuidFromSystemAddress( RakNet::UNASSIGNED_SYSTEM_ADDRESS ).ToString() );
    printf( "Started on %s\n", peer->GetMyBoundAddress().ToString( true ) );
    return true;
}

void LvlDerelict::rakUpdate()
{
    //TODO Make this able to be seen from other machines + add a timer to it so that it broadcasts more respectably?
//    if (pGameMgr->rakNetStateCS == 's')
//    {
//        peer->AdvertiseSystem(peer->GetLocalIP( 0 ),SERVER_PORT,0,0,0);
//        printf("Advertised System\n");
//    }

    RakNet::BitStream bsOut;
    RakNet::BitStream bsShoot;
    RakNet::BitStream bsJoin;
    RakNet::BitStream bsStartGame;
    RakNet::BitStream bsServerUpdate;

    for ( packet = peer->Receive(); packet; peer->DeallocatePacket( packet ), packet = peer->Receive() )
    {
        switch ( packet->data[0] )
        {
        case ID_ADVERTISE_SYSTEM:
            printf( "A system is Advertising to us!\n" );
            printf( "The Advertising system is at: " );
            printf( packet->systemAddress.ToString() );
            printf( "\n" );
            break;
        case ID_REMOTE_DISCONNECTION_NOTIFICATION:
            printf( "Another client has disconnected.\n" );
            break;
        case ID_REMOTE_CONNECTION_LOST:
            printf( "Another client has lost the connection.\n" );
            break;
        case ID_REMOTE_NEW_INCOMING_CONNECTION:
            printf( "Another client has connected.\n" );
            break;
        case ID_CONNECTION_REQUEST_ACCEPTED:
        {
            RakNet::BitStream bsIn( packet->data, packet->length, false );
            bsIn.IgnoreBytes( sizeof( RakNet::MessageID ) );
            printf( "Our connection request has been accepted.\n" );
            bsIn.Read( serverCount );
            // TODO change team here based on team numbers.


            // Use a BitStream to write a custom user message
            // Bitstreams are easier to use than sending casted structures, and handle endian swapping automatically
//					RakNet::BitStream bsOut;
            conNum += 1;
            break;
        } // connecting to a server

        case ID_NEW_INCOMING_CONNECTION:
            printf( "The server is full.\n" );

            serverCount += 1;
            cout << "here" << endl;
            if( pGameMgr->rakNetStateCS == 's' )
            {
                cout << "client sucessfully joined" << endl;
                bsJoin.Write( ( RakNet::MessageID )ID_CONNECTION_REQUEST_ACCEPTED );
                bsJoin.Write( serverCount );
                cout << "nnnoob bole" << endl;
                if( serverCount >= 2 )
                    startRespawn = gameTime + 10000;
                cout << "awe bole" << endl;
                peer->Send( &bsJoin, HIGH_PRIORITY, RELIABLE_ORDERED, 0, packet->systemAddress, false );
            }


            cout << "Problemo" << endl;
            break;  // disconnect


        case ID_DISCONNECTION_NOTIFICATION:
            if ( isServer )
            {
                printf( "A client has disconnected.\n" );

                //friendMesh->setVisible(false);
                friendMesh->setPosition( core::vector3df( 0.0f, 0.0f, 0.0f ) );
                cout << " boop 1 disconnection" << endl;
                serverCount -= 1;
            }
            else
            {
                printf( "We have been disconnected.\n" );
                //friendMesh->setVisible(false);
                friendMesh->setPosition( core::vector3df( 0.0f, 0.0f, 0.0f ) );
                cout << " boop 2 disconnection" << endl;
            }
            break;
        case ID_CONNECTION_LOST:
            conNum -= 1;
            if ( isServer )
            {
                printf( "A client lost the connection.\n" );
                friendMesh->setPosition( core::vector3df( 0.0f, 0.0f, 0.0f ) );
                cout << " boop 1 connection lost" << endl;
            }
            else
            {
                printf( "Connection lost.\n" );
                friendMesh->setPosition( core::vector3df( 0.0f, 0.0f, 0.0f ) );
                cout << " boop 2 connection lost" << endl;
            }
            break;
        case ID_GAME_MESSAGE_1:
        {

            RakNet::RakString rs;
            RakNet::BitStream bsIn( packet->data, packet->length, false );
            bsIn.IgnoreBytes( sizeof( RakNet::MessageID ) );
            bsIn.Read( rs );
            printf( "%s\n", rs.C_String() );

        }
        break;
        case ID_GAME_MESSAGE_2: // messages that is always sending ingame, movement, weapon and names

        {
            wchar_t nameCake[256];
            RakNet::RakString rs;
            RakNet::BitStream bsIn( packet->data, packet->length, false );
            bsIn.IgnoreBytes( sizeof( RakNet::MessageID ) );
//					bsIn.Read(rs);
            vector3df pos, angle;
            bsIn.Read( nameCake );

            bsIn.Read( pos.X );
            bsIn.Read( pos.Y );
            bsIn.Read( pos.Z );
            bsIn.Read( angle.Y );
            bsIn.Read( angle.Z );
            bsIn.Read( newWeapon );

            bsIn.Read( newAnimation );
            selectAnimation();



            friendMesh->setPosition( core::vector3df( pos.X, pos.Y, pos.Z ) );
            friendMesh->setRotation( core::vector3df( angle.X, angle.Y, angle.Z ) );
//            cout << "AH DREW MAH FREIND" << endl;
            if( billName == NULL )
            {
                cout << "bill nye" << endl;
                billName = pSceneMgr->addBillboardTextSceneNode( pGUIEnv->getFont( "media/fonts/bigfont.png" ),
                           nameCake,
                           0,
                           core::dimension2d<f32>( 50, 25 ),
                           core::vector3df( 0, 80, 0 )
                                                               );
                if ( pPlayerObject->getTeam() == pPlayerObject->Rebel )
                {
                    video::SColor tempColour;
                    tempColour.set( 255, 0, 0, 255 );
                    billName->setTextColor( tempColour );
                }
                else
                {
                    video::SColor tempColour;
                    tempColour.set( 255, 255, 0, 0 );
                    billName->setColor( tempColour );
                }
                cout << "finished names names" << endl;
            }


            friendMesh->addChild( billName );
        }

        break;
        case ID_GAME_MESSAGE_3:
        {
            vector3df pos, endPoint;
            int selected;
            RakNet::RakString rs;
            RakNet::BitStream bsIn( packet->data, packet->length, false );
            bsIn.IgnoreBytes( sizeof( RakNet::MessageID ) );
            bsIn.Read( selected );
            bsIn.Read( pos );
            bsIn.Read( endPoint );
            bsIn.Read( guidmy );
            shoot( selected, 1, pos, endPoint );

            cout << "********* GUID " << guidmy.ToString() << " **********" << endl;
            SystemAddress guidIP = peer->GetSystemAddressFromGuid( guidmy );
            cout << "********* GUID IP" << guidIP.ToString() << " **********" << endl;
            cout << "*** Name: " << pDataMgr->getSystemSetting( "name" ) << "*****" << endl;
            cout << "recieving boolets" << endl;
            cout << pos.X << endl;
            cout << pos.Y << endl;
            cout << pos.Z << endl;
            cout << endPoint.X << endl;
            cout << endPoint.Y << endl;
            cout << endPoint.Z << endl;

            printf( "%s\n", rs.C_String() );
            break;
        }
        case ID_GAME_MESSAGE_4: // message we send out saying that we died

        {
            cout << "***** Recieveded Death *****" << endl;
            bool isDead;
            int damageTaken;
            bool team;
            RakNet::BitStream bsIn( packet->data, packet->length, false );
            bsIn.IgnoreBytes( sizeof( RakNet::MessageID ) );

            bsIn.Read( isDead );
            bsIn.Read( damageTaken );
            bsIn.Read( team );
            cout << "damage taken = " << damageTaken << endl;

            pPlayerObject->takeDamage( damageTaken );

            if( pPlayerObject->health <= 0 )
            {
                pPlayerObject->setDead( isDead );
            }

            break;
        }
        case ID_GAME_MESSAGE_5:
        {
            cout << "Message 5 Received" << endl;
            // The server synchronisation!
            if ( !isServer )
            {
                cout << "Server Sent Synch" << endl;
                RakNet::BitStream bsIn( packet->data, packet->length, false );
                bsIn.IgnoreBytes( sizeof( RakNet::MessageID ) );
                bsIn.Read( serverCount );
                bsIn.Read( imperialKills );
                bsIn.Read( imperialPlayers );
                bsIn.Read( rebelKills );
                bsIn.Read( rebelPlayers );
                cout << "Recieved Server Synch Update" << imperialKills << " " << rebelKills << endl;

            }

            break;
        }

        case ID_GAME_MESSAGE_6: // keeps count of team scores
        {
            cout << "Message 6 Received" << endl;
            if ( isServer )
            {
                bool lossTeam;

                RakNet::BitStream bsIn( packet->data, packet->length, false );
                bsIn.IgnoreBytes( sizeof( RakNet::MessageID ) );
                bsIn.Read( lossTeam );
                if ( lossTeam == pPlayerObject->Imperial )
                {
                    cout << "Imperial Death Received" << endl;
                    rebelKills++;
                }
                else
                {
                    cout << "Rebel Death Received" << endl;
                    imperialKills++;
                }
                clientsNeedUpdate = true;
            }

            break;
        }

        case ID_GAME_MESSAGE_START_GAME:
        {
            cout << "Start Game Message received." << endl;
            startGame();
            break;
        }

        case ID_GAME_MESSAGE_CLIENT_COMMIT:
        {
            cout << "Client Commit Received." << endl;
            int temp;
            RakNet::BitStream bsIn( packet->data, packet->length, false );
            bsIn.IgnoreBytes( sizeof( RakNet::MessageID ) );
            bsIn.Read(temp);
            rebelKills+=temp;
            clientsNeedUpdate = true;
        }


        default:
            printf( "Message with identifier %i has arrived.\n", packet->data[0] );
            break;
        }


        if( disconnect ) // what happens when we disconnect

        {
            bsOut.Write( ( RakNet::MessageID )ID_DISCONNECTION_NOTIFICATION );
        }
        else
        {
            //    if(conNum > 0 && LvlDerelict::started == true)
//        {
            //cout << "Start of if test" << endl;
            // RakNet::BitStream bsUpdate;
            // cout << "Made bsUpdate" << endl;

            //        bsOut.Write((RakNet::MessageID)ID_GAME_MESSAGE_1);
            //cout << "Set ID" << endl;
            //        bsOut.Write("test data send");
            // cout << "Wrote data" << endl;
            //        bsOut.Write( (playerMesh->getPosition().X,
            //                  playerMesh->getPosition().Y,
            //                  playerMesh->getPosition().Z));
            //cout << "Wrote position" << endl;
            //        bsOut.Write(playerMesh->getRotation().Y);
            // cout << "Wrote rotation" << endl;


            bsOut.Write( ( RakNet::MessageID )ID_GAME_MESSAGE_2 );
//                        printf( " %f" , pCamera->getAbsolutePosition().X );
//                        printf( " %f" , pCamera->getAbsolutePosition().Y );
//                        printf( " %f" , pCamera->getAbsolutePosition().Z );
//                        printf( " %f" , pCamera->getRotation().X  );
//                        printf( " %f" , pCamera->getRotation().Y );
//                        printf( " %f" , pCamera->getRotation().Z );
//            playerMesh->setVisible(true);


            const char* temp = pDataMgr->getSystemSetting( "name" ).c_str();
            // cout << "making bacon" << endl;
            wchar_t wcstring[256];
            // cout << "hurduhurduhur" << endl;
            mbstowcs( wcstring, temp, strlen( temp ) + 1 );
            // cout << "pootis" << endl;

            bsOut.Write( wcstring );




            // what happens when we die
            if( pPlayerObject->isDead() )
            {
                bsOut.Write( 0 );
                bsOut.Write( 0 );
                bsOut.Write( 0 );
                bsOut.Write( 0 );
                bsOut.Write( 0 );
                bsOut.Write( pPlayerObject->getSelectedWeapon() );
                bsOut.Write( sendAnimation );
            }
            else
            {
                bsOut.Write( pCamera->getAbsolutePosition().X );
                bsOut.Write( pCamera->getAbsolutePosition().Y - 35 );
                bsOut.Write( pCamera->getAbsolutePosition().Z - 10 );
                bsOut.Write( pCamera->getRotation().Y );
                bsOut.Write( pCamera->getRotation().Z );
                bsOut.Write( pPlayerObject->getSelectedWeapon() );
                bsOut.Write( sendAnimation );
            }



//            playerMesh->setVisible(false);

            //cout << "making names" << endl;
        }


        peer->Send( &bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, packet->systemAddress, false );
        if( shooting ) // our shooting data gets sent to other players

        {
            bsShoot.Write( ( RakNet::MessageID )ID_GAME_MESSAGE_3 );
            bsShoot.Write( weaponSelection );
            bsShoot.Write( sStart );
            bsShoot.Write( sEnd );
            bsShoot.Write( guidmy );
            peer->Send( &bsShoot, HIGH_PRIORITY, RELIABLE_ORDERED, 0, packet->systemAddress, false );
            shooting = 0;

            cout << "sending boolets" << endl;
            cout << sStart.X << endl;
            cout << sStart.Y << endl;
            cout << sStart.Z << endl;
            cout << sEnd.X << endl;
            cout << sEnd.Y << endl;
            cout << sEnd.Z << endl;
        }


        if( gameButton && !gameStarted )
        {
            startGame();
            if( isServer )
            {
                bsStartGame.Write( ( RakNet::MessageID )ID_GAME_MESSAGE_START_GAME );
                bsStartGame.Write( gameStarted );
                peer->Send( &bsStartGame, HIGH_PRIORITY, RELIABLE_ORDERED, 0, packet->systemAddress, false );
            }
            gameStarted = true;
        }


        if( enemyHit ) // what happens to our health when we get shot

        {
            RakNet::BitStream bsHit;
            bsHit.Write( ( RakNet::MessageID )ID_GAME_MESSAGE_4 );
            bsHit.Write( enemyHit );

            switch ( pPlayerObject->getSelectedWeapon() )
            {
            case 1:
                tempDamageInt = 20;
                break;

            case 2:
                tempDamageInt = 15;
                break;

            case 3:
                tempDamageInt = 10;
                break;

            case 4:
                tempDamageInt = 35;
                break;

            default:
                tempDamageInt = 25;
                break;
            }
            bsHit.Write( tempDamageInt );
            bsHit.Write( pPlayerObject->getTeam() );
            peer->Send( &bsHit, HIGH_PRIORITY, RELIABLE_ORDERED, 0, packet->systemAddress, false );
            cout << "sended" << endl;
            enemyHit = false;
            cout << "dieded false" << endl;
        }

        if ( clientsNeedUpdate )
        {
            cout << "Clients Need Update" << endl;
            if ( isServer )
            {
                clientsNeedUpdate = false;
                bsServerUpdate.Write( ( RakNet::MessageID )ID_GAME_MESSAGE_5 );
                bsServerUpdate.Write( serverCount );
                bsServerUpdate.Write( imperialKills );
                bsServerUpdate.Write( imperialPlayers );
                bsServerUpdate.Write( rebelKills );
                bsServerUpdate.Write( rebelPlayers );
                peer->Send( &bsServerUpdate, HIGH_PRIORITY, RELIABLE_ORDERED, 4, packet->systemAddress, false );
                cout << "Client Update Sent" << endl;
            }

        }

        if( pPlayerObject->isDead() && !isServer && clientDiedOnce == false )
        {
            cout << "Client Has Dieded" << endl;
            RakNet::BitStream bsDead;
            bsDead.Write( ( RakNet::MessageID )ID_GAME_MESSAGE_6 );
            bsDead.Write( pPlayerObject->getTeam() );
            peer->Send( &bsDead, HIGH_PRIORITY, RELIABLE_ORDERED, 7, packet->systemAddress, false );
            clientDiedOnce = true;
            cout << "moar testing " << endl;
        }

        if (clientScoreUpdate == true)
        {
            clientScoreUpdate = false;
            RakNet::BitStream bsCommitClientScore;
            bsCommitClientScore.Write(( RakNet::MessageID )ID_GAME_MESSAGE_CLIENT_COMMIT);
            bsCommitClientScore.Write(entityArray[250].getValue());
            peer->Send( &bsCommitClientScore, HIGH_PRIORITY, RELIABLE_ORDERED, 9, packet->systemAddress, false );
        }

        if( pPlayerObject->isDead() && isServer && clientDiedOnce == false )
        {
            cout << "Server Has Dieded" << endl;
            if ( pPlayerObject->getTeam() == pPlayerObject->Imperial )
            {
                rebelKills++;
            }

            else
            {
                imperialKills++;
            }
            clientsNeedUpdate = true;
            clientDiedOnce = true;
        }
    }
}


