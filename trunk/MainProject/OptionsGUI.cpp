#include "OptionsGUI.h"
#include "PauseGUI.h"
using namespace irr;

//! define the static singleton pointer to this class
OptionsGUI OptionsGUI::instance;

void OptionsGUI::update()
{
    updateTimer();
    pVideo->beginScene( true, true, video::SColor( 255, 100, 101, 140 ) );
    //draw a bg image
    pVideo->draw2DImage( bgImage,
                         core::rect<s32>( 0, 0, pGameMgr->getScreenResolution().Width, pGameMgr->getScreenResolution().Height ),
                         core::rect<s32>( 0, 0, bgImage->getSize().Width, bgImage->getSize().Height )
                       );
    pSceneMgr->drawAll();
    pGUIEnv->drawAll();
    // draw mouse cursor but not if using an irrlicht FPS camera
//    core::position2d<s32> mousePos = pDevice->getCursorControl()->getPosition();
//    pVideo->draw2DImage(
//        mouseCursor,
//        core::position2d<s32>( mousePos.X, mousePos.Y ),
//        core::rect<s32>( 0, 0, 16, 16 ),
//        0,
//        video::SColor( 255, 255, 255, 255 ),
//        true );
#ifdef _DEBUG
    if( showDebugData )
        this->displayDebugData();
#endif
    pVideo->endScene();
}

bool OptionsGUI::initState( GameMgr& game )
{
    // set the local pointer to the game manager reference address
    pGameMgr = &game;
    // init the other Irrlicht system pointers
    pDevice = pGameMgr->getDevice();
    pVideo = pGameMgr->getVideoDriver();
    pSceneMgr = pGameMgr->getSceneManager();
    pGUIEnv = pGameMgr->getGUIEnvironment();

//    // initialise mouse cursor image
//    mouseCursor = pVideo->getTexture( "media/cursors/crosshair.png" );
//    // set the image transparent colour - the pixel in pos 0,0 for this image
//    pVideo->makeColorKeyTexture( mouseCursor, core::position2d<s32>( 0, 0 ) );
    // disable system mouse cursor to draw custom cursor
    pDevice->getCursorControl()->setVisible( true );
    // init the keys array
    pGameMgr->clearKeys();
    // the order of the following function calls will be important
    // when there are dependencies so add/rearrange/remove as required.
    if ( ! this->initData() )
        return false;
    if ( ! this->initWorld() )
        return false;
    if ( ! this->initCameras() )
        return false;
    if ( ! this->initCollisionDetection() )
        return false;
    if ( ! this->initGUI() )
        return false;
    cout << "OptionsGUI initState() success  finally" << endl;
    return true;
}

bool OptionsGUI::initData()
{
#ifdef _DEBUG
    showDebugData = true;
#else
    showDebugData = false;
#endif
    gameTime = timeDelta = 0;
    desiredFPS = 60;
    cout << "OptionsGUI initData() success" << endl;
    return true;
}

//bool OptionsGUI::initNodes()
//{
//    cout << "OptionsGUI initNodes() success" << endl;
//    return true;
//}

bool OptionsGUI::initCameras()
{
    pCamera = pSceneMgr->addCameraSceneNode( );
    if ( ! pCamera )
    {
        cout << "OptionsGUI initCameras() failed ***" << endl;
        return false;
    }
    // set position of camera and the camera target // default 0,0,0
    pCamera->setPosition( core::vector3df( 0.0f, 0.0f, -1024.0f ) );
    pCamera->setTarget( core::vector3df( 0.0f, 0.0f, 0.0f ) );
    pCamera->setFarValue( 4000.0f );    // default 2000
    cout << "OptionsGUI initCameras() success" << endl;
    return true;
}

bool OptionsGUI::initWorld()
{
    bgImage = pVideo->getTexture( "media/irrPauseGUIbg3.png" );
    fader = pGUIEnv->addInOutFader();
    fader->setColor( video::SColor( 0, 0, 0, 0 ) );
    //ARGB to start from
    fader->fadeIn( 3000 );


    // load terrain height map and colour overlay
//    terrain = pSceneMgr->addTerrainSceneNode(
//            "media/terrain/terrain-heightmap.png",      // grey scale (256) height map
//            0,                                          // parent
//            -1,                                         // id value - default -1 = none
//            core::vector3df( 0.0f, 0.0f, 0.0f ),        // location in the world
//            core::vector3df( 0.0f, 0.0f, 0.0f ),        // rotation
//            core::vector3df( 10.0f, 1.5f, 10.0f ),      // scale x, y and z
//            video::SColor( 255, 255, 255, 255 ),        // vertex colour default white
//            5,                                          // maximum level of detail
//            scene::ETPS_17,                             // patch Size
//            0,                                          // smooth Factor
//            false                                       // add Also If Height map Empty
//            );
//    if ( terrain )
//    {
//         large scale colour overlay
//        terrain->setMaterialTexture( 0, pVideo->getTexture( "media/terrain/terrain.jpg" ) );
//         "distant" detail
//        terrain->setMaterialTexture( 1, pVideo->getTexture( "media/textures/terr_dirt-grass.jpg" ) );
//         "close up" detail
//        terrain->setMaterialTexture( 2, pVideo->getTexture( "media/textures/sandripple.png" ) );
//         use detail mapped material
//        terrain->setMaterialType( video::EMT_DETAIL_MAP );
//         RTM for explanation
//        terrain->scaleTexture( 1.0f, 20.0f );
//         set location in the world
//        terrain->setPosition( core::vector3df( 0.0f, 0.0f, 0.0 ) );
//        terrain->setMaterialFlag( video::EMF_LIGHTING, false );
//            terrain->setMaterialFlag(video::EMF_FOG_ENABLE, true);
//    }
//    else
//        cout << "addTerrainSceneNode() failed" << endl;;
//
//     add sky box or sky dome
//    pSceneMgr->addSkyBoxSceneNode(
//        pVideo->getTexture( "media/sky/sea-top.jpg" ),
//        pVideo->getTexture( "media/sky/sea-down.jpg" ),
//        pVideo->getTexture( "media/sky/sea-left.jpg" ),
//        pVideo->getTexture( "media/sky/sea-right.jpg" ),
//        pVideo->getTexture( "media/sky/sea-front.jpg" ),
//        pVideo->getTexture( "media/sky/sea-back.jpg" )
//    );
//     add sky dome
//    pSceneMgr->addSkyDomeSceneNode( pVideo->getTexture( "media/sky/SkyDome1.png" ), 16, 8, 0.9, 2.0, 1000.0f, 0, 10 );
//    cout << "OptionsGUI initWorld() success" << endl;
    return true;
}

bool OptionsGUI::initCollisionDetection()
{
//    // to collect all the meshes in the terrain
//    scene::ITriangleSelector* selector = pSceneMgr->createTerrainTriangleSelector(terrain, 0);
//    terrain->setTriangleSelector(selector);
//    // must drop all objects made with a create...() function when finished with them
//    selector->drop();
//    // create collision response animator
//    scene::ISceneNodeAnimator* anim =
//        pSceneMgr->createCollisionResponseAnimator(
//            selector,                               // triangle selector - world
//            pCamera,                                // sceneNode
//            core::vector3df(10.0f, 60.0f, 10.0f),   // ellipsoid Radius default vector3df(30, 60, 30)
//            core::vector3df(0.0f, -5.0f, 0.0f),     // gravityPerSecond default vector3df(0,-100.0f, 0)
//            core::vector3df(0.0f, 5.0f, 0.0f ),     // ellipsoid Translation default vector3df(0, 0, 0)
//            0.0001f                                 // sliding value
//        );
//    if ( ! anim )
//    {
//        cout << "createCollisionResponseAnimator() failed" << endl;
//        return false;
//    }
//    pCamera->addAnimator( anim );
//    anim->drop();
    return true;
}

bool OptionsGUI::initGUI()
{
#ifdef _DEBUG
    // load the font map
    gui::IGUIFont* font = pGUIEnv->getFont( "media/fonts/ConsoleFont.bmp" );
    if ( ! font )
        return false;
    // creating a skin that uses different )from the default) fonts and colours - RTM for details
    gui::IGUISkin* newskin = pGUIEnv->createSkin( gui::EGST_WINDOWS_CLASSIC );
    newskin->setFont( font );
    pGUIEnv->setSkin( newskin );
    newskin->drop();
    // add a static label for the debug data
    lblDebugData = pGUIEnv->addStaticText(
                       L"",
                       core::rect<s32>( 0, 0, 256, 64 ),
                       true,
                       true,
                       0,
                       -1,
                       true );
    if ( ! lblDebugData )
        return false;
    lblDebugData->setOverrideColor( video::SColor( 255, 0, 0, 0 )  );

#endif
    // this happens for both debug and release
    // readXMLSettings("data/DeviceOptions.xml");

    // position the buttons to the centre/middle of the GUI screen...
    int leftX = ( pGameMgr->getScreenResolution().Width / 2 ) - 50 , topY = pGameMgr->getScreenResolution().Height / 3 , width = 80, height = 32;
    // OK and Cancel
    cmdBack = pGUIEnv->addButton(
                  core::rect<s32>( leftX, topY, leftX + width, topY + height ),   // location ans size
                  0,                                                              // parent
                  GUI_CMD_BACK,
                  L"Back",
                  L"Click to to back to the Pause Menu" );

    //
    topY = topY;




    return true;
}

void OptionsGUI::keyboardEvent( SEvent event )
{
    if ( event.KeyInput.PressedDown )
    {
        switch ( event.KeyInput.Key )
        {
        case irr::KEY_F1: // switch wire frame mode
#ifdef _DEBUG
            showDebugData = !showDebugData;
            if( showDebugData )
                lblDebugData->setVisible( true );
            else
                lblDebugData->setVisible( false );
#endif
            break;
        case irr::KEY_F2: // switch wire frame mode
//            terrain->setMaterialFlag( video::EMF_WIREFRAME, !terrain->getMaterial( 0 ).Wireframe );
//            terrain->setMaterialFlag( video::EMF_POINTCLOUD, false );
            break;
        case irr::KEY_F3: // switch points only mode
//            terrain->setMaterialFlag( video::EMF_POINTCLOUD, !terrain->getMaterial( 0 ).PointCloud );
//            terrain->setMaterialFlag( video::EMF_WIREFRAME, false );
            break;
        case irr::KEY_F4: // toggle detail map
//            terrain->setMaterialType(
//                terrain->getMaterial( 0 ).MaterialType == video::EMT_SOLID ?
//                video::EMT_DETAIL_MAP : video::EMT_SOLID );
            break;
        default :
            break;
        }
    }
}

void OptionsGUI::mouseEvent( SEvent event )
{
}

void OptionsGUI::mouseGUIEvent( SEvent event )
{
    // get the event ID number
    s32 id = event.GUIEvent.Caller->getID();
    switch ( event.GUIEvent.EventType )
    {
    case gui::EGET_BUTTON_CLICKED :
        if( id ==  GUI_CMD_BACK )
        {
            // get the user settings and save to XML
//            cout << "Clicked OK and Selected item " << cboRenderer->getSelected() <<
//                    " " << Utils::stdString(cboRenderer->getItem(cboRenderer->getSelected())) << endl;

            pGameMgr->changeState( PauseGUI::getInstance() );
            cout << "Clicked Back" << endl;

            //pGameMgr->running = false;
        }
//        else if ( id == GUI_CMD_OPTIONS )
//        {
//            cout << "Clicked Options" << endl;
//            pGameMgr->running = false;

//}
        break;

    default:
        break;
    }

}


void OptionsGUI::freeResources( )
{
    pSceneMgr->clear();
    pGUIEnv->clear();
    cout << "OptionsGUI freeResources()" << endl;
}


void OptionsGUI::displayDebugData()
{
    char buffer[256]; // using sprintf for its easy formatting abilities
    core::stringw wstr( "Debug - FPS: " );       // debug title
    wstr += core::stringw( pVideo->getFPS() );
    wstr += "\nPoly: ";
    wstr += core::stringw( pVideo->getPrimitiveCountDrawn() );
    // get camera position
    if ( pCamera )
    {
        sprintf( buffer, "\nCam: X:%.0f Y:%.0f Z:%.0f",
                 pCamera->getPosition().X,
                 pCamera->getPosition().Y,
                 pCamera->getPosition().Z );
    }
    else
        sprintf( buffer, "No Camera\n" );
    wstr += buffer;
    // get game time
    unsigned int seconds = gameTime / 1000;
    sprintf( buffer, "%02d:%02d:%02d", seconds / 60, seconds % 60, gameTime % 1000 );
    wstr += "\nTime: ";
    wstr += buffer;
    lblDebugData->setText( wstr.c_str() );
}

void OptionsGUI::updateTimer()
{
    timeNow = pDevice->getTimer()->getTime();
    timeDelta = timeNow - timeLast;
    timeLast = timeNow;
    gameTime += timeDelta;
}

void OptionsGUI::pause()
{
//    if( pSound )
//        pSound->pause();
    cout << "*** OptionsGUI pause() ***" << endl;
}

void OptionsGUI::resume()
{
//    if( pSound )
//        pSound->pause();
    cout << "*** OptionsGUI resume() ***" << endl;
}

























