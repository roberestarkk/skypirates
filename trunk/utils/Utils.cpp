#include "Utils.h"

float Utils::RadToDeg = 57.29577f;
float Utils::DegToRad = 0.0174532f;

std::wstring Utils::wideString( const std::string& pstring )
{
    // initialise a wide string to all blank characters
    std::wstring temp( pstring.length(), L' ' );
    // copy all the content of the standard (1 byte char) string to the (2 byte wchar_t)
    std::copy( pstring.begin(), pstring.end(), temp.begin() );
    return temp;
}

std::string Utils::stdString( const std::wstring& wideStr )
{
    std::string temp( wideStr.length(), ' ' );
    std::copy( wideStr.begin(), wideStr.end(), temp.begin() );
    return temp;
}

float Utils::toFloat( const std::string& pstring )
{
    return parseString<float>( pstring );
}

signed Utils::toInt( const std::string& pstring )
{
    return parseString<int>( pstring );
}

unsigned Utils::toUInt( const std::string& pstring )
{
    return parseString<unsigned int>( pstring );
}

bool Utils::toBoolean( const std::string& pstring )
{

    if ( ignoreCaseCompare( pstring, "true" )
            || ignoreCaseCompare( pstring, "T" )
            || pstring == "1" )
        return true;
    else
        return false;
}

bool Utils::ignoreCaseCompare( const std::string& s1, const std::string& s2 )
{
    // if both strings are the same size and every char is the same
    return( ( s1.size( ) == s2.size( ) ) &&
            equal( s1.begin( ), s1.end( ), s2.begin( ), ignoreCaseCharCompare ) );
}

bool Utils::ignoreCaseCharCompare( char a, char b )
{
    return( toupper( a ) == toupper( b ) );
}

std::string& Utils::toUpperCase( std::string& str )
{
    std::transform( str.begin(), str.end(), str.begin(), toupper );
    // passing the address of the C toupper() function to transform()
    return str;
}

std::string& Utils::toLowerCase( std::string& str )
{
    for ( std::string::iterator iter = str.begin(); iter != str.end(); ++iter )
        *iter = tolower( *iter );
    return str;

}

std::string& Utils::trimLeft( std::string& str )
{
    std::string::iterator iter = str.begin();
    // keep looping while char is a space
    while ( iter != str.end() && isspace( *iter ) )
        ++iter; // note test for end before using iter
    // if reached the end and is empty string
    if ( iter == str.end() )
        // completely clear it
        str.clear();
    else
        // remove the spaces
        str.erase( str.begin(), iter );
    return str;
}

std::string& Utils::trimRight( std::string& str )
{
    std::string::iterator iter = str.end() - 1;
    while ( iter != str.begin() && isspace( *iter ) )
        --iter;
    if ( iter == str.begin() )
        str.clear();
    else
        str.erase( iter + 1, str.end() );
    return str;
}

std::string& Utils::trim( std::string& str )
{
    return trimLeft( trimRight( str ) );
}

void Utils::splitString( const std::string& str, char ch, std::vector<std::string>& vec )
{
    // search start index
    std::string::size_type index = 0;
    // posiion of the first occurence of char ch
    std::string::size_type posn = str.find( ch );
    // while not at the end of the string
    if ( posn == std::string::npos )
        std::cout << "*** Utils::splitString() *** could not find " << ch << std::endl;
    while ( posn != std::string::npos )
    {
        // save the "field" in the provided vector
        vec.push_back( str.substr( index, posn - index ) );
        // start next search from one char after ch
        index = ++posn;
        posn = str.find( ch, posn );
        // if it is at the end, save the current "field"
        if ( posn == std::string::npos  )
        {
            vec.push_back( str.substr( index, str.length( ) ) );
        }
    }
}

bool Utils::isFileExist( const char* fileName )
{
    bool flag = false;
    std::fstream file( fileName, std::ios::in );
    if ( file.is_open() )
    {
        file.close();
        flag = true;
    }
    return flag;
}

