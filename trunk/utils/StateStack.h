#ifndef STATESTACK_H_INCLUDED
#define STATESTACK_H_INCLUDED

//! A simple Stack (FILO) class to control the game states.
/** The stack has a default capacity of 10. More than enough for
  * most game scenarios. 10 would be the maximum push()'s allowed.
  * This class uses the actual objects, it does NOT make copies.
  */
template <typename T>
class StateStack
{
public:
    //! ctor with the default capacity 10
    StateStack( unsigned = 10 );
    //! dtor
    ~StateStack()
    {
        delete [] stackPtr;
    }

    //! push current state onto stack for later retrieval
    /** \param state to push onto stack */
    void push( const T item );

    //! pop top state off the stack
    /** \return last pushed onto stack */
    void pop( );

    //! top state is the current state
    /** \return the top state on the stack */
    T top( );

    bool isEmpty( )
    {
        return ( index == -1 );
    }

    signed size( )
    {
        return index;
    }

private:
    // Number of elements on StateStack
    signed capacity;
    signed index;
    T* stackPtr;

};

// initialise stack index value to -1 (empty condition)
// first state will be index 0
template <typename T>
StateStack<T>::StateStack( unsigned size )  : capacity( size ), index( -1 )
{
    stackPtr = new T[ capacity ];
}

template <typename T>
void StateStack<T>::push( const T state )
{
    if ( index < capacity - 1 )
        stackPtr[ ++index ] = state;
}

template <typename T>
void StateStack<T>::pop( )
{
    if ( index >= 0 )
        --index;
}

template <typename T>
T StateStack<T>::top( )
{
    if ( ! isEmpty() )
    {
        return stackPtr[index];
    }
    // pop unsuccessful
    return 0 ;
}

#endif // STATESTACK_H_INCLUDED



//#ifndef STATESTACK_H_INCLUDED
//#define STATESTACK_H_INCLUDED
//
////! A simple Stack (FILO) class to control the game states.
///** The stack has a default capacity of 10. More than enough for
//  * most game scenarios. 10 would be the maximum push()'s allowed.
//  */
//template <typename T>
//class StateStack
//{
//public:
//    //! ctor with the default capacity 10
//    /** initialise stack index value to -1 (empty condition)
//    // \param size of the stack. Capacity not checked for silly values */
//	StateStack( unsigned size = 10 );
//    //! dtor
//	~StateStack() { delete [] stackPtr; }
//
//	//! push current state onto stack for later retrieval
//	/** \param state to push onto stack */
//	void push( const T item );
//
//	//! pop top state off the stack
//	/** \return last pushed onto stack */
//	void pop( );
//
//	//! top state is the current state
//	/** \return the top state on the stack */
//	T top( );
//
//	bool isEmpty( ) { return ( index == -1 ); }
//
//	signed size( ) { return index; }
//
//private:
//	// Number of elements on StateStack
//	signed capacity;
//	signed index;
//	T* stackPtr;
//
//};
//
//template <typename T>
//StateStack<T>::StateStack( unsigned size ) : capacity(size), index( -1 )
//{
//	// create the stack array (for object pointers)
//	stackPtr = new T[ capacity ];
//	// first state will be index 0
//}
//
//template <typename T>
//void StateStack<T>::push( const T state )
//{
//	if ( index < capacity - 1 )
//		stackPtr[ ++index ] = state;
//}
//
//template <typename T>
//void StateStack<T>::pop( )
//{
//	if ( index < 0 )
//        // pop unsuccessful
//        throw "Empty StateStack error in pop()";
//	else
//		--index;
//}
//
//template <typename T>
//T StateStack<T>::top( )
//{
//	if ( index < 0 )
//        // pop unsuccessful
//        throw "Empty StateStack error in top()";
//	else
//		return stackPtr[index];
//}
//
//#endif // STATESTACK_H_INCLUDED
