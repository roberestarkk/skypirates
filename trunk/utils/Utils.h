#ifndef UTILS_H
#define UTILS_H

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>

//! a few useful functions
/** <ul>
  <li>shortcut radian <- -> degree conversions</li>
  <li>check a file exists</li>
  <li>string conversions to number or boolean</li>
  <li>number or boolean conversions to string</li>
  </ul>
  \note the functions are all declared static, to use:
  <code>signed value = Utils::toInt( "1234" );</code>
  */
class Utils
{
public:
    //! A multiplier (57.29577951f) to convert Radians to Degrees
    static float RadToDeg;

    //! A multiplier (0.017453293f) to convert Degrees to Radians
    static float DegToRad ;

    //! attempt to convert string to a wide character string
    /** \param pstring: the string to convert
    \return the wstring in ISO Latin format only, ie not true Unicode */
    static std::wstring wideString( const std::string& pstring );

    //! attempt to convert unicode wstring to a single char string
    /** \param pstring: the string to convert
    \return the converted std::string  */
    static std::string stdString( const std::wstring& wString );

    //! accepts either TRUE or T (case ignored) and 1 as true, anything else is false
    static bool toBoolean( const std::string& pstring );

    //! attempt to convert string to signed int
    /** \param pstring: the string to convert
    \return the signed int value of the string
    \note User must check value is within range for the given data type */
    static signed toInt( const std::string& pstring );

    //! attempt to convert string to int
    /** \param pstring: the string to convert
    \return the unsigned int value of the string
    \note User must check value is within range for the given data type */
    static unsigned toUInt( const std::string& pstring );

    //! attempt to convert string to float
    /** \param pstring: the string to convert
    \return the unsigned int value of the string
    \note User must check value is within range for the given data type */
    static float toFloat( const std::string& pstring );

    //! compare two strings ignore the case of the characters
    /** \param s1: the first string to test
    \param s2: the second string to test
    \return true if they have the same content */
    static bool ignoreCaseCompare( const std::string& s1, const std::string& s2 );

    //! checks if a file exists by trying to open it
    /** \param fileName: the file name to check
    \return true if it exists and has some content
    false if the file has a name or is 0 bytes */
    static bool isFileExist( const char* fileName );

    /***** STRING HANDLING *****/

    //! convert string contents to all uppercase
    /** using std::transform() to convert the string
    \param str: the string to convert
    \return the uppercase'd string.  */
    static std::string& toUpperCase( std::string& str );

    //! convert string contents to all lowercase
    /** same as toUpperCase() but using iterator and for loop
    \param str: the string to convert
    \return the lowercase'd string.  */
    static std::string& toLowerCase( std::string& str );

    /***** TRIM SPACES FROM STRING  *****/

    //! remove any spaces from the LHS of a string
    /** \param str: the string to trim
    \return the trimmed string. Cleared if all spaces.  */
    static std::string& trimLeft( std::string& str );

    //! remove any spaces from the RHS of a string
    /** \param str: the string to trim
    \return the trimmed string. Cleared if all spaces.  */
    static std::string& trimRight( std::string& str );

    //! remove any spaces from both ends of a string
    /** \param str: the string to trim
    \return the trimmed string. Cleared if all spaces.  */
    static std::string& trim( std::string& str );

    //! break a string into fields\tokens using a given delimiter
    /** \param str: the string to split
    \param ch: the delimiter char
    \param vec: the vector to fill with the fields
    \note For use with CSV or INI files etc. For example:<br>
    String:
    <code>Hello,World,How,Are,You</code>
    has five fields or tokens with a comma delimiter<br>
    The five fields would each be put in the given vector */
    static void splitString( const std::string& str, char ch, std::vector<std::string>& vec );

    //! convert a bool to a string
    /** \param value: the bool value
    \return the string equiv. of the bool or an empty string if invalid value  */
    template<bool>
    static std::string toString( const bool& value )
    {
        std::ostringstream oss;
        try
        {
            oss << std::boolalpha << value;
        }
        catch ( std::ios_base::failure e )
        {
            std::cerr << e.what() << std::endl;
            // clear the ostream
            oss.clear( std::ios::goodbit );
            return "";
        }
        return oss.str();
    }

    //! convert a value to a string
    /** \param value: the nemeric value
    \return the string equiv. of the number or an empty string if conversion not possible  */
    template<typename T>
    static std::string toString( const T& value )
    {
        std::ostringstream oss;
        try
        {
            // Turn on exceptions
            oss.exceptions( std::ios::badbit | std::ios::failbit );
            oss << value;
        }
        catch ( std::ios_base::failure e )
        {
            std::cerr << e.what() << std::endl;
            // clear the ostream
            oss.clear( std::ios::goodbit );
            return "";
        }
        return oss.str();
    }

    //! convert a string to a bool
    /** \param str the string representation of the boolean value
    \return the number in the given data type  */
    template<bool>
    static bool parseString( const std::string& str )
    {
        bool value;
        std::istringstream iss( str );
        iss >> std::boolalpha >> value;
        return value;
    }

    //! convert a string to a value.
    /** \param str the string representation of the value
    \return the bool value
    \note User must check value is within range for the given data type */
    template<typename T>
    static T parseString( const std::string& str )
    {
        T value;
        std::istringstream iss( str );
        iss >> value;
        return value;
    }

    //! case insensitive compare of two characters
    /** \param a: the first character
    \param b: the second character
    \return true if the same else false */
    static bool ignoreCaseCharCompare( char a, char b );

};

#endif // UTILS_H
