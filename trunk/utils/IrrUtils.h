#ifndef IRR_UTILS_H
#define IRR_UTILS_H

#include "Irrlicht.h"

#include <string>
#include <math.h>

using namespace irr;

/**
    rotateNode -- rotate a scene node locally
 */
void rotateNode( scene::ISceneNode *node, core::vector3df rot );

/**
translateNode -- translate a scene node locally
*/
void translateNode( scene::ISceneNode *node, core::vector3df vel );


/** @brief setFogForAllCurrentMeshes
  * Enables fog on terrain, cube and mesh nodes.
  * @param Pointer to the scene Manager
  */
void setFogForAllCurrentMeshes( scene::ISceneManager* pSceneMgr );

/** @brief setLightingForAllCurrentMeshes
  * Used to setup light for the terrain and cube nodes.
  * @param Pointer to the scene Manager
  */
void setLightingForAllCurrentMeshes( scene::ISceneManager* pSceneMgr );

inline float randFloat( float min, float max )
{
    return ( float )( min + ( max - min ) * ( rand() / float( RAND_MAX ) ) );
}

//! from code posted on
//! http://www.ambiera.com/cgi-bin/forum/Blah.pl?b-ie/m-1176834845/
//! function names modified to course standards and commentary added
/** you can create a triangle selector for most scene node types.
you can also identify nodes by their name or id to see if they
were supposed to get collision information generated or not */

//! add all current meshes for collision.
/** To use objects in an .irr file or loaded into the SceneManager
they need to be extracted as nodes and serialised to obtain the
component triangles and those triangles assigned to
a collision detection animator and/or event handler.
\param  pSceneMgr: the scene manager containing the nodes.
\param  camera: the camera that the collisions apply too.
\return the animator that will be attached to the node.
\note the animator MUST be dropped. */
scene::ISceneNodeAnimatorCollisionResponse* addAllCurrentMeshesForCollision(
    scene::ISceneManager* pSceneMgr, scene::ICameraSceneNode* camera );

//! get all the triangles owned by the scene manager
/** \param  pSceneMgr: the scene manager containing the nodes.
\return the meta selector that will be attached to the node.
\note the meta selector MUST be dropped. */
scene::IMetaTriangleSelector* gatherAllTriangles(
    scene::ISceneManager* pSceneMgr );

//! Principally for collision detection.
/** Handles static or animated meshes and cubes.
Code can be modified to ignore/include certain meshes.
Can be modified for game level state save and restore via XML for example.
This is the function to modify for node exclusion by name/id
\param node reference to serialise
\param scene manager reference */
void nodeAssignTriangleSelector( scene::ISceneNode* node, scene::ISceneManager* pSceneMgr );

//! recursive function to iterate a scene manger and assign selectors to the node triangles
/** used by the collision detection and other scene manager funtions, fog, culling etc.
\note do not call this function directly, use managerAssignTriangleSelectors() */
void nodeAssignTriangleSelectors( irr::scene::ISceneNode* node, scene::ISceneManager* pSceneMgr );
//! the entry point for nodeAssignTriangleSelectors()
void managerAssignTriangleSelectors( scene::ISceneManager* pSceneMgr );


//! recursive function to iterate a scene manger and obtain all the node triangle selectors
/** used by the collision detection and other scene manager funtions, fog, culling etc.
\note do not call this function directly, use managerAssignTriangleSelectors() */
void nodeGatherTriangleSelectors( scene::ISceneNode* node, scene::IMetaTriangleSelector* meta );
//! the entry point for nodeGatherTriangleSelectors()
void managerGatherTriangleSelectors( scene::ISceneManager* pSceneMgr, scene::IMetaTriangleSelector* meta );


#endif // IRR_UTILS_H
