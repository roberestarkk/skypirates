# README #
This is the codebase for my major project from my Diploma in Games Design and Dev at Hornsby TAFE.

Regrettably, the original project was versioned using SVN, and the remote repo was lost before anyone thought to keep it for posterity, so all the revision information is gone.

This is not solely my work, see: https://www.youtube.com/watch?v=lrWs2GkdxS4 for a gource visualisation of the repository before it was lost.